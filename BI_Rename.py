from _datetime import datetime
import os

src = 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d") + '.csv'
dst = 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d") + '_old.csv'

if dst in os.listdir('log'):
    os.remove('log/' + 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d") + '_old.csv')
    print('file_removed')

if src in os.listdir('log'):
    src = 'log/' + 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d") + '.csv'
    dst = 'log/' + 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d") + '_old.csv'
    os.rename(src, dst)
else:
    print('OK, no file present')