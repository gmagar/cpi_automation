import os
import time
import pathlib
from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver import ActionChains
from locators.Locators import Locators
from Tests.utilities.reporter import Reporter
from Pages.DashboardPage import dashboardpageclass

class accountclass():
    def __init__(self, driver):
        self.driver = driver
        # self.reporter = Reporter('Account Page')
        self.dashboard = dashboardpageclass(self.driver)

    def check_loading_message(self, reporter):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.account_loading_message))
            )

            load_message = self.driver.find_element(By.XPATH, Locators.account_loading_message)
            if load_message.get_attribute('textContent') == 'Processing...':
                reporter.append_row('Account Page load message', 'OK')
                reporter.report()
                print('Account Page load message is OK')
            else:
                reporter.append_row('Account Page load message', 'NOT OK')
                reporter.report()
                print('Account Page load message is NOT OK')

        except TimeoutException:
            reporter.append_row('Timeout Account_Loading_Message', '--')
            reporter.report()
            print('Timeout Account_Loading_Message')

    def get_report_index(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_selector_v2))
            )

            report_selector = self.driver.find_element(By.XPATH, Locators.report_selector_v2)
            report_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="report_ul"]'))
            )

            report_list = self.driver.find_element(By.XPATH, '//*[@id="report_ul"]')
            report_items = report_list.find_elements(By.TAG_NAME, 'li')
            report_name_list = []
            for item in report_items:
                report_name_list.append(item.text)
            report_selector.click()
            return report_name_list

        except TimeoutException:
            print('Timeout GET_REPORT_INDEX')

    def click_cpi_assortment(self, reporter, first_report_index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_reports"]/a/span/span[1]')))

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.product_detail_loading_message))
            )

            report_selector = self.driver.find_element(By.XPATH, '//*[@id="account_reports"]/a/span/span[1]')
            time.sleep(5)
            report_selector.click()
            # time.sleep(5)
            # report_selector.click()


            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="report_ul"]/li['+str(first_report_index)+']/a')))

            cpi_assortment = self.driver.find_element(By.XPATH, '//*[@id="report_ul"]/li['+str(first_report_index)+']/a')
            cpi_assortment.click()

        except TimeoutException:
            print('Timeout CPI_Assortment_Click')

    def wait_product_detail_message_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.product_detail_loading_message))
            )

        except TimeoutException:
            print('Timeout Product_Detail_Load')

    def wait_cpi_assortment_load(self):
        try:
            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="assortment-detail-datatable_processing"]'))
            )

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//div[@class="report-loader"]'))
            )


        except TimeoutException:
            reporter.append_row('Timeout CPI_Assortment_Load', '--')
            reporter.report()
            print('Timeout CPI_Assortment_Load')

    def set_platform(self, report_platform, select_index, reporter):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/button'))
            )

            test = report_platform
            for i in range(len(test)):
                if test[i] == 'Google':
                    test[i] = 'Google Shopping'
            platform_filter_text = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/button/span')
            platform_text = platform_filter_text.text
            if platform_text != test[int(select_index)]:
                '//*[@id="filtercontrols"]/form[1]/div[2]/div/div/ul/li[7]/a/label'
                platform_filter = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/button')
                platform_filter.click()
                # platform_select = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/ul/li['+str(select_index+2)+']')
                platform_select = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/ul/li/a/label[contains(text(),"'+str(test[int(select_index)])+'")]')
                # platform_select = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[2]/div/div/ul/li/a/label[contains(text(),"'+str(test[int(select_index)])+'")]')
                platform_select.click()

                WebDriverWait(self.driver, 30).until(
                    EC.element_to_be_clickable((By.XPATH, Locators.apply_filter_button))
                )

                try:
                    WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/ul/li/a/label[contains(text(),"'+str(test[int(select_index)])+'")]'))
                    )
                    platform_filter.click()

                except TimeoutException:
                    pass

                click_apply_filter = self.driver.find_element(By.XPATH, Locators.apply_filter_button)
                click_apply_filter.click()

        except TimeoutException:
            # reporter.append_row('Timeout Platform Select', '--')
            # reporter.report()
            print('Timeout Platform Select')

    def set_platform_competitor_report(self, report_platform, select_index, reporter):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/button'))
            )

            test = report_platform
            # for i in range(len(test)):
            #     if test[i] == 'Google':
            #         test[i] = 'Google Shopping'
            platform_filter_text = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/button/span')
            platform_text = platform_filter_text.text
            if platform_text != test[int(select_index)]:
                platform_filter = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/button')
                platform_filter.click()
                platform_select = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul/li[2]/a/label[contains(text(),"'+str(test[int(select_index)])+'")]')
                platform_select.click()

                WebDriverWait(self.driver, 30).until(
                    EC.element_to_be_clickable((By.XPATH, Locators.apply_filter_button))
                )

                try:
                    WebDriverWait(self.driver, 10).until(
                        EC.visibility_of_element_located((By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul/li[2]/a/label[contains(text(),"'+str(test[int(select_index)])+'")]'))
                    )
                    platform_filter.click()

                except TimeoutException:
                    pass

                click_apply_filter = self.driver.find_element(By.XPATH, Locators.apply_filter_button)
                click_apply_filter.click()

        except TimeoutException:
            # reporter.append_row('Timeout Platform Select', '--')
            # reporter.report()
            print('Timeout Platform Select')

    def click_cpi_dashboard(self, reporter, report_index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_selector_v2)))

            report_selector = self.driver.find_element(By.XPATH, Locators.report_selector_v2)
            report_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="report_ul"]/li['+str(report_index)+']')))

            cpi_dashboard = self.driver.find_element(By.XPATH, '//*[@id="report_ul"]/li['+str(report_index)+']')
            cpi_dashboard.click()

        except TimeoutException:
            # reporter.append_row('Timeout CPI_Dashboard_Select', '--')
            # reporter.report()
            print('Timeout CPI_Dashboard_Select')

    def wait_cpi_dashboard(self, reporter):
        try:
            # WebDriverWait(self.driver, 300).until(
            #     EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div[2]/div/table/tbody/tr[3]/td[2]/img'))
            # )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.report_loader_icon))
            )

        except TimeoutException:
            # reporter.append_row('Timeout CPI_Dashboard_Load', '--')
            # reporter.report()
            print('Timeout CPI_Dashboard_Load')

    def click_cpi_insights(self, reporter):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_selector_v2)))

            report_selector = self.driver.find_element(By.XPATH, Locators.report_selector_v2)
            report_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_insights_v2_report_select)))

            cpi_insights = self.driver.find_element(By.XPATH, Locators.cpi_insights_v2_report_select)
            cpi_insights.click()

        except TimeoutException:
            # reporter.append_row('Timeout CPI_Insights_Select', '--')
            # reporter.report()
            print('Timeout CPI_Insights_Select')

    def wait_cpi_insights(self, reporter):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_insights_wait))
            )

        except TimeoutException:
            # reporter.append_row('Timeout CPI_Insights_Load', '--')
            # reporter.report()
            print('Timeout CPI_Insights_Load')

    def click_map_violations(self, reporter, report_index):
        try:
            WebDriverWait(self.driver, 30).until(
                 EC.visibility_of_element_located((By.XPATH, Locators.report_selector_v2)))

            report_selector = self.driver.find_element(By.XPATH, Locators.report_selector_v2)
            report_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="report_ul"]/li['+str(report_index)+']')))

            cpi_map_violation = self.driver.find_element(By.XPATH, '//*[@id="report_ul"]/li['+str(report_index)+']')
            cpi_map_violation.click()


        except TimeoutException:
            # reporter.append_row('Timeout CPI_Map_Violations', '--')
            # reporter.report()
            print('Timeout CPI_Map_Violations')


    def check_no_data_map_violation(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.loader_loading_icon))
            )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.report_loader_icon))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]'))
            )

            map_violation_no_data_check = self.driver.find_element(By.XPATH, '//*[@id="block-row1col1"]')
            return map_violation_no_data_check.text

        except TimeoutException:
            return 'Data'

    def wait_map_violations(self, reporter):
        try:
            # WebDriverWait(self.driver, 300).until(
            #     EC.invisibility_of_element_located((By.XPATH, Locators.nearest_5_wait))
            # )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="row1col1"]/div/div[2]/div[3]'))
            )

        except TimeoutException:
            # reporter.append_row('Timeout CPI_Map_Violation_Load', '--')
            # reporter.report()
            print('Timeout CPI_Map_Violation_Load')

    def click_cpi_nearest_competitors(self, reporter, report_index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_selector_v2)))

            report_selector = self.driver.find_element(By.XPATH, Locators.report_selector_v2)
            report_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="report_ul"]/li['+str(report_index)+']')))

            cpi_nearest_competitors = self.driver.find_element(By.XPATH, '//*[@id="report_ul"]/li['+str(report_index)+']')
            cpi_nearest_competitors.click()

        except TimeoutException:
            # reporter.append_row('Timeout CPI_Nearest_Competitors_Click', '--')
            # reporter.report()
            print('Timeout CPI_Nearest_Competitors_Click')

    def wait_cpi_report(self, reporter):
        try:
            # WebDriverWait(self.driver, 300).until(
            #     EC.invisibility_of_element_located((By.XPATH, Locators.nearest_5_wait))
            # )

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_report_loader_icon))
            )

        except TimeoutException:
            print('Timeout CPI_Report')

    def click_cpi_product_detail(self, reporter):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_selector_cpi_product_detail))
            )

            cpi_product_detail = self.driver.find_element(By.XPATH, Locators.report_selector_cpi_product_detail)
            cpi_product_detail.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.go_button_dashboard)))

            go_button = self.driver.find_element(By.XPATH, Locators.go_button_dashboard)
            go_button.click()


        except TimeoutException:
            # reporter.append_row('Timeout CPI_Product_Detail_Click', '--')
            # reporter.report()
            print('Timeout CPI_Product_Detail_Click')

    def wait_product_detail_load(self, reporter):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.product_detail_load_wait)))

        except TimeoutException:
            # reporter.append_row('Timeout Product_Detail_Page', '--')
            # reporter.report()
            print('Timeout Product_Detail_Page')

    def set_product_sku(self, reporter, sku):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.product_page_filter))
            )

            insert_product_sku = self.driver.find_element(By.XPATH, Locators.product_page_filter)
            insert_product_sku.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.product_page_enter_sku))
            )

            send_sku = self.driver.find_element(By.XPATH, Locators.product_page_enter_sku)
            send_sku.send_keys(sku)

            time.sleep(5)

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.product_detail_page_sku_result))
            )

            click_sku_result = self.driver.find_element(By.XPATH, Locators.product_detail_page_sku_result)
            click_sku_result.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.apply_filter_button))
            )

            apply_filter_click = self.driver.find_element(By.XPATH, Locators.apply_filter_button)
            apply_filter_click.click()

        except TimeoutException:
            print('Timeout Insert Product SKU')

    def wait_product_detail_load_with_sku(self, reporter):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.product_detail_load_wait)))

        except TimeoutException:
            # reporter.append_row('Timeout Product_Detail_Page', '--')
            # reporter.report()
            print('Timeout Product_Detail_Page_SKU_Updated')

    def click_map_violation_detail(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.map_violation_detail_first_row))
            )

            first_seller_in_map = self.driver.find_element(By.XPATH, Locators.map_violation_first_seller)
            first_seller_name = first_seller_in_map.text

            first_seller_map_count = self.driver.find_element(By.XPATH, Locators.map_violation_first_seller_count)
            first_seller_map_count.click()

            self.driver.switch_to.window(self.driver.window_handles[2])
            return first_seller_name

        except TimeoutException:
            print('Timeout click_map_violation_count')


    def check_map_violation_detail(self, seller_name, map_violation_checker):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.map_violation_detail_page_competitor))
            )

            competitor_name = self.driver.find_element(By.XPATH, Locators.map_violation_detail_page_competitor)
            if seller_name.lower() in competitor_name.text.lower() and 'cpi_map_violations_detail' in self.driver.current_url:
                map_violation_checker.append(['Map_Violation_Page_Redirection', 'PASS'])
            else:
                map_violation_checker.append(['Map_Violation_Page_Redirection', 'FAIL'])
            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[1])

        except TimeoutException:
            print('Timeout Check_Map_Violation_Detail')

    def map_violation_download_in_map_violation_page(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.map_violation_download_map_violation_report))
            )

            map_click = self.driver.find_element(By.XPATH, Locators.map_violation_download_map_violation_report)
            map_click.click()


        except TimeoutException:
            print('Timeout Click_Map_Violation_MapViolation_Report')

    def wait_map_violation_download(self):
        def wait_competitors_report_page_load(self):
            try:
                WebDriverWait(self.driver, 300).until(
                    EC.invisibility_of_element_located((By.XPATH,
                                                        '//*[@id="cpi-competitors-list_processing"]/span[contains(text(),"Please wait while we repopulating Seller list...")]'))
                )

                time.sleep(2)

                WebDriverWait(self.driver, 300).until(
                    EC.invisibility_of_element_located((By.XPATH,
                                                        '//*[@id="cpi-competitors-list_processing"]/span[contains(text(),"Please wait while we are loading your report data...")]'))
                )

                WebDriverWait(self.driver, 300).until(
                    EC.invisibility_of_element_located((By.XPATH, Locators.cpi_report_loading_message))
                )

            except TimeoutException:
                print('Timeout Competitor_report_load')

    def wait_map_violation_download(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-competitors-list_processing"]/span'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.cpi_report_loading_message))
            )

        except TimeoutException:
            print('Timeout Competitor_report_load')


    def map_download_verification(self, map_file_name, map_violation_checker, map_folder):
        file = str(pathlib.Path().absolute()) + '\\map_files\\'+str(map_folder)+'\\'+str(map_file_name)+'.zip'
        if os.path.exists(file):
            current_time = datetime.strptime(datetime.now().strftime("%H::%M::%S"), '%H::%M::%S')
            file_created_time = time.strftime('%H::%M::%S', time.localtime(os.path.getctime(file)))
            file_created_time_date_format = datetime.strptime(file_created_time, '%H::%M::%S')
            difference_time = (current_time - file_created_time_date_format).total_seconds()/3600
            if difference_time < 1:
                map_violation_checker.append(['Downloading of Map file', 'PASS'])
            else:
                map_violation_checker.append(['Downloading of Map file', 'FAIL'])
        else:
            map_violation_checker.append(['Downloading of Map file', 'FAIL'])





