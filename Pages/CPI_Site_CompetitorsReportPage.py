import re
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, WebDriverException, NoSuchElementException
from selenium.webdriver import ActionChains
from locators.Locators import Locators
from Tests.utilities.reporter import Reporter
from Pages.DashboardPage import dashboardpageclass

class cpisitecompetitorsreportclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('CPI_SITE_Competitors_Report Page')
        self.dashboard = dashboardpageclass(self.driver)

    def wait_for_page_load(self):
        try:
            time.sleep(15)

            WebDriverWait(self.driver, 600).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

        except TimeoutException:
            print('Timeout CPI_CompetitorReport_Page')

        except NoSuchElementException:
            pass

    def reset_seller_filter(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_competitor_report_seller_filter))
            )

            seller_filter = self.driver.find_element(By.XPATH, Locators.cpi_site_competitor_report_seller_filter)
            seller_filter.click()

            try:
                WebDriverWait(self.driver, 60).until(
                    EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_competitor_report_seller_filter_top_25))
                )

                seller_filter_top25 = self.driver.find_element(By.XPATH, Locators.cpi_site_competitor_report_seller_filter_top_25)
                if seller_filter_top25.is_selected() is False:
                    seller_filter_top25.click()
                WebDriverWait(self.driver, 30).until(
                    EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_filter_button))
                )

                filter_button = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
                filter_button.click()

            except TimeoutException:
                pass

        except TimeoutException:
            print('Timeout CPI_Site_Competitor_Report_Reset_Seller_Filter')

    def reset_slider_value(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/div/div/div[5]'))
            )

            slider_first = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/div/div/div[5]')
            slider_last = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[4]/div/div/div[6]')
            # location = slider.location
            # x = location.get('x')
            # y = location.get('y')
            move = ActionChains(self.driver)
            # move.click_and_hold(slider).move_by_offset(int(x/2), int(x/2)).release().perform()  # 10 offset = 40 space
            move.drag_and_drop_by_offset(slider_first, -100, 0).release().perform()
            move.drag_and_drop_by_offset(slider_last, 100, 0).release().perform()

        except TimeoutException:
            self.driver.save_screenshot('CompReport_slider.png')
            print('Timeout CPI_Site_CompReport_Reset_slider_value')

    def get_product_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.competitor_report_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.competitor_report_product_count).text
            product_count = re.split(r'(of | entries)\s*', product_count_text)
            return product_count[2]

        except TimeoutException:
            print('Timeout competitor_report_get_product_count')

    def check_go_color(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.go_button_cpi)))

            cpi_go_button = self.driver.find_element(By.XPATH, Locators.go_button_cpi)
            self.check_color(cpi_go_button, 'CPI GO Button')

        except TimeoutException:
            print('Timeout CPI_Page_check_color')

    def change_slider_value(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.current_price_slider))
            )

            slider = self.driver.find_element(By.XPATH, Locators.current_price_slider)
            move = ActionChains(self.driver)
            move.click_and_hold(slider).move_by_offset(10, 0).release().perform()  # 10 offset = 40 space
            time.sleep(5)

        except TimeoutException:
            print('Timeout change_price_slider')

    def select_account(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.select_account)))

            select_account = self.driver.find_element_by_xpath(Locators.select_account)
            select_account.click()

            # select_account_input = self.driver.find_element(By.XPATH, Locators.select_account)
            # select_account_input.send_keys(account_name)
            # time.sleep(2)
            # select_account_input.send_keys(Keys.ENTER)

        except TimeoutException:
            print('Timeout CPI_Page')

    def click_go(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.go_button_cpi))
            )

            cpi_go_button = self.driver.find_element(By.XPATH, Locators.go_button_cpi)
            cpi_go_button.click()

        except TimeoutException:
            print('Timeout go_button_CPI')

    def check_load_message(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_report_loading_message))
            )

            cpi_loading_message = self.driver.find_element(By.XPATH, Locators.cpi_report_loading_message)
            if 'Exclusive' in cpi_loading_message.text:
                self.reporter.append_row('Load message has text Exclusive?', 'NOT OK')
                self.reporter.report()
                print('Load message has text Exclusive')
            else:
                self.reporter.append_row('Load message has text Exclusive?', 'OK')
                self.reporter.report()
                print('Load message is OK')

        except TimeoutException:
            print('Timeout CPI_Page_Load_Message')

    def wait_competitors_report_page_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-competitors-list_processing"]/span[contains(text(),"Please wait while we repopulating Seller list...")]'))
            )

            time.sleep(2)

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-competitors-list_processing"]/span[contains(text(),"Please wait while we are loading your report data...")]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.cpi_report_loading_message))
            )

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//*[@class="loading-logo"]'))
            )

        except TimeoutException:
            print('Timeout Competitor_report_load')

    def wait_change_slider_report_page_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.cpi_report_loading_message))
            )

        except TimeoutException:
            print('Timeout Competitor_report_load')

    def check_button_in_main_competitor_report(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.export_report_button))
            )

            export_report = self.driver.find_element(By.XPATH, Locators.export_report_button)
            self.check_color(export_report, 'Export Report Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.filter_button)
            self.check_color(filter_button, 'Filter Button')

        except TimeoutException:
            print('Timeout Competitor_Report_Button')

    def check_navigation_color(self):
        try:
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.page_number))
            )

            page_number = self.driver.find_element(By.XPATH, Locators.page_number)
            self.check_color(page_number, 'Page Number Link')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.jump_page))
            )

            jump_page = self.driver.find_element(By.XPATH, Locators.jump_page)
            self.check_color(jump_page, 'Jump Page Number Link')

        except TimeoutException:
            print('Timeout page number')

    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + ' color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + ' color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')

    def clear_filter(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_product_filter))
            )

            product_name_filter = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_product_filter)
            product_name_filter.clear()

            WebDriverWait(self.driver, 600).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_reset_button))
            )
            reset_button = self.driver.find_element(By.XPATH,Locators.cpi_site_reset_button)
            reset_button.click()

        except TimeoutException:
            print('Timeout Reset_Filter')


    def select_price_category(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_site_comp_report_price_category_filter))
            )

            price_category = self.driver.find_element(By.XPATH, Locators.cpi_site_comp_report_price_category_filter)
            price_category.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_comp_report_price_category_filter_select_all))
            )

            price_category_select_all = self.driver.find_element(By.XPATH, Locators.cpi_site_comp_report_price_category_filter_select_all)
            if price_category_select_all.is_selected() is False:
                price_category_select_all.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_filter_button))
            )

            time.sleep(1)
            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
            self.driver.execute_script("arguments[0].click();", filter_button)
            # filter_button.click()

        except TimeoutException:
            print('Timeout CPI_CompReport_PRODUCT_COUNT_CHEAP_NONCHEP')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_comp_report_platform))
            )

            time.sleep(10)
            platform = self.driver.find_element(By.XPATH, Locators.cpi_site_comp_report_platform)
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_comp_report_platform_ul))
            )

            platform_list = self.driver.find_element(By.XPATH, Locators.cpi_site_comp_report_platform_ul)
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout cpi_site_comp_report_platform')

    def selected_text(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//span[contains(@class, "multiselect-selected-text")]'))
            )
            selected_option = self.driver.find_element_by_xpath('//span[contains(@class, "multiselect-selected-text")]')

            return selected_option.text

        except TimeoutException:
            print('Timeout Selected_Text_COMP_REPORT')


    def select_platform(self, select_platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_comp_report_platform))
            )

            # WebDriverWait(self.driver, 30).until(
            #     EC.element_to_be_clickable((By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[2]'))
            # )

            # clear_icon = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[2]')
            # clear_icon.click()

            platforms = self.driver.find_element(By.XPATH, Locators.cpi_site_comp_report_platform)
            platforms.click()

            to_select_platform = self.driver.find_element(By.XPATH, '/html/body/div[1]/div[1]/div[2]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul/li/a/label[contains(text(),"'+str(select_platform)+'")]')
            to_select_platform.click()

            time.sleep(15)

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//*[@class="loading-logo"]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_site_filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
            filter_button.click()

        except TimeoutException:
            print('Timeout CPI_Comp_Report_Select_Platform')

    def cpi_site_get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_run_date))
            )

            run_date = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_run_date)
            return run_date.text

        except TimeoutException:
            print('Timeout comp_report_run_date')


