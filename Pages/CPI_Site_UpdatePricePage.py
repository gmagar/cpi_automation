import time
import re
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from Config import Database as db
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, WebDriverException, NoSuchElementException
from locators.Locators import Locators
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter

class cpisiteupdatepricesclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Update_Prices Page')
        self.dashboard = dashboardpageclass(self.driver)

    def check_loading_message(self):
        try:
            # WebDriverWait(self.driver, 30).until(
            #     EC.visibility_of_element_located((By.XPATH, Locators.update_prices_loading_message))
            # )

            print('check loading message triggered')
            loading_message_logo = self.driver.find_element(By.XPATH, Locators.update_prices_loading_message_icon)
            if 'gbd-logo' in loading_message_logo.get_attribute('src'):
                self.reporter.append_row('Loading message logo', 'OK')
                self.reporter.report()
                print('Loading message logo is OK')
            else:
                self.reporter.append_row('Loading message logo', 'NOT OK')
                self.reporter.report()
                print('Loading message logo is NOTOK')

            loading_message_text = self.driver.find_element(By.XPATH, Locators.update_prices_loading_message_text)
            if loading_message_text.get_attribute('textContent') == 'Please wait while we are loading your report data...':
                self.reporter.append_row('Loading message text', 'OK')
                self.reporter.report()
                print('Loading message text is OK')
            else:
                self.reporter.append_row('Loading message text', 'NOT OK')
                self.reporter.report()
                print('Loading message text is NOTOK')

        except TimeoutException:
            print('Timeout Update_Price_Loading_Message')

    def check_page_for_exclusive(self):
        if 'ExclusiveConcept' in self.driver.page_source:
            print('Exclusive Text is present please check')
            self.reporter.append_row('Exclusive Text', 'NOT OK')
            self.reporter.report()
        else:
            print('Exclusive Text not present')
            self.reporter.append_row('Exclusive Text', 'OK')
            self.reporter.report()

    def check_loader_and_top_logo(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.mini_logo)))

            top_logo = self.driver.find_element(By.XPATH, Locators.mini_logo)
            if 'gbd-logo' in top_logo.get_attribute("src"):
                self.reporter.append_row('Mini logo', 'OK')
                self.reporter.report()
                print('Mini Logo OK')
            else:
                self.reporter.append_row('Mini logo', 'NOT OK')
                self.reporter.report()
                print('Mini Logo not same')

            loader_logo = self.driver.find_element(By.XPATH, Locators.loader_logo)
            if 'gbd-logo' in loader_logo.get_attribute("src"):
                self.reporter.append_row('Loader Logo', 'OK')
                self.reporter.report()
                print('Loader Logo OK')
            else:
                self.reporter.append_row('Loader Logo', 'NOT OK')
                self.reporter.report()
                print('Loader Logo not same')

            loader_icon = self.driver.find_element(By.XPATH, Locators.loader_loading_icon)
            if '10px solid rgb(175, 0, 0)' in loader_icon.value_of_css_property('border-top'):
                self.reporter.append_row('Loader loading Color', 'OK')
                self.reporter.report()
                print('Loader loading color is OK')
            else:
                self.reporter.append_row('Loader loading Color', 'NOT OK')
                self.reporter.report()
                print('Loader loading color is Not OK')

        except TimeoutException:
            print('Timeout report_load')

    def wait_for_page_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.update_price_select_account)))

        except TimeoutException:
            print('Timeout CPI_Page')

    def select_account(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.update_price_select_account)))

            # print(os.environ["Account"])
            # account_name = input("Please Enter Account Name:  ")
            # account = self.driver.find_element(By.XPATH, '//select[@id="accounts"]/option[text()="'+os.environ["Account"]+'"]')
            account = self.driver.find_element(By.XPATH, '//select[@id="accounts"]/option[contains(@value,"'+str(Locators.account_id)+'")]')
            account.click()

        except TimeoutException:
            print('Timeout select account')

    def cpi_site_wait_switch_platform(self):
        try:
            # try:
            #     WebDriverWait(self.driver, 30).until(
            #         EC.visibility_of_element_located((By.XPATH,'//*[@id="cpi-list_processing"]/span[contains(text(),"Please wait while we repopulating Seller list...")]'))
            #     )
            # except WebDriverException:
            #     pass
            #
            # WebDriverWait(self.driver, 30).until(
            #     EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-list_processing"]/span[contains(text(),"Please wait while we repopulating Seller list...")]'))
            # )
            time.sleep(15)


            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="report-loader"]'))
            )

        except TimeoutException:
            print('Timeout WAIT_CPI_site_Switch_Platform')

    def cpi_site_wait_sku_click(self):
        try:
            time.sleep(15)

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="assortment-detail-datatable_processing"]'))
            )

            WebDriverWait(self.driver, 300).until_not(
                EC.visibility_of_element_located((By.XPATH, '//div[@class="report-loader"]'))
            )

        except TimeoutException:
            print('Timeout WAIT_SKU_CLICK_Platform')

    def check_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.filter_button)
            self.dashboard.check_color(filter_button, 'Filter Button Update Prices')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.export_prices))
            )

            export_prices = self.driver.find_element(By.XPATH, Locators.export_prices)
            self.dashboard.check_color(export_prices, 'UpdatePrices Export Proces Button')

        except TimeoutException:
            print('Timeout Export Prices Buttons')

    def get_clickable_sku_index(self):
        i = 1
        while i < 30:
            try:
                WebDriverWait(self.driver, 30).until(
                    EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(i)+']/td[2]/a'))
                )
                return i
            except TimeoutException:
                i = i+1
                pass
        return 1

    def get_first_sku_name(self, clickable_sku_index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]/a'))
            )

            first_sku = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]/a')
            return first_sku.text

        except TimeoutException:
            print('Timeout SKU Read')

    def check_click_first_sku(self, clicked_sku, platform, update_price_check):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/table/tbody/tr/td[2]/span'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.product_detail_page_product_name))
            )

            sku_name_xpath = self.driver.find_element(By.XPATH, Locators.product_detail_page_product_name)
            platform_xpath = self.driver.find_element(By.XPATH, Locators.dashboard_rundate)
            lowest_competitor = self.driver.find_element(By.XPATH, Locators.product_detal_lowest_competitor_name)
            # print(lowest_competitor.text, sku_name_xpath.text)
            sku_name = sku_name_xpath.text
            if clicked_sku in sku_name and platform in platform_xpath.text:
                update_price_check.append(['Redirection to Product Detail Page on click SKU', 'PASS'])

            else:
                update_price_check.append(['Redirection to Product Detail Page on click SKU', 'FAIL'])

            return lowest_competitor.text

        except TimeoutException:
            print('Timeout First_Sku_Click')


    def cpi_site_click_first_item(self, clickable_sku_index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[2]/a'))
            )

            platform = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform)
            platform.click()
            platform.click()

            updateprice_first_item = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[2]/a')
            updateprice_first_item.click()
            self.driver.save_screenshot("screenshot_2.png")
            # print(len(self.driver.window_handles))
            self.driver.switch_to.window(self.driver.window_handles[1])

        except TimeoutException:
            print('Timeout cpi_site_click_first_item')

    def check_click_first_item(self, platform, update_price_check, lowest_competitor_name):
        try:
            # WebDriverWait(self.driver, 300).until(
            #     EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_first_item_click_wait))
            # )

            time.sleep(10)
            item_url = self.driver.current_url
            # print(lowest_competitor_name, item_url, lowest_competitor_name.lower())
            if 'Google'.lower() in platform.lower().split(' ')[0] or 'Amazon'.lower() in platform.lower().split(' ')[0]:
                if platform.lower().split(' ')[0] in item_url:
                    update_price_check.append(['Redirection to Platform Page on click item link', 'PASS'])
                else:
                    update_price_check.append(['Redirection to Platform Page on click item link', 'FAIL'])
            else:
                if lowest_competitor_name.lower() in item_url:
                    update_price_check.append(['Redirection to Platform Page on click item link', 'PASS'])
                else:
                    update_price_check.append(['Redirection to Platform Page on click item link', 'FAIL'])

            self.driver.save_screenshot("screenshot_3.png")
            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])
            self.driver.save_screenshot("screenshot_4.png")

        except TimeoutException:
            print('Timeout check_click_first_item')

    def seller_info_name(self, clickable_sku):
        updateprice_first_item = self.driver.find_element(By.XPATH,
                                                          '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr[' + str(
                                                              clickable_sku) + ']/td[14]')
        print(updateprice_first_item.text)

    def filter_only_seller(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter))
            )

            platform = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform)
            platform.click()

            price_category_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter)
            price_category_xpath.click()

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/ul/li/a/label[contains(text(), "Only Seller")]'))
            )

            select_all_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter_select_all)
            if select_all_xpath.is_selected() is True:
                select_all_xpath.click()

            parameter_to_click = ['Cheapest / Stay', 'Not Cheapest / Stay', 'Cheapest / Raise', 'Not Cheapest / Raise', 'Cheapest / Lower', 'Not Cheapest / Lower', 'Only Seller']

            for i in parameter_to_click:
                parameter_xpath = self.driver.find_element(By.XPATH,
                                                         '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/ul/li/a/label[contains(text(), "'+str(i)+'")]')
                parameter_xpath.click()


            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_filter_button))
            )

            filter_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
            filter_xpath.click()

        except TimeoutException:
            print('Timeout filter_only_seller')

    def filter_cheapest_nonc_raise(self, parameter_to_click):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter))
            )

            price_category_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter)
            price_category_xpath.click()

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH,Locators.cpi_site_update_price_category_filter_select_all))
            )

            select_all_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter_select_all)

            if select_all_xpath.is_selected() is True:
                select_all_xpath.click()
            else:
                select_all_xpath.click()
                time.sleep(2)
                select_all_xpath.click()

            time.sleep(3)

            for i in parameter_to_click:
                parameter_xpath = self.driver.find_element(By.XPATH,
                                                         '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/ul/li/a/label[contains(text(), "'+str(i)+'")]')
                parameter_xpath.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_filter_button))
            )

            filter_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
            filter_xpath.click()

        except TimeoutException:
            print('Timeout filter_for_arrow_check')

    def check_action_sign(self, arrow, update_price_check):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr[1]/td[9]/div'))
            )
            logo = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr[1]/td[9]/div')
            if str(arrow)+'.png' in logo.value_of_css_property('background-image'):
                update_price_check.append([str(arrow)+' Arrow Sign', 'PASS'])
            else:
                update_price_check.append([str(arrow)+' Arrow Sign', 'FAIL'])

        except TimeoutException:
            pass
            # print('Timeout check_filter_for_arrow_check')

    def check_stay_action_sign(self, arrow, update_price_check):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr[1]/td[9]/div'))
            )
            logo = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr[1]/td[9]/div')
            if str(arrow) in logo.get_attribute('class'):
                update_price_check.append([str(arrow)+' Arrow Sign', 'PASS'])
            else:
                update_price_check.append([str(arrow)+' Arrow Sign', 'FAIL'])

        except TimeoutException:
            pass
            # print('Timeout check_stay_filter_for_arrow_check')

    def get_map_sku(self, run_date, account_name):
        map_sku = db.getMapVoilationSKU("""
                                        select top 1 pp.LocalSku from PIMProduct pp
                                        inner join CPIPricingDetail cpd
                                        on pp.id = cpd.ProductID
                                        where pp.storeid = (select id from PIMstore where name = '"""+str(account_name)+"""')
                                        and cpd.rundatetime = '"""+str(run_date)+"""'
                                        and cpd.MapViolatedCount>1;
                                        """)
        return map_sku

    def filter_product_sku(self, sku):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_product_filter))
            )

            product_name = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_product_filter)
            product_name.clear

            product_name.send_keys(sku)

        except TimeoutException:
            print("Timeout Update_Price_filter_product")

    def check_map_download(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr/td[7]/a'))
            )

            map_click = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr/td[7]/a')
            map_click.click()

            WebDriverWait(self.driver, 100).until(EC.number_of_windows_to_be(2))

        except TimeoutException:
            print('Timeout Click_Map_Violation_Update_Price')

    def wait_map_download(self):
        paths = WebDriverWait(self.driver, 120, 1).until(self.every_downloads_chrome(self.driver))
        # print(paths)

    def every_downloads_chrome(self,driver):
        if not driver.current_url.startswith("chrome://downloads"):
            driver.get("chrome://downloads/")
        return driver.execute_script("""
            var items = downloads.Manager.get().items_;
            if (items.every(e => e.state === "COMPLETE"))
                return items.map(e => e.fileUrl || e.fileUrl);
            """)

    def click_seller_info(self, update_price_check):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr[3]/td[3]/a'))
            )


            sku_name_xpath = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr[3]/td[3]/a')
            sku_name = sku_name_xpath.text
            seller_table = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[2]/div/div/div/table/thead/tr/th[14]')
            seller_info = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr[3]/td[14]')

            self.driver.execute_script("arguments[0].scrollIntoView();", seller_table)

            time.sleep(4)
            seller_info.click()
            self.driver.save_screenshot("screenshot_seller_click_issue.png")
            self.driver.switch_to.window(self.driver.window_handles[1])
            self.driver.save_screenshot("screenshot_seller_click_issue_2.png")
            if 'CompetitorsReport' in self.driver.current_url and sku_name in self.driver.current_url:
                update_price_check.append(['Redirection of Seller_Info', 'PASS'])
            else:
                update_price_check.append(['Redirection of Seller_Info', 'FAIL'])

            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])


        except TimeoutException:
            pass
            print('Timeout Click_Seller_Info')

    def cpi_site_click_first_sku(self, clickable_sku_index):
        try:
            time.sleep(10)
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div/i'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]'))
            )
            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div/i'))
            )

            self.driver.save_screenshot("affordable_lampss.png")

            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]'))
            )
            first_sku = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]')
            first_sku.click()
            self.driver.switch_to.window(self.driver.window_handles[1])

        except TimeoutException:
            print('Timeout CPI_SKU')

    def wait_sku_detail_load(self):
        try:
            # self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.TAB)
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="container"]/div/table/tbody/tr[5]/td[2]'))
            )

        except TimeoutException:
            print('Timeout Load_SKU')

    def change_slider_value(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.current_price_slider))
            )

            slider = self.driver.find_element(By.XPATH, Locators.current_price_slider)
            move = ActionChains(self.driver)
            move.click_and_hold(slider).move_by_offset(10, 0).release().perform()  # 10 offset = 40 space
            time.sleep(5)

        except TimeoutException:
            print('Timeout change_price_slider')

    def check_dashboard_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.go_button_dashboard)
            ))

            go_button = self.driver.find_element(By.XPATH, Locators.go_button_dashboard)
            self.check_color(go_button , 'Go Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.search_report_button)
            ))

            search_button = self.driver.find_element(By.XPATH, Locators.search_report_button)
            self.check_color(search_button, 'Search Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.execute_button)
            ))

            execute_button = self.driver.find_element(By.XPATH, Locators.execute_button)
            self.check_color(execute_button, 'Execute Button')


        except TimeoutException:
            print('Timeout dashboard_buttons')


    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + ' color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + ' color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')

    def check_navigation_color(self):
        try:
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_prices_page_number))
            )
            page_number = self.driver.find_element(By.XPATH, Locators.update_prices_page_number)
            self.check_color(page_number, 'Page Number Link')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.jump_page)
            )                )

            jump_page = self.driver.find_element(By.XPATH, Locators.jump_page)
            self.check_color(jump_page, 'Jump Page Number Link')

        except TimeoutException:
            print('Timeout Update_Price page number')

    def wait_account_page(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.cpi_specialist_load_message))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_report))
            )

        except TimeoutException:
            print('Timeout report_load')

    def cpi_site_wait_account_page(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="hot"]'))
            )

            time.sleep(2)

        except TimeoutException:
            print('Timeout cpi_site_report_load')

    def cpi_site_get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_run_date))
            )

            run_date = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_run_date)
            return run_date.text

        except TimeoutException:
            print('Timeout run_date')

    def get_run_date_V2(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div'))
            )

            run_date = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div')
            run_date.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/ul/li[2]'))
            )

            run_date_data = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[1]/div/div/ul/li[2]/a/label')
            date_to_return = run_date_data.text
            run_date.click()
            return date_to_return

        except TimeoutException:
            print('Timeout run_date_dashboard')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_platform))
            )

            time.sleep(10)
            platform = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform)
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_platform_ul))
            )

            platform_list = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform_ul)
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout cpi_site_platform')

    def is_date_present(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="Rundate"]'))
            )

            run_date = self.driver.find_element(By.XPATH, '//*[@id="Rundate"]')
            if run_date.text != " ":
                return True
            else:
                return False

        except TimeoutException:
            print('Timeout update_price_run_date')

    def select_price_category(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter))
            )

            platform = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform)
            platform.click()
            platform.click()

            price_category = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter)
            price_category.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter_select_all))
            )

            price_category_select_all = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter_select_all)
            # print(self.is_checked(self.driver, price_category_select_all))
            if price_category_select_all.is_selected() is False:
                price_category_select_all.click()


            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
            filter_button.click()

        except TimeoutException:
            print('Timeout PRODUCT_COUNT_CHEAP_NONCHEP_UPDATE_PRICE')

    def get_product_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.update_price_product_count).text
            product_count = re.split(r'(of | entries)\s*', product_count_text)
            return product_count[2]

        except TimeoutException:
            print('Timeout update_price_get_product_count')

    def cpi_site_get_product_count(self):
        try:
            time.sleep(5)
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div/i'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_product_count).text
            product_count = re.split(r'(of | entries)\s*', product_count_text)
            return product_count[2]

        except TimeoutException:
            print('Timeout update_price_get_product_count')

    def cpi_site_get_table_data_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_table))
            )

            cpi_site_update_price_table = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_table)
            rows = cpi_site_update_price_table.find_elements(By.TAG_NAME, 'tr')
            return len(rows)

        except TimeoutException:
            print('Timeout Update_Price_Table_Data_Count')

    def clear_filter(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_product_filter))
            )

            product_name_filter = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_product_filter)
            product_name_filter.clear()

            time.sleep(2)

        except TimeoutException:
            print('Timeout CPI_Site_Reset_Filter')

    def reset_slider_value(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[4]/div/div/div[5]'))
            )

            slider_first = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[4]/div/div/div[5]')
            slider_last = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[4]/div/div/div[6]')
            slider_first_source = self.driver.find_element(By.XPATH, '//*[@id="sliderFrom"]')
            slider_last_source = self.driver.find_element(By.XPATH, '//*[@id="sliderTo"]')

            # x = location.get('x')
            # y = location.get('y')
            move = ActionChains(self.driver)
            # move.click_and_hold(slider).move_by_offset(int(x/2), int(x/2)).release().perform()  # 10 offset = 40 space
            # move.drag_and_drop(slider_first, slider_first_source).release().perform()
            # move.drag_and_drop(slider_last, slider_last_source).release().perform()
            move.drag_and_drop_by_offset(slider_first, -100, 0).release().perform()
            move.drag_and_drop_by_offset(slider_first, -100, 0).release().perform()
            move.drag_and_drop_by_offset(slider_last, 100, 0).release().perform()
            move.drag_and_drop_by_offset(slider_last, 100, 0).release().perform()

        except TimeoutException:
            print('Timeout Reset_slider_value')


    def click_brand(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[3]/div/div/div'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[7]/span[1]/div/button'))
            )

            time.sleep(3)

            brand_button = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[7]/span[1]/div/button')
            brand_button.click()
            time.sleep(3)
            brand_button.click()

        except TimeoutException:
            print('Timeout click_brand')


    def get_competitor_report_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.competitor_report_platform))
            )

            time.sleep(10)
            platform = self.driver.find_element(By.XPATH, Locators.competitor_report_platform)
            platform.click()

            items = platform.find_elements_by_tag_name("option")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout competitor_platform')


    def select_platform(self, select_platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_platform))
            )

            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[2]'))
            )

            # clear_icon = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[2]')
            # clear_icon.click()

            platforms = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform)
            platforms.click()

            to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul/li/a/label[contains(text(),"'+str(select_platform)+'")]')
            to_select_platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="Filter"]'))
            )

            filter_button = self.driver.find_element(By.XPATH, '//*[@id="Filter"]')
            filter_button.click()

        except TimeoutException:
            print('Timeout CPI_Select_Platform')


    def only_switch_platform(self, select_platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_platform))
            )

            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[2]'))
            )

            platforms = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform)
            if platforms.text == select_platform:
                pass
            else:
                platforms.click()

                to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul/li/a/label[contains(text(),"'+str(select_platform)+'")]')
                to_select_platform.click()

        except TimeoutException:
            print('Timeout CPI_Select_Only_Platform')


    def cpi_site_select_platform(self, select_platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_platform))
            )

            platforms = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_platform)
            platforms.click()

            to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[2]/div/ul/li[2]/a/label[contains(text(),"'+str(select_platform)+'")]')
            to_select_platform.click()

        except TimeoutException:
            print('Timeout Select_Platform')

    def select_platform_competitor_report(self, select_platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.competitor_report_platform))
            )

            to_select_platform = self.driver.find_element(By.XPATH, '//select[@id="platforms"]/option[text()="'+str(select_platform)+'"]')
            to_select_platform.click()
            # '//*[@id="platforms"]/option[1]'

        except TimeoutException:
            print('Timeout Select_Competitor__Platform')

    def wait_comp_report_platform_load(self):
        try:
            WebDriverWait(self.driver, 50).until(
                EC.visibility_of_element_located((By.XPATH, Locators.first_item_competitor_report))
            )

        except TimeoutException:
            print('Timeout comp_report_product_load')

    def click_first_sku(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_first_sku))
            )

            first_sku = self.driver.find_element(By.XPATH, Locators.update_price_first_sku)
            first_sku.click()
            self.driver.switch_to.window(self.driver.window_handles[1])

        except TimeoutException:
            print('Timeout report_SKU')

    def wait_cpi_report_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="row1col2"]/div/div[1]/h3'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.report_loader_icon))
            )

        except TimeoutException:
            print('Timeout cpi_product_load')


    def get_on_hold_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi-filter-showOnHoldOnly"]'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi-filter-showOnHoldOnly"]/label[2]/span'))
            )

            slider = self.driver.find_element(By.XPATH, '//*[@id="cpi-filter-showOnHoldOnly"]/label[2]/span')
            slider.click()

            filter = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
            filter.click()

            time.sleep(2)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_product_count).text
            product_count = re.split(r'(of | entries)\s*', product_count_text)

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi-filter-showOnHoldOnly"]/label[2]/span'))
            )

            slider = self.driver.find_element(By.XPATH, '//*[@id="cpi-filter-showOnHoldOnly"]/label[2]/span')
            slider.click()

            time.sleep(2)

            filter = self.driver.find_element(By.XPATH, Locators.cpi_site_filter_button)
            filter.click()

            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

            return product_count[2]

        except TimeoutException:
            return 0

    def is_checked(self, driver, item):
        checked = driver.execute_script(("return document.getElementById('%s').checked") % item)
        return checked







