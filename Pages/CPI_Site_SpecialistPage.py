import time
import datetime
from Config import Database as db
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from selenium.common.exceptions import WebDriverException
from locators.Locators import Locators
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter


class cpisitespecialistclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('CPI_Specialist Page')
        self.dashboard = dashboardpageclass(self.driver)

    def check_load_message(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_load_message))
            )

            load_message = self.driver.find_element(By.XPATH, Locators.cpi_specialist_load_message)
            if load_message.get_attribute('textContent') == 'Processing...':
                self.reporter.append_row('CPI Specialist load message', 'OK')
                self.reporter.report()
                print('CPI Specialist load message is OK')
            else:
                self.reporter.append_row('CPI Specialist load message', 'NOT OK')
                self.reporter.report()
                print('CPI Specialist load message is NOT OK')

        except TimeoutException:
            print('Timeout CPI_Specialist_Load_Message')

    def wait_for_load(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.cpi_specialist_load_message))
            )

        except TimeoutException:
            print('Timeout CPI_Specialist_Load')

    def cpi_site_wait_for_load(self):
        try:
            time.sleep(5)

            WebDriverWait(self.driver, 600).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )

        except TimeoutException:
            print('Timeout CPI_SITE_CPI_Specialist_Filter_Load')

    def cpi_site_check_client_run(self, client_name, today_date):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div/i'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, Locators.cpi_site_cpi_first_client_name))
            )

            client_table = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[1]/td[2]')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="filter-elements"]/div/div[3]/span[1]/div/button'))
            )

            rundates_button = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[3]/span[1]/div/button')
            rundates_button.click()

            first_rundate = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[3]/span[1]/div/ul/li[3]')

            if client_name == client_table.text and first_rundate.text == today_date:
                return True
            else:
                return False

        except TimeoutException:
            print('Timeout cpi_site_check_client')

    def check_page_for_exclusive(self):
        if 'ExclusiveConcept' in self.driver.page_source:
            print('Exclusive Text is present please check')
            self.reporter.append_row('Exclusive Text', 'NOT OK')
            self.reporter.report()
        else:
            print('Exclusive Text not present')
            self.reporter.append_row('Exclusive Text', 'OK')
            self.reporter.report()

    def click_first_item(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_first_item))
            )

            first_item = self.driver.find_element(By.XPATH, Locators.cpi_specialist_first_item)
            first_item.click()

        except TimeoutException:
            print('Timeout CPI_Specialist_First_Item')

    def check_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_filter))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_specialist_filter)
            self.check_color(filter_button, 'CPI Specialist Filter Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_send_cpi))
            )

            send_cpi_button = self.driver.find_element(By.XPATH, Locators.cpi_specialist_send_cpi)
            self.check_color(send_cpi_button, 'CPI Specialist Send CPI Button')



        except TimeoutException:
            print('Timeout Button_color_check_cpispecialist')

    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + ' color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + ' color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')

    def select_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_rundate_selector))
            )

            run_date_selector = self.driver.find_element(By.XPATH, Locators.cpi_specialist_rundate_selector)
            run_date_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_first_rundate))
            )

            first_rundate = self.driver.find_element(By.XPATH, Locators.cpi_specialist_first_rundate)
            first_rundate.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_filter_button)
            filter_button.click()

        except TimeoutException:
            print('Timeout Rundate_Selector')

    def is_client_present(self, cpi_site_account_name):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi-list"]'))
            )
            spec_dashboard_table = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]')
            count = 0
            rows = spec_dashboard_table.find_elements(By.TAG_NAME, "tr")
            for row in rows:
                count = count + 1
            for i in range(24):
                account_text = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(i + 1) + ']/td[2]/a')
                if str(cpi_site_account_name) in account_text.text:
                    account_present = "True"
                    break
                else:
                    account_present = "False"
            if account_present == 'True':
                return 'True'
            else:
                print('This accounts run has not been completed!!! Please Check')
                return 'False'

        except TimeoutException:
            print('Timeout is_client_present')
            return 'False'

    def filter_client(self, account, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_platform))
            )

            client_platform_filter = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_platform)
            client_platform_filter.click()

            select_all_platform_reset = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_platform_select_all)
            select_all_platform_reset.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_platform_input))
            )

            platform_filter_input = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_platform_input)
            platform_filter_input.send_keys(platform)

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_platform_select))
            )

            time.sleep(2)

            select_platform = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[2]/span[1]/div/ul/li/a/label[@title="'+str(platform)+'"]')
            select_platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_client))
            )

            client_filter = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_client)
            client_filter.click()

            select_all_client_reset = self.driver.find_element(By.XPATH,
                                                               Locators.cpi_site_cpi_specialist_client_select_all)
            select_all_client_reset.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_client_input))
            )

            client_filter_input = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_client_input)
            client_filter_input.send_keys(account)

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_client_select))
            )

            time.sleep(2)

            select_client = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[1]/span[1]/div/ul/li/a/label[@title="'+str(account)+'"]')
            select_client.click()

            time.sleep(10)
            self.driver.save_screenshot("screenshot_specialist.png")
            try:
                WebDriverWait(self.driver, 600).until(
                    EC.element_to_be_clickable((By.XPATH, Locators.cpi_site_cpi_specialist_rundate_selector))
                )

                run_date_selector = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_rundate_selector)
                run_date_selector.click()

                WebDriverWait(self.driver, 30).until(
                        EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_first_rundate))
                )

                select_all_rundate = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_select_all_rundate)
                select_all_rundate.click()

                first_rundate = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_first_rundate)
                first_rundate.click()

            except TimeoutException:
                run_date_selector = self.driver.find_element(By.XPATH,Locators.cpi_site_cpi_specialist_rundate_selector)
                if run_date_selector.is_enabled() == False:
                    pass
                else:
                    print(run_date_selector.is_enabled())
                    print('Timeout filter_client_run_date')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_filter))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_filter)
            filter_button.click()

        except TimeoutException:
            print('Timeout filter_client')
            self.driver.save_screenshot('Timeout_filter_client.png')

    def filter_platform(self, account, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_client))
            )

            client_filter = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_client)
            client_filter.click()


            select_all_client_reset = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_client_select_all)
            select_all_client_reset.click()


            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_client_input))
            )

            client_filter_input = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_client_input)
            client_filter_input.send_keys(account)

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filter-elements"]/div/div[1]/span[1]/div/ul/li/a/label[@title="' + str(account) + '"]'))
            )

            time.sleep(2)

            select_client = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[1]/span[1]/div/ul/li/a/label[@title="' + str(account) + '"]')
            select_client.click()


            time.sleep(5)

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_platform))
            )

            platform_filter = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_platform)
            platform_filter.click()


            select_all_reset = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[2]/span[1]/div/ul/li[2]')
            select_all_reset.click()

            select_platform = self.driver.find_element(By.XPATH,
                                                       '//*[@id="filter-elements"]/div/div[2]/span[1]/div/ul/li/a/label[contains(text(),"' + platform + '")]')
            select_platform.click()

            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_site_cpi_specialist_rundate_selector))
            )

            run_date_selector = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_rundate_selector)
            run_date_selector.click()


            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_site_cpi_specialist_first_rundate))
            )

            select_all_rundate = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_select_all_rundate)
            select_all_rundate.click()

            first_rundate = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_first_rundate)
            first_rundate.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_filter))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_filter)
            filter_button.click()

        except TimeoutException:
            self.driver.save_screenshot("filter_timeout.png")
            print('Timeout filter_platform')

        except StaleElementReferenceException:
            self.driver.save_screenshot("filter_stale_issue.png")

    def click_account(self, cpi_site_account_name):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//td/a[contains(text(), "'+str(cpi_site_account_name)+'")]'))
            )

            account_select = self.driver.find_element(By.XPATH,
                                                      '//td/a[contains(text(), "'+str(cpi_site_account_name)+'")]')
            # self.driver.execute_script("arguments[0].scrollIntoView();", account_select)
            account_select.click()

        except TimeoutException:
            print('Timeout click_account')

    def get_total_product_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_total_product_count))
            )

            time.sleep(10)
            product_count_text = self.driver.find_elements(By.XPATH, Locators.cpi_specialist_total_product_count)
            return product_count_text[1].text.strip()
            # count = product_count_text[0].text.split('TOTAL ')
            # print(count[0])
            # print(count[1][0])
            # return product_count

        except TimeoutException:
            print('Timeout Specialist_product_count')

    def get_price_category_index(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )
            price_category = []
            current_run = []
            previous_run = []
            price_category_to_check = ['High Price Difference',
                                       'Product Not Found',
                                       'Competition Not Found',
                                       'URL Expired']
            price_category_to_check_index = []
            only_seller_index = []
            cpi_specialist_table = self.driver.find_element(By.XPATH, Locators.cpi_specialist_table)
            rows = cpi_specialist_table.find_elements(By.TAG_NAME, 'tr')
            for row in rows:
                col = row.find_elements(By.TAG_NAME, 'th')
                if len(col) != 0 and len(col) > 2:
                    price_category.append(col[0].text)
                    current_run.append(col[1].text)
                    previous_run.append(col[2].text)
                else:
                    price_category.append(col[0].text)
                    current_run.append(col[1].text)
                    previous_run.append(0)

            for i in price_category_to_check:
                if i in price_category:
                    price_category_to_check_index.append(price_category.index(i))
            if 'Only Seller' in price_category:
                only_seller_index.append(price_category.index('Only Seller'))
            return price_category_to_check, price_category_to_check_index, only_seller_index

        except TimeoutException:
            print('Timeout Get_Price_Category_Index')

    def get_table_data(self, price_category_to_check, price_category_to_check_index, only_seller_index, result_list, spec_report_product_count, check_data):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )

            current_run_data = []
            previous_run_data = []
            percent_change = []
            count = 0

            values = db.getStoreProdValue(check_data, 4)
            if 'Product Not Found' in price_category_to_check:
                index_to_check = price_category_to_check.index('Product Not Found')
                price_category_to_check.remove('Product Not Found')
                product_not_found_index = price_category_to_check_index[index_to_check]
                price_category_to_check_index.remove(product_not_found_index)

            for i in price_category_to_check_index:
                current_value = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(i) + ']/th[2]/span[1]')
                change_percent_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(
                    i) + ']/th[2]/span[2]/span[2]')
                try:
                    previous_value = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(i) + ']/th[3]/span[1]')
                except:
                    previous_value = change_percent_xpath

                current_run_data.append(current_value.text.strip())
                previous_run_data.append(previous_value.text.strip())
                percent_change = change_percent_xpath.text.strip()
                stored_prod_index = [x for x in values if price_category_to_check[count] in x][0]
                # if price_category_to_check[count] == 'Product Not Found':
                #     if previous_value.text == current_value.text:
                #         result_list.append(
                #             ['Percentage Change in Previous and Current Run of "Product Not Found"', 'PASS'])
                #     else:
                #         result_list.append(
                #             ['Percentage Change in Previous and Current Run of "Product Not Found"', 'FAIL',
                #              percent_change])
                #     break
                # print(values)
                # print(values.index(stored_prod_index))
                # print(values[values.index(stored_prod_index)][3])
                if values[values.index(stored_prod_index)][3] == 'N/A':
                    if previous_value.text == 0 and current_value.text != 0 and (int(current_value.text)/int(spec_report_product_count)) > 0.03:
                        result_list.append(
                            ['Percentage Change in Previous and Current Run of "' + str(price_category_to_check[count]) + '"',
                             'FAIL', percent_change, values[values.index(stored_prod_index)][3]])
                    else:
                        result_list.append(['Percentage Change in Previous and Current Run of "'+str(price_category_to_check[count])+ '"', 'PASS'])
                elif values[values.index(stored_prod_index)][3] is not None and float(values[values.index(stored_prod_index)][3]) > 2:
                    result_list.append(
                        ['Percentage Change in Previous and Current Run of "' + str(price_category_to_check[count])+ '"',
                         'FAIL', percent_change, values[values.index(stored_prod_index)][3]])
                else:
                    result_list.append(
                        ['Percentage Change in Previous and Current Run of "' + str(price_category_to_check[count])+ '"',
                         'PASS'])
                count = count + 1

            if product_not_found_index:
                current_value = self.driver.find_element(By.XPATH,
                                                         '//*[@id="cpi-list"]/tbody/tr[' + str(product_not_found_index) + ']/th[2]/span[1]')
                change_percent_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(product_not_found_index) + ']/th[2]/span[2]/span[2]')
                try:
                    previous_value = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(product_not_found_index) + ']/th[3]/span[1]')
                except:
                    previous_value = change_percent_xpath

                current_run_data.append(current_value.text.strip())
                previous_run_data.append(previous_value.text.strip())
                percent_change = change_percent_xpath.text.strip()
                if previous_value.text == current_value.text:
                    result_list.append(['Percentage Change in Previous and Current Run of "Product Not Found"', 'PASS'])
                else:
                    result_list.append(['Percentage Change in Previous and Current Run of "Product Not Found"', 'FAIL', percent_change])


            if only_seller_index:
                only_seller_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(
                    only_seller_index[0]) + ']/th[2]/span[2]/span[2]')
                try:
                    prev_only_seller_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(
                        only_seller_index[0]) + ']/th[3]/span[1]/a')
                except:
                    prev_only_seller_xpath = only_seller_xpath
                only_seller_percentage_change = only_seller_xpath.text.strip()
                only_seller_store_prod_index = [x for x in values if 'Only Seller' in x][0]
                if values[values.index(only_seller_store_prod_index)][3] == 'N/A':
                    if only_seller_xpath.text == 0 and  prev_only_seller_xpath.text != 0 and (int(only_seller_xpath.text)/int(spec_report_product_count)) > 0.03:
                        result_list.append(['Percentage Change in Previous and Current Run of "Only Seller"', 'FAIL'],  only_seller_percentage_change, values[values.index(only_seller_store_prod_index)][3])
                    else:
                        result_list.append(['Percentage Change in Previous and Current Run of "Only Seller"', 'PASS'])
                elif values[values.index(only_seller_store_prod_index)][3] is not None and -2 > float(values[values.index(only_seller_store_prod_index)][3]) > 2:
                    result_list.append(['Percentage Change in Previous and Current Run of "Only Seller"', 'FAIL',   only_seller_percentage_change, values[values.index(only_seller_store_prod_index)][3]])
                else:
                    result_list.append(['Percentage Change in Previous and Current Run of "Only Seller"', 'PASS'])

        except TimeoutException:
            print('Timeout SPECIALIST_GET_TABLE_DATA')

    def get_comp_report_product_data(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )
            price_category = []
            count = 0

            cpi_specialist_table = self.driver.find_element(By.XPATH, Locators.cpi_specialist_table)
            rows = cpi_specialist_table.find_elements(By.TAG_NAME, 'tr')
            for row in rows:
                col = row.find_elements(By.TAG_NAME, 'td')
                if len(col) != 0:
                    price_category.append(col[0].text)

            for i in price_category:
                if 'Cheapest' in i or 'Not Cheapest' in i or 'High Price Difference' in i:
                    index = price_category.index(i)
                    current_value = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(
                        index + 1) + ']/td[2]/span[1]')
                    count = count + int(current_value.text.strip())

            return count

        except TimeoutException:
            print('Timeout Get_Price_Category_Index')

    def get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi-list"]/thead/tr/th[2]'))
            )

            run_date = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/thead/tr/th[2]')
            return run_date.text

        except TimeoutException:
            print('Timeout run_date')

    def attribute_change_page(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )

            for i in range(20):
                get_product_attribute = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(i + 1) + ']/th[2]/span[1]/a')
                if int(get_product_attribute.text) != 0:
                    attribute = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(i + 1) + ']/th[2]/span[1]/a')
                    self.driver.execute_script("arguments[0].scrollIntoView();", attribute)
                    attribute.click()
                    self.driver.switch_to.window(self.driver.window_handles[2])
                    break

        except TimeoutException:
            print('Timeout Attribute_Change_Click_Page')

    def attribute_change_wait(self):
        try:
            time.sleep(15)

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div/i'))
            )

        except TimeoutException:
            print('Timeout CPI_SITE_Attribute_Change_Load_Wait')

    def data_present_attribute_change(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '/html/body/div/div[1]/div[2]/div[2]/div[1]/ul/li'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist_product_attribute_first_element))
            )
            first_element = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist_product_attribute_first_element)
            if first_element:
                return 'True'
            else:
                return 'False'
        except TimeoutException:
            return 'False'

