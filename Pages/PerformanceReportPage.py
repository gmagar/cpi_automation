import time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from locators.Locators import Locators
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter

class performancereportclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Performance_Report Page')
        self.dashboard = dashboardpageclass(self.driver)

    def wait_page_load(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.performance_report_first_item_action))
            )

        except TimeoutException:
            print('Timeout Performancereport_Load')

    def check_page_for_exclusive(self):
        if 'ExclusiveConcept' in self.driver.page_source:
            print('Exclusive Text is present please check')
            self.reporter.append_row('Exclusive Text', 'NOT OK')
            self.reporter.report()
        else:
            print('Exclusive Text not present')
            self.reporter.append_row('Exclusive Text', 'OK')
            self.reporter.report()

    def first_item_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.performance_report_first_item_action))
            )

            performance_report_first = self.driver.find_element(By.XPATH, Locators.performance_report_first_item_action)
            performance_report_first.click()

        except TimeoutException:
            print('Timeout Performace_first_item')

    def check_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.Execute_performance_report))
            )

            execute_button = self.driver.find_element(By.XPATH, Locators.Execute_performance_report)
            self.check_color(execute_button, 'Performance Report Execute_button')

        except TimeoutException:
            print('Timeout execute_button')


    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + ' color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + ' color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')

