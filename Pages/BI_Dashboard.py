import time
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from locators.Locators_Ganga import Locators
from Tests.utilities.reporter import Reporter
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from Config import Database as db


class bidashboardpageclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('BI Dashboard Page')

    def wait_dashboard_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_views)))

        except TimeoutException:
            print('Timeout Dashboard_Load')

    def switch_bi_to_cpi_button_click(self):

        try:
            time.sleep(5)
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.bi_to_cpi_switch_button))
            )

            cpi_switch_button = self.driver.find_element(By.XPATH, Locators.bi_to_cpi_switch_button)
            cpi_switch_button.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.bi_to_cpi_switch_link))
            )

            cpi_switch_link = self.driver.find_element(By.XPATH, Locators.bi_to_cpi_switch_link)
            cpi_switch_link.click()

        except TimeoutException:
            print('Timeout BI_TO_CPI_SITE_SWITCH')

    def switch_cpi_window(self):
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])

    def check_is_cpi_tab_open(self):
        try:
            "cpi.exclusiveconcepts.com" in self.driver.current_url
        except:
            print(str(Locators.account_name) + ' didnt open CPI tab')
            return 0

    def get_account_name_from_db(self, account_id):
        account_name = db.getMapVoilationSKU("""select Name from PIMstore where AccountId='"""+str(account_id)+"""';""")
        return account_name[0]


