import time
import re
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from locators.Locators_Ganga import Locators
from Tests.utilities.reporter import Reporter
from Config import Database as db

class dashboardpageclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Dashboard Page')

    def wait_loader(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.loader)))

        except TimeoutException:
            print('Timeout loader')

    def wait_report_loader(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_loader_retrieve_data)))

        except TimeoutException:
            print('Timeout report_loader')


    def check_loader_and_top_logo(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.mini_logo)))

            top_logo = self.driver.find_element(By.XPATH, Locators.mini_logo)
            if 'gbd-logo' in top_logo.get_attribute("src"):
                self.reporter.append_row('Mini logo', 'OK')
                self.reporter.report()
                print('Mini Logo OK')
            else:
                self.reporter.append_row('Mini logo', 'NOT OK')
                self.reporter.report()
                print('Mini Logo not same')

            loader_logo = self.driver.find_element(By.XPATH, Locators.loader_logo)
            if 'gbd-logo' in loader_logo.get_attribute("src"):
                self.reporter.append_row('Loader Logo', 'OK')
                self.reporter.report()
                print('Loader Logo OK')
            else:
                self.reporter.append_row('Loader Logo', 'NOT OK')
                self.reporter.report()
                print('Loader Logo not same')

            loader_icon = self.driver.find_element(By.XPATH, Locators.loader_loading_icon)
            if '10px solid rgb(175, 0, 0)' in loader_icon.value_of_css_property('border-top'):
                self.reporter.append_row('Loader loading Color', 'OK')
                self.reporter.report()
                print('Loader loading color is OK')
            else:
                self.reporter.append_row('Loader loading Color', 'NOT OK')
                self.reporter.report()
                print('Loader loading color is Not OK')

        except TimeoutException:
            print('Timeout report_load')

    def check_loader_loading_icon(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.loader_loading_icon)))

            loader_icon = self.driver.find_element(By.XPATH, Locators.loader_loading_icon)
            print(loader_icon.value_of_css_property('border-top'))
            if '#af0000' in loader_icon.value_of_css_property('border-top'):
                print('Loader loading color is OK')
            else:
                print('Loader loading color is Not OK')

        except TimeoutException:
            print('Timeout Loader_loading_icon')

    def wait_retrieve_report_loader(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_loader_retrieve_data)))

        except TimeoutException:
            print('Timeout report_load')

    def wait_dashboard_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.report_views)))

        except TimeoutException:
            print('Timeout Dashboard_Load')

    def check_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.go_button_dashboard)
            ))

            go_button = self.driver.find_element(By.XPATH, Locators.go_button_dashboard)
            self.check_color(go_button , 'Go Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.search_report_button)
            ))

            search_button = self.driver.find_element(By.XPATH, Locators.search_report_button)
            self.check_color(search_button, 'Search Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.execute_button)
            ))

            execute_button = self.driver.find_element(By.XPATH, Locators.execute_button)
            self.check_color(execute_button, 'Execute Button')


        except TimeoutException:
            print('Timeout dashboard_buttons')

    def check_page_for_exclusive(self):
        if 'ExclusiveConcept' in self.driver.page_source:
            print('Exclusive Text is present please check')
            self.reporter.append_row('Exclusive Text', 'NOT OK')
            self.reporter.report()
        else:
            print('Exclusive Text not present')
            self.reporter.append_row('Exclusive Text', 'OK')
            self.reporter.report()
        # src = self.driver.page_source
        # text_found = re.search(r'exclusiveconcept', src)
        # self.assertNotEqual(text_found, None)

    def cpi_side_bar_click(self):
        try:
            time.sleep(10)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-competitors-list_processing"]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_side_bar)))

            cpi_button = self.driver.find_element(By.XPATH, Locators.cpi_side_bar)
            # self.driver.execute_script("window.scrollTo(0, 200)")
            actions = ActionChains(self.driver)
            actions.move_to_element(cpi_button).perform()
            self.driver.execute_script("arguments[0].scrollIntoView();", cpi_button)
            actions.move_to_element(cpi_button).perform()

        except TimeoutException:
            print('Timeout cpi_side_bar click')

    def cpi_site_cpi_side_bar_click(self):
        try:
            time.sleep(10)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-competitors-list_processing"]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_site_cpi_side_bar)))

            cpi_button = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_side_bar)
            # self.driver.execute_script("window.scrollTo(0, 200)")
            actions = ActionChains(self.driver)
            actions.move_to_element(cpi_button).perform()
            self.driver.execute_script("arguments[0].scrollIntoView();", cpi_button)
            actions.move_to_element(cpi_button).perform()

        except TimeoutException:
            print('Timeout cpi_site__side_bar click')

    def reports_side_bar_click(self):
        try:
            time.sleep(10)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-competitors-list_processing"]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.report_dashboard_bar)))

            report_button = self.driver.find_element(By.XPATH, Locators.report_dashboard_bar)
            # actions = ActionChains(self.driver)
            # actions.move_to_element(report_button).perform()
            report_button.click()

        except TimeoutException:
            print('Timeout reports_side_bar click')


    def account_side_bar_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.account))
            )

            account_page = self.driver.find_element(By.XPATH, Locators.account)
            account_page.click()

        except TimeoutException:
            print('Timeout account_click')

    def competitor_report_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.competitorsreport)))

            competitor_report_button = self.driver.find_element(By.XPATH, Locators.competitorsreport)
            self.driver.execute_script("arguments[0].scrollIntoView();", competitor_report_button)
            competitor_report_button.click()

        except TimeoutException:
            print('Timeout Competitor_report')

    def update_prices_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_prices))
            )

            client_info = self.driver.find_element(By.XPATH, '/html/body/div[1]/aside/section/ul/li[8]/ul/li[1]/a')
            actions = ActionChains(self.driver)
            actions.move_to_element(client_info).perform()

            update_prices = self.driver.find_element(By.XPATH, Locators.update_prices)
            self.driver.execute_script("arguments[0].scrollIntoView();", update_prices)
            update_prices.click()

        except TimeoutException:
            print('Timeout Update_Prices')

    def performance_report_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.performance_report))
            )

            performance_report = self.driver.find_element(By.XPATH, Locators.performance_report)
            performance_report.click()

        except TimeoutException:
            print('Timeout Performance_Report')

    def cpi_specialist_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist))
            )

            cpi_specialist = self.driver.find_element(By.XPATH, Locators.cpi_specialist)
            cpi_specialist.click()

        except TimeoutException:
            print('Timeout cpi_specialist_click')


    def cpi_site_cpi_specialist_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_cpi_specialist))
            )

            cpi_specialist = self.driver.find_element(By.XPATH, Locators.cpi_site_cpi_specialist)
            cpi_specialist.click()

        except TimeoutException:
            print('Timeout cpi_site_cpi_specialist_click')

    def wait_internal_report(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_all_elements_located((By.XPATH, Locators.internal_report_first_element))
            )
        except TimeoutException:
            print('Timeout Internal_Report')

    def filter_cpi_report(self, report_name):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, Locators.internal_report_filter_report_name))
            )

            filter_report = self.driver.find_element(By.XPATH, Locators.internal_report_filter_report_name)
            filter_report.send_keys(report_name)

        except TimeoutException:
            print('Timeout Internal_Report_Filter_Report')

    def click_cpi_delievery_status(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, Locators.internal_report_first_element))
            )

            click_delievery_status = self.driver.find_element(By.XPATH, Locators.internal_report_first_element)
            click_delievery_status.click()

        except TimeoutException:
            print('Timeout CPI_Delievery_Status')

    def wait_cpi_delivery_status_report(self):
        try:
            time.sleep(2)
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.delievery_status_wait))
            )

        except TimeoutException:
            print('Timeout WAIT_CPI_DELIEVERY_STATUS_REPORT')

    def delivery_status_filter_last_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, Locators.internal_report_last_run_date))
            )

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.delievery_status_wait))
            )

            last_run_date = self.driver.find_element(By.XPATH, Locators.internal_report_last_run_date)
            actions = ActionChains(self.driver)
            actions.move_to_element(last_run_date).perform()
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, Locators.internal_report_last_run_date_filter))
            )
            last_run_date_filter = self.driver.find_element(By.XPATH, Locators.internal_report_last_run_date_filter)
            last_run_date_filter.click()

        except TimeoutException:
            print('Timeout Delivery_status_filter_last_run')

    def get_client_status(self, account_name):
        try:
            # WebDriverWait(self.driver, 300).until(
            #     EC.invisibility_of_element_located((By.XPATH, Locators.delievery_status_wait))
            # )
            #
            # WebDriverWait(self.driver, 30).until(
            #     EC.element_to_be_clickable((By.XPATH, Locators.cpi_delievery_status_table))
            # )
            #
            # client_table = self.driver.find_elements(By.XPATH, Locators.cpi_delievery_status_table)
            # rows = client_table.find_elements(By.TAG_NAME, 'tr')
            # for row in rows:
            #     if account_name in row:
            #         print(row)
            print('akjaskdjfkasklf')

        except TimeoutException:
            print('Timeout CPI_DELEIVERY_STATUS_TABLE')

    def internal_report_client(self, account_name):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.delievery_status_wait))
            )

            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_delievery_status_table))
            )

            client_table = self.driver.find_element(By.XPATH, Locators.cpi_delievery_status_table)
            rows = client_table.find_elements(By.TAG_NAME, "tr")
            client_data = []
            client_progress = []
            for row in rows:
                if account_name in row.text:
                    col_platform = row.find_elements(By.TAG_NAME, "td")[1]
                    col_last_run_date = row.find_elements(By.TAG_NAME, "td")[4]
                    col_progress = row.find_elements(By.TAG_NAME, "td")[6]
                    client_data.append(col_platform.text)
                    client_data.append(col_last_run_date.text)
                    client_data.append(col_progress.text)
                    if col_last_run_date.text == str(datetime.date.today().strftime("%b %d, %Y")):
                        client_progress.append(client_data)
                    client_data = []
            return client_progress

        except TimeoutException:
            print('Timeout CPI_DELEIVERY_STATUS_TABLE')

    def cpi_site_account_name(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="topStoreSelector"]'))
            )

            cpi_site_account_name = self.driver.find_element(By.XPATH, '//*[@id="topStoreSelector"]').text
            return cpi_site_account_name

        except TimeoutException:
            print('Timeout cpi_site_account_name')

    def cpi_switch_button_click(self):
        try:
            time.sleep(10)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-competitors-list_processing"]'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_switch_button))
            )

            cpi_switch_button = self.driver.find_element(By.XPATH, Locators.cpi_switch_button)
            cpi_switch_button.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_switch_link))
            )

            cpi_switch_link = self.driver.find_element(By.XPATH, Locators.cpi_switch_link)
            cpi_switch_link.click()

        except TimeoutException:
            print('Timeout CPI_SITE_SWITCH')

    def switch_cpi_window(self):
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])

    def check_is_cpi_tab_open(self):
        try:
            "cpi.exclusiveconcepts.com" in self.driver.current_url
        except:
            print(str(Locators.account_name) + ' didnt open CPI tab')
            return 0

    def get_account_name_from_db(self, account_id):
        account_name = db.getMapVoilationSKU("""
                                        select Name from PIMstore 
                                        where AccountId='"""+str(account_id)+"""';
                                        """)
        return account_name[0]


    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + ' color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + ' color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')