import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains

from locators.Locators_Ganga import Locators
from selenium.common.exceptions import TimeoutException
from Tests.utilities.reporter import Reporter
from selenium.webdriver.support.select import Select


class cpiInsightclass():

    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Insight Page')

    def switch_insight_page(self):
        try:

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_reports"]/a'))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, '//*[@id="account_reports"]/a')
            dropdown_menu.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_ul"]/li[3]/a[contains(text(),"Insights")]'))
            )

            insight = self.driver.find_element(By.XPATH, '//*[@id="account_ul"]/li[3]/a[contains(text(),"Insights")]')
            insight.click()


        except TimeoutException:
            print('Timeout switch_insight_page')

    def get_total_product(self, platform, update_price_check, count):
        try:
            # '//*[@id="block-row1col1"]/div/div/div/div/div/table/tfoot/tr/td[2]/a
            # WebDriverWait(self.driver, 30).until(
            #     EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[1]/table/thead/tr/td[2]'))
            # )
            #
            # platform_go = self.driver.find_element(By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[1]/table/thead/tr/td[2]')
            # platform_go = platform_go.text
            # platform_google = platform_go.replace(platform_go, 'Google')
            #
            # WebDriverWait(self.driver, 30).until(
            #     EC.visibility_of_element_located(
            #         (By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[2]/table/thead/tr/td[2]'))
            # )
            # platform_am = self.driver.find_element(By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[2]/table/thead/tr/td[2]')
            # platform_amazon = platform_am.text
            # print(platform_amazon)
            # if platform_google == platform:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[1]/table/tfoot/tr/td[2]/a'))
            )
            total_item = self.driver.find_element(By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[1]/table/tfoot/tr/td[2]/a')
            # print("if", total_item.text)
            if total_item.text == count:
                update_price_check.append([platform + ': Total item in Insights', 'PASS'])
            else:
                update_price_check.append([platform + ': Total item in Insights', 'FAIL'])

            # elif platform_amazon == platform:
            #     WebDriverWait(self.driver, 30).until(
            #         EC.visibility_of_element_located(
            #             (By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[2]/table/tfoot/tr/td[2]/a'))
            #     )
            #     total_item = self.driver.find_element(By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div[2]/table/tfoot/tr/td[2]/a')
            #     print("else", total_item.text)
            #     if total_item.text == count:
            #         update_price_check.append([platform+': Total item in Insights', 'Pass'])
            #     else:
            #         update_price_check.append([platform+': Total item in Insights', 'FAIL'])

            # else:
                # update_price_check.append(['TABLE FOR ['+platform+'] NOT FOUND', 'NOT FOUND'])

        except TimeoutException:
            print('Timeout get_total_product')
