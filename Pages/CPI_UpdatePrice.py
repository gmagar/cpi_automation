import time
import re
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from Config import Database as db
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, WebDriverException, NoSuchElementException
from locators.Locators_Ganga import Locators
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter


class cpiupdatepricepageclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Update_Prices Page')

    def switch_to_update_price_menu(self):
        try:

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_report_name"]'))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, '//*[@id="account_report_name"]')
            dropdown_menu.click()
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_ul"]/li[1]/a[contains(text(),"Update Prices")]'))
            )

            update_price_menu = self.driver.find_element(By.XPATH,
                                                          '//*[@id="account_ul"]/li[1]/a[contains(text(),"Update Prices")]')
            update_price_menu.click()
        except TimeoutException:
            print('Timeout switch_to_update_price_menu')

    def get_all_platform(self):

        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_update_price_platform))
            )


            time.sleep(10)
            platform = self.driver.find_element(By.XPATH, Locators.cpi_update_price_platform)
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_update_price_platform_ul))
            )

            platform_list = self.driver.find_element(By.XPATH, Locators.cpi_update_price_platform_ul)
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout cpi_site_platform')

    def cpi_switch_platform(self, select_platform):
        # print("Platform: ", select_platform)
        try:

            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_update_price_platform))
            )

            platforms = self.driver.find_element(By.XPATH, Locators.cpi_update_price_platform)
            # print('Current platform: ', platforms.text)
            self.driver.save_screenshot(select_platform + '_detail.png')

            if platforms.text != select_platform:
                # print('Changing Platform....')
                self.driver.execute_script("arguments[0].click();", platforms)
                # print('Platform Click')
                to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul/li/a/label[contains(text(),"' + str(select_platform) + '")]')
                to_select_platform.click()

                cpi_filter = self.driver.find_element(By.XPATH, Locators.filter_button)
                cpi_filter.click()
                # print('Filter click[Platform change] to: ', to_select_platform)
                time.sleep(5)
                self.driver.save_screenshot(select_platform+'_detail.png')
            else:
                pass

        except TimeoutException:
            print('Timeout CPI_Select_Platform')

    def cpi_get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_run_date))
            )
            run_date = self.driver.find_element(By.XPATH, Locators.cpi_run_date)
            return run_date.text
        except TimeoutException:
            print('Timeout get_run_date')

    def filter_cheapest_noncheapest_raise(self, parameter_to_click):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter))
            )
            price_category_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter)
            # price_category_xpath.click()

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter_select_all))
            )

            select_all_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter_select_all)

            if select_all_xpath.is_selected() is True:
                select_all_xpath.click()

            else:
                select_all_xpath.click()
                time.sleep(2)
                select_all_xpath.click()

            time.sleep(5)

            for i in parameter_to_click:
                parameter_xpath = self.driver.find_element(By.XPATH,
                                                         '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/ul/li/a/label[contains(text(), "'+str(i)+'")]')
                parameter_xpath.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.filter_button))
            )

            filter_xpath = self.driver.find_element(By.XPATH, Locators.filter_button)
            filter_xpath.click()

        except TimeoutException:
            print('Timeout filter_cheapest_non-cheapest_raise')

    def cpi_product_count(self):
        # print('cpi_prouduct_count in update price')
        try:

            time.sleep(5)
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter))
            )
            price_category_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter)
            time.sleep(1)
            # price_category_xpath.click()
            self.driver.execute_script("arguments[0].click();", price_category_xpath)

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_category_filter_select_all))
            )
            select_all_xpath = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_category_filter_select_all)
            time.sleep(2)
            if select_all_xpath.is_selected() is True:
                select_all_xpath.click()

            else:
                select_all_xpath.click()
                time.sleep(2)
                select_all_xpath.click()

            time.sleep(5)
            # print('test1')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_update_price_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.cpi_site_update_price_product_count).text
            # print(product_count_text)
            product_count = re.split(r'(of | entries)\s*', product_count_text)
            return product_count[2]

        except TimeoutException:
            print('Timeout update_price_get_product_count')

    def get_clickable_sku_index(self):
        i = 1
        while i < 30:
            try:
                WebDriverWait(self.driver, 30).until(
                    EC.visibility_of_element_located(
                        (By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr[' + str(i) + ']/td[2]/a'))
                )
                return i
            except TimeoutException:
                i = i + 1
                pass
        return i

    def get_first_sku_name(self, clickable_sku_index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]/a'))
            )

            first_sku = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]/a')
            return first_sku.text

        except TimeoutException:
            print('Timeout SKU Read')

    def cpi_site_click_first_sku(self, clickable_sku_index, update_price_check, platform):
        try:

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[3]/a'))
            )


            first_sku = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr[' + str(
                clickable_sku_index) + ']/td[3]/a')
            first_sku.click()

            self.driver.switch_to.window(self.driver.window_handles[1])
            self.driver.save_screenshot(platform + "_sku_detail.png")
            if platform.lower() in self.driver.current_url:
                update_price_check.append(['Redirection to Detail Page on click SKU', 'PASS'])
            else:
                update_price_check.append(['Redirection to Detail Page on click SKU', 'Fail'])

            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])
            # self.driver.save_screenshot('Return_page.png')
        except TimeoutException:
            print('Timeout CPI_SKU')

    def cpi_site_click_first_item(self, clickable_sku_index, update_price_check, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[2]/a'))
            )

            updateprice_first_item = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[2]/a')
            updateprice_first_item.click()
            self.driver.switch_to.window(self.driver.window_handles[1])
            time.sleep(1)
            if platform.lower() in self.driver.current_url:
                self.driver.save_screenshot("item_detail.png")
                update_price_check.append(['Redirection to Item Page on click Item', 'PASS'])
            else:
                update_price_check.append(['Redirection to Item Page on click Item', 'Fail'])

            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])

        except TimeoutException:
            print('Timeout cpi_site_click_first_item')

    def check_first_item_cost(self, clickable_sku_index, update_price_check, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[5]'))
            )

            item_cost = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[5]')

            if item_cost.text != None:
                update_price_check.append(['Check Cost of Item', 'PASS'])
            else:
                update_price_check.append(['Check Cost of Item', 'FAIL'])

        except TimeoutException:
            print('Timeout check_first_item_cost')

    def check_first_item_floor_price(self, clickable_sku_index, update_price_check, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[6]'))
            )

            floor_price = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[6]')
            if floor_price.text != None:
                update_price_check.append(['Check Floor Price of Item', 'PASS'])
            else:
                update_price_check.append(['Check Floor Price of Item', 'FAIL'])

        except TimeoutException:
            print('Timeout check_first_item_floor_price')

    def check_map_violation_download_option(self, clickable_sku_index, update_price_check, platform):
        try:

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[7]/a/i'))
            )

            map_violation_download_icon = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[7]/a/i')


            if map_violation_download_icon != None:
                map_violation_download_icon.click()
                time.sleep(10)
                update_price_check.append(['Map Violation of Item downloaded', 'PASS'])
            else:
                update_price_check.append(['Map Violation of Item downloaded', 'Fail'])

        except TimeoutException:
            update_price_check.append(['Map Violation of Item downloaded', 'No Download Option'])

    def check_first_item_current_price(self, clickable_sku_index, update_price_check, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[8]'))
            )

            update_price_current_price = self.driver.find_element(By.XPATH,
                                                                   '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[8]')
            if update_price_current_price.text != None:
                update_price_check.append(['Current Price of Item', 'PASS'])


        except TimeoutException:
            print('Timeout check_first_item_current_price')

    def check_first_item_suggested_price(self, clickable_sku_index, update_price_check, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[1]/div/div/div[1]/table/tbody/tr['+str(clickable_sku_index)+']/td[10]'))
            )

            update_price_suggested_price = self.driver.find_element(By.XPATH,
                                                                  '//*[@id="hot"]/div[1]/div/div/div[1]/table/tbody/tr['+str(clickable_sku_index)+']/td[10]')
            if update_price_suggested_price.text != None:
                update_price_check.append(['Suggested Price of Item', 'PASS'])


        except TimeoutException:
            print('Timeout check_first_item_suggested_price')

    def click_seller_info(self, update_price_check, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr[3]/td[3]/a'))
            )
            sku_name_xpath = self.driver.find_element(By.XPATH,
                                                      '//*[@id="hot"]/div[4]/div/div/div/table/tbody/tr[3]/td[3]/a')
            sku_name = sku_name_xpath.text
            print('SKU_NAME:', sku_name)
            # seller_table = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[2]/div/div/div/table/thead/tr/th[14]')
            seller_info = self.driver.find_element(By.XPATH, '//*[@id="hot"]/div[1]/div/div/div/table/tbody/tr[3]/td[14]/a')
            # print(seller_info.text)
            # self.driver.execute_script("arguments[0].scrollIntoView();", seller_table)
            time.sleep(1)
            if seller_info.text != '-':
                print("Found data", seller_info.text)
                self.driver.execute_script("arguments[0].scrollIntoView();", seller_info)
                # self.driver.save_screenshot(platform +": screenshot_seller_click_issue.png")
                self.driver.switch_to.window(self.driver.window_handles[1])
                self.driver.save_screenshot(platform + ": screenshot_seller_click_issue_2.png")
                if 'CompetitorsReport' in self.driver.current_url and sku_name in self.driver.current_url:
                    update_price_check.append(['Redirection of Seller_Info', 'PASS'])
                else:
                    update_price_check.append(['Redirection of Seller_Info', 'FAIL'])

                self.driver.close()
                self.driver.switch_to.window(self.driver.window_handles[0])
            else:
                print('No Seller info')
                breakpoint()
                update_price_check.append(['Seller info', 'NOT Present(Blank -)'])
        except TimeoutException:
            pass
            print('Timeout Click_Seller_Info')
