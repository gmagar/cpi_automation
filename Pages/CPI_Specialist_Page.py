import time
from tabulate import tabulate
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from selenium.common.exceptions import WebDriverException
from locators.Locators import Locators
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter

class cpispecialistclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('CPI_Specialist Page')
        self.dashboard = dashboardpageclass(self.driver)

    def check_load_message(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_load_message))
            )

            load_message = self.driver.find_element(By.XPATH, Locators.cpi_specialist_load_message)
            if load_message.get_attribute('textContent') == 'Processing...':
                self.reporter.append_row('CPI Specialist load message', 'OK')
                self.reporter.report()
                print('CPI Specialist load message is OK')
            else:
                self.reporter.append_row('CPI Specialist load message', 'NOT OK')
                self.reporter.report()
                print('CPI Specialist load message is NOT OK')

        except TimeoutException:
            print('Timeout CPI_Specialist_Load_Message')

    def wait_for_load(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.cpi_specialist_load_message))
            )

        except TimeoutException:
            print('Timeout CPI_Specialist_Load')

    def cpi_site_wait_for_load(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="cpi-list"]'))
            )

        except TimeoutException:
            print('Timeout CPI_SITE_CPI_Specialist_Load')

    def cpi_site_check_client_run(self, client_name):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_all_elements_located((By.XPATH, Locators.cpi_site_cpi_first_client_name))
            )

            if client_name in self.driver.page_source:
                return True
            else:
                return False

        except TimeoutException:
            print('Timeout cpi_site_check_client')

    def check_page_for_exclusive(self):
        if 'ExclusiveConcept' in self.driver.page_source:
            print('Exclusive Text is present please check')
            self.reporter.append_row('Exclusive Text', 'NOT OK')
            self.reporter.report()
        else:
            print('Exclusive Text not present')
            self.reporter.append_row('Exclusive Text', 'OK')
            self.reporter.report()

    def click_first_item(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_first_item))
            )

            first_item = self.driver.find_element(By.XPATH, Locators.cpi_specialist_first_item)
            first_item.click()

        except TimeoutException:
            print('Timeout CPI_Specialist_First_Item')

    def check_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_filter))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_specialist_filter)
            self.check_color(filter_button , 'CPI Specialist Filter Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_send_cpi))
            )

            send_cpi_button = self.driver.find_element(By.XPATH, Locators.cpi_specialist_send_cpi)
            self.check_color(send_cpi_button, 'CPI Specialist Send CPI Button')



        except TimeoutException:
            print('Timeout Button_color_check_cpispecialist')

    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + ' color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + ' color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')

    def select_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_rundate_selector))
            )

            run_date_selector = self.driver.find_element(By.XPATH, Locators.cpi_specialist_rundate_selector)
            run_date_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_first_rundate))
            )

            first_rundate = self.driver.find_element(By.XPATH, Locators.cpi_specialist_first_rundate)
            first_rundate.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_filter_button)
            filter_button.click()

        except TimeoutException:
            print('Timeout Rundate_Selector')

    def is_client_present(self):
        try:
            spec_dashboard_table = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]')
            count = 0
            rows = spec_dashboard_table.find_elements(By.TAG_NAME, "tr")
            for row in rows:
                count = count + 1
            for i in range(count):
                account_text = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr['+str(i+1)+']/td[2]/a')
                if str(Locators.account_id) in account_text.get_attribute('href'):
                    account_present = "True"
                    break
                else:
                    account_present = "False"
            if account_present == 'True':
                return 'True'
            else:
                print('This accounts run has not been completed!!! Please Check')
                return 'False'

        except TimeoutException:
            print('Timeout is_client_present')

    def filter_platform(self, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_platform))
            )

            platform_filter = self.driver.find_element(By.XPATH, Locators.cpi_specialist_platform)
            platform_filter.click()

            select_all_reset = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[2]/span[1]/div/ul/li[2]')
            select_all_reset.click()

            select_platform = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div/div[2]/span[1]/div/ul/li[3]/a/label[contains(text(),"'+str(platform)+'")]')
            select_platform.click()

            time.sleep(5)

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_rundate_selector))
            )

            run_date_selector = self.driver.find_element(By.XPATH, Locators.cpi_specialist_rundate_selector)
            run_date_selector.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_first_rundate))
            )

            select_all_rundate = self.driver.find_element(By.XPATH, Locators.cpi_specialist_select_all_rundate)
            select_all_rundate.click()

            first_rundate = self.driver.find_element(By.XPATH, Locators.cpi_specialist_first_rundate)
            first_rundate.click()
            
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_filter))
            )
            
            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_specialist_filter)
            filter_button.click()

        except TimeoutException:
            print('Timeout filter_platform')

    def click_account(self):
        try:
            WebDriverWait(self.driver,30).until(
                EC.visibility_of_element_located((By.XPATH, '//a[contains(@href, "'+str(Locators.account_id)+'")]'))
            )

            account_select = self.driver.find_element(By.XPATH, '//a[contains(@href, "'+str(Locators.account_id)+'")]')
            self.driver.execute_script("arguments[0].scrollIntoView();", account_select)
            account_select.click()

        except TimeoutException:
            print('Timeout click_account')

    def get_total_product_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_total_product_count))
            )

            time.sleep(10)
            product_count_text = self.driver.find_elements(By.XPATH, Locators.cpi_specialist_total_product_count)
            return product_count_text[1].text.strip()
            # count = product_count_text[0].text.split('TOTAL ')
            # print(count[0])
            # print(count[1][0])
            # return product_count

        except TimeoutException:
            print('Timeout Specialist_product_count')

    def get_price_category_index(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )
            price_category = []
            current_run = []
            previous_run = []
            price_category_to_check = ['High Price Difference',
                                       'No Matching Product Found',
                                       'No Sellers Found',
                                       'Not able to collect Product Data']
            price_category_to_check_index = []
            only_seller_index = []
            cpi_specialist_table = self.driver.find_element(By.XPATH, Locators.cpi_specialist_table)
            rows = cpi_specialist_table.find_elements(By.TAG_NAME, 'tr')
            for row in rows:
                col = row.find_elements(By.TAG_NAME, 'td')
                if len(col) != 0:
                    price_category.append(col[0].text)
                    current_run.append(col[1].text)
                    previous_run.append(col[2].text)

            for i in price_category_to_check:
                if i in price_category:
                    price_category_to_check_index.append(price_category.index(i))
            if 'Only Seller' in price_category:
                only_seller_index.append(price_category.index('Only Seller'))
            return price_category_to_check, price_category_to_check_index, only_seller_index
        except TimeoutException:
            print('Timeout Get_Price_Category_Index')

    def get_table_data(self, price_category_to_check, price_category_to_check_index, only_seller_index, result_list):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )

            current_run_data = []
            previous_run_data = []
            percent_change = []
            count = 0
            print_tabulate = []

            for i in price_category_to_check_index:
                current_value = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr['+str(i+1)+']/td[2]/span[1]')
                change_percent_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr['+str(i+1)+']/td[2]/span[3]')
                previous_value = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr['+str(i+1)+']/td[3]/span[1]')
                current_run_data.append(current_value.text.strip())
                previous_run_data.append(previous_value.text.strip())
                percent_change = change_percent_xpath.text.strip()
                if percent_change.split('%')[0] == 'N/A':
                    # print('Percentage Change in Previous and Current Run of "'+str(price_category_to_check[count])+'" is N/A.')
                    result_list.append(['Percentage Change in Previous and Current Run of "'+str(price_category_to_check[count]), 'N/A'])
                elif float(percent_change.split('%')[0]) > 50:
                    # print('Percentage Change in Previous and Current Run of "'+str(price_category_to_check[count])+'" is FAIL.')
                    result_list.append(
                        ['Percentage Change in Previous and Current Run of "' + str(price_category_to_check[count]),
                         'FAIL'])
                else:
                    # print('Percentage Change in Previous and Current Run of "'+str(price_category_to_check[count])+'" is PASS.')
                    result_list.append(
                        ['Percentage Change in Previous and Current Run of "' + str(price_category_to_check[count]),
                         'PASS'])
                count = count + 1

            if only_seller_index:
                only_seller_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr['+str(only_seller_index[0]+1)+']/td[2]/span[3]')
                only_seller_percentage_change = only_seller_xpath.text.strip()
                if only_seller_percentage_change.split('%')[0] == 'N/A':
                    # print('Percentage Change in Previous and Current Run of "Only Seller" is N/A.')
                    result_list.append(['Percentage Change in Previous and Current Run of "Only Seller"', 'N/A'])
                elif -50 > float(only_seller_percentage_change.split('%')[0]) > 50:
                    # print('Percentage Change in Previous and Current Run of "Only Seller" is FAIL.')
                    result_list.append(['Percentage Change in Previous and Current Run of "Only Seller"', 'FAIL'])
                else:
                    # print('Percentage Change in Previous and Current Run of "Only Seller" is PASS.')
                    result_list.append(['Percentage Change in Previous and Current Run of "Only Seller"', 'PASS'])

            print(tabulate(print_tabulate))

        except TimeoutException:
            print('Timeout SPECIALIST_GET_TABLE_DATA')

    def get_comp_report_product_data(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )
            price_category = []
            count = 0

            cpi_specialist_table = self.driver.find_element(By.XPATH, Locators.cpi_specialist_table)
            rows = cpi_specialist_table.find_elements(By.TAG_NAME, 'tr')
            for row in rows:
                col = row.find_elements(By.TAG_NAME, 'td')
                if len(col) != 0:
                    price_category.append(col[0].text)

            for i in price_category:
                if 'Cheapest' in i or 'Not Cheapest' in i or 'High Price Difference' in i:
                    index = price_category.index(i)
                    current_value = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr[' + str(index + 1) + ']/td[2]/span[1]')
                    count = count + int(current_value.text.strip())

            return count

        except TimeoutException:
            print('Timeout Get_Price_Category_Index')


    def get_run_date(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi-list"]/thead/tr/th[2]'))
            )

            run_date = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/thead/tr/th[2]')
            return run_date.text

        except TimeoutException:
            print('Timeout run_date')

    def attribute_change_page(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )

            for i in range(10):
                get_product_attribute = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr['+str(i+1)+']/td[2]/span[1]')
                if int(get_product_attribute.text) != 0:
                    attribute = self.driver.find_element(By.XPATH, '//*[@id="cpi-list"]/tbody/tr['+str(i+1)+']/td[2]/span[1]/a')
                    attribute.click()
                    break

        except TimeoutException:
            print('Timeout Attribute_Change_Click_Page')

    def attribute_change_wait(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_product_attribute_wait))
            )

        except TimeoutException:
            print('Timeout Attribute_Change_Load_Wait')

    def data_present_attribute_change(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_product_attribute_wait))
            )

            first_element = self.driver.find_element(By.XPATH, Locators.cpi_specialist_product_attribute_first_element)
            if first_element:
                return 'True'
            else:
                return 'False'
        except TimeoutException:
            print('Timeout Attribute_Change_Load')

    def wait_specialist_detail(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_specialist_table))
            )

        except TimeoutException:
            print('Timeout Specialist_Detail')

