import time
import re
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from Config import Database as db
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, WebDriverException, NoSuchElementException
from locators.Locators_Ganga import Locators
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter

class cpiproductdetalpageclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Product Detail Page')

    def switch_product_detail_page(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_dropdown_menu))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, Locators.cpi_dropdown_menu)
            dropdown_menu.click()
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="report_ul"]/li[14]/a[contains(text(),"CPI Product Detail V2")]'))
            )

            performance_dashboard = self.driver.find_element(By.XPATH,
                                                             '//*[@id="report_ul"]/li[14]/a[contains(text(),"CPI Product Detail V2")]')
            performance_dashboard.click()
            self.driver.save_screenshot('product_dashboard.png')
        except TimeoutException:
            print('Timeout switch_product_detail_page')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button'))
            )

            platform = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button')
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul'))
            )

            platform_list = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul')
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout get_platform')

    def switch_platform(self, platform_from_list):
        try:
            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button/span'))
            )

            platforms = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button/span')
            if platforms.text != platform_from_list:
                # print('Changing Platform....')
                # platforms.click()
                self.driver.execute_script("arguments[0].click();", platforms)
                to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul/li/a/label[contains(text(), "' + str(platform_from_list) + '")]')
                to_select_platform.click()

                # print('Filter click[Platform change] to: ', platform_from_list)
                time.sleep(5)
                # self.driver.save_screenshot(platform_from_list + '_map_violation_detail.png')
            else:
                # print(platform_from_list)
                pass

        except TimeoutException:
            print('Timeout switch_platform')

    def search_by_sku(self, sku_name):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="select2-product_sku-container"]/span'))
            )

            product = self.driver.find_element(By.XPATH, '//*[@id="select2-product_sku-container"]/span')
            product.click()
            time.sleep(2)
            self.driver.find_element(By.XPATH, '/html/body/span/span/span[1]/input').send_keys(sku_name)
            time.sleep(2)
            found_item = self.driver.find_element(By.XPATH, '//*[@id="select2-product_sku-results"]/li')
            found_item.click()
            time.sleep(1)
            apply_btn = self.driver.find_element(By.XPATH, '//*[@id="apply-filter"]')
            apply_btn.click()
            # self.driver.save_screenshot('product_dashboard.png')

        except TimeoutException:
            print('Timeout switch_product_detail_page')

    def check_detail(self, cpi_product_detail_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div/div[2]/div/div[2]/table/tbody/tr[1]/td[2]'))
            )
            other_sellers = self.driver.find_element(By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/div/div[2]/div/div[2]/table/tbody/tr[1]/td[2]').text


            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="assortment-detail-datatable"]/tbody'))
            )
            table_list = self.driver.find_element(By.XPATH, '//*[@id="assortment-detail-datatable"]/tbody')
            rows = table_list.find_elements(By.TAG_NAME, "tr")

            if int(len(rows)) == int(other_sellers):
                cpi_product_detail_result.append(['Total other_sellers == table data Match item', 'PASS'])
            else:
                cpi_product_detail_result.append(['Total other_sellers == table data Match item', 'FAIL'])
        except TimeoutException:
            print('Timeout switch_product_detail_page')