import time
import re
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException, WebDriverException
from locators.Locators import Locators
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter

class updatepricesclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Update_Prices Page')
        self.dashboard = dashboardpageclass(self.driver)

    def check_loading_message(self):
        try:
            # WebDriverWait(self.driver, 30).until(
            #     EC.visibility_of_element_located((By.XPATH, Locators.update_prices_loading_message))
            # )

            print('check loading message triggered')
            loading_message_logo = self.driver.find_element(By.XPATH, Locators.update_prices_loading_message_icon)
            if 'gbd-logo' in loading_message_logo.get_attribute('src'):
                self.reporter.append_row('Loading message logo', 'OK')
                self.reporter.report()
                print('Loading message logo is OK')
            else:
                self.reporter.append_row('Loading message logo', 'NOT OK')
                self.reporter.report()
                print('Loading message logo is NOTOK')

            loading_message_text = self.driver.find_element(By.XPATH, Locators.update_prices_loading_message_text)
            if loading_message_text.get_attribute('textContent') == 'Please wait while we are loading your report data...':
                self.reporter.append_row('Loading message text', 'OK')
                self.reporter.report()
                print('Loading message text is OK')
            else:
                self.reporter.append_row('Loading message text', 'NOT OK')
                self.reporter.report()
                print('Loading message text is NOTOK')

        except TimeoutException:
            print('Timeout Update_Price_Loading_Message')

    def check_page_for_exclusive(self):
        if 'ExclusiveConcept' in self.driver.page_source:
            print('Exclusive Text is present please check')
            self.reporter.append_row('Exclusive Text', 'NOT OK')
            self.reporter.report()
        else:
            print('Exclusive Text not present')
            self.reporter.append_row('Exclusive Text', 'OK')
            self.reporter.report()

    def check_loader_and_top_logo(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.mini_logo)))

            top_logo = self.driver.find_element(By.XPATH, Locators.mini_logo)
            if 'gbd-logo' in top_logo.get_attribute("src"):
                self.reporter.append_row('Mini logo', 'OK')
                self.reporter.report()
                print('Mini Logo OK')
            else:
                self.reporter.append_row('Mini logo', 'NOT OK')
                self.reporter.report()
                print('Mini Logo not same')

            loader_logo = self.driver.find_element(By.XPATH, Locators.loader_logo)
            if 'gbd-logo' in loader_logo.get_attribute("src"):
                self.reporter.append_row('Loader Logo', 'OK')
                self.reporter.report()
                print('Loader Logo OK')
            else:
                self.reporter.append_row('Loader Logo', 'NOT OK')
                self.reporter.report()
                print('Loader Logo not same')

            loader_icon = self.driver.find_element(By.XPATH, Locators.loader_loading_icon)
            if '10px solid rgb(175, 0, 0)' in loader_icon.value_of_css_property('border-top'):
                self.reporter.append_row('Loader loading Color', 'OK')
                self.reporter.report()
                print('Loader loading color is OK')
            else:
                self.reporter.append_row('Loader loading Color', 'NOT OK')
                self.reporter.report()
                print('Loader loading color is Not OK')

        except TimeoutException:
            print('Timeout report_load')

    def wait_for_page_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.element_to_be_clickable((By.XPATH, Locators.update_price_select_account)))

        except TimeoutException:
            print('Timeout CPI_Page')

    def select_account(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.update_price_select_account)))

            # print(os.environ["Account"])
            # account_name = input("Please Enter Account Name:  ")
            # account = self.driver.find_element(By.XPATH, '//select[@id="accounts"]/option[text()="'+os.environ["Account"]+'"]')
            account = self.driver.find_element(By.XPATH, '//select[@id="accounts"]/option[contains(@value,"'+str(Locators.account_id)+'")]')
            account.click()

        except TimeoutException:
            print('Timeout select account')

    def wait_switch_platform(self):
        try:
            # try:
            #     WebDriverWait(self.driver, 30).until(
            #         EC.visibility_of_element_located((By.XPATH,'//*[@id="cpi-list_processing"]/span[contains(text(),"Please wait while we repopulating Seller list...")]'))
            #     )
            # except WebDriverException:
            #     pass
            #
            # WebDriverWait(self.driver, 30).until(
            #     EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-list_processing"]/span[contains(text(),"Please wait while we repopulating Seller list...")]'))
            # )
            time.sleep(2)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-list_processing"]'))
            )

        except TimeoutException:
            print('Timeout Switch_Platform')

    def check_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.filter_button)
            self.dashboard.check_color(filter_button, 'Filter Button Update Prices')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.export_prices))
            )

            export_prices = self.driver.find_element(By.XPATH, Locators.export_prices)
            self.dashboard.check_color(export_prices, 'UpdatePrices Export Proces Button')

        except TimeoutException:
            print('Timeout Export Prices Buttons')

    def get_first_sku_name(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.first_sku))
            )

            first_sku = self.driver.find_element(By.XPATH, Locators.first_sku)
            return first_sku.text

        except TimeoutException:
            print('Timeout SKU Read')

    def click_first_sku(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.first_sku))
            )

            first_sku = self.driver.find_element(By.XPATH, Locators.first_sku)
            first_sku.click()
            self.driver.switch_to.window(self.driver.window_handles[1])

        except TimeoutException:
            print('Timeout SKU')

    def check_click_first_sku(self, clicked_sku, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/table/tbody/tr/td[2]/span'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.product_detail_page_product_name))
            )

            sku_name_xpath = self.driver.find_element(By.XPATH, Locators.product_detail_page_product_name)
            platform_xpath = self.driver.find_element(By.XPATH, Locators.dashboard_rundate)
            sku_name = sku_name_xpath.text
            if sku_name in clicked_sku and platform_xpath.text in platform :
                print('Pass')
            else:
                print('Fail')

            self.driver.close()

        except TimeoutException:
            print('Timeout First_Sku_Click')

    def wait_sku_detail_load(self):
        try:
            # self.driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL + Keys.TAB)
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="container"]/div/table/tbody/tr[5]/td[2]'))
            )

        except TimeoutException:
            print('Timeout Load_SKU')

    def change_slider_value(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.current_price_slider))
            )

            slider = self.driver.find_element(By.XPATH, Locators.current_price_slider)
            move = ActionChains(self.driver)
            move.click_and_hold(slider).move_by_offset(10, 0).release().perform()  # 10 offset = 40 space
            time.sleep(5)

        except TimeoutException:
            print('Timeout change_price_slider')

    def check_dashboard_button_color(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.go_button_dashboard)
            ))

            go_button = self.driver.find_element(By.XPATH, Locators.go_button_dashboard)
            self.check_color(go_button , 'Go Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.search_report_button)
            ))

            search_button = self.driver.find_element(By.XPATH, Locators.search_report_button)
            self.check_color(search_button, 'Search Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.execute_button)
            ))

            execute_button = self.driver.find_element(By.XPATH, Locators.execute_button)
            self.check_color(execute_button, 'Execute Button')


        except TimeoutException:
            print('Timeout dashboard_buttons')


    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + ' color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + ' color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')

    def check_navigation_color(self):
        try:
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_prices_page_number))
            )
            page_number = self.driver.find_element(By.XPATH, Locators.update_prices_page_number)
            self.check_color(page_number, 'Page Number Link')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.jump_page)
            )                )

            jump_page = self.driver.find_element(By.XPATH, Locators.jump_page)
            self.check_color(jump_page, 'Jump Page Number Link')

        except TimeoutException:
            print('Timeout Update_Price page number')

    def wait_account_page(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.cpi_specialist_load_message))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_report))
            )

        except TimeoutException:
            print('Timeout report_load')

    def cpi_site_wait_account_page(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_all_elements_located((By.XPATH, '//*[@id="hot"]'))
            )

            time.sleep(2)

        except TimeoutException:
            print('Timeout cpi_site_report_load')

    def get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_run_date))
            )

            run_date = self.driver.find_element(By.XPATH, Locators.update_price_run_date)
            return run_date.text

        except TimeoutException:
            print('Timeout run_date')

    def get_run_date_V2(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[2]/div/div/button'))
            )

            run_date = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[2]/div/div/button')
            run_date.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[2]/div/div/ul/li[2]'))
            )

            run_date_data = self.driver.find_element(By.XPATH, '//*[@id="filtercontrols"]/form[1]/div[2]/div/div/ul/li[2]/a/label')
            date_to_return = run_date_data.text
            run_date.click()
            return date_to_return

        except TimeoutException:
            print('Timeout run_date_dashboard')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_platform))
            )

            time.sleep(10)
            platform = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[1]/div/button')
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_platform_ul))
            )

            platform_list = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[1]/div/ul')
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout platform')

    def is_date_present(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="ci_rundate"]'))
            )

            run_date = self.driver.find_element(By.XPATH, '//*[@id="ci_rundate"]')
            if run_date.text != "":
                return "true"
            else:
                return "false"

        except TimeoutException:
            print('Timeout update_price_run_date')

    def select_price_category(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_category_filter))
            )

            price_category = self.driver.find_element(By.XPATH, Locators.update_price_category_filter)
            price_category.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_category_filter_select_all))
            )

            price_category_select_all = self.driver.find_element(By.XPATH, Locators.update_price_category_filter_select_all)
            if price_category_select_all.is_selected() is True:
                price_category_select_all.click()

            price_category_all_options = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[5]/div/ul')
            price_category_all_options_li = price_category_all_options.find_elements(By.TAG_NAME, 'li')
            for i in price_category_all_options_li:
                if 'Cheapest' in i.text or 'Not Cheapest' in i.text or 'High Price Difference' in i.text:
                    index = price_category_all_options_li.index(i)
                    checkbox_select = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[5]/div/ul/li['+str(index+1)+']/a/label/input')
                    if checkbox_select.is_selected() is False:
                        checkbox_select.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.filter_button)
            filter_button.click()

        except TimeoutException:
            print('Timeout PRODUCT_COUNT_CHEAP_NONCHEP_UPDATE_PRICE')

    def get_product_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.update_price_product_count).text
            product_count = re.split(r'(of | entries)\s*', product_count_text)
            return product_count[2]

        except TimeoutException:
            print('Timeout update_price_get_product_count')

    def get_on_hold_count(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filter-elements"]/div[8]'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filter-elements"]/div[8]/div/label/div'))
            )

            slider = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[8]/div/label/div')
            slider.click()

            time.sleep(2)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-list_processing"]'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.update_price_product_count).text
            product_count = re.split(r'(of | entries)\s*', product_count_text)

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="filter-elements"]/div[8]/div/label/div'))
            )

            slider = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[8]/div/label/div')
            slider.click()

            time.sleep(2)
            WebDriverWait(self.driver, 30).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-list_processing"]'))
            )

            return product_count[2]

        except TimeoutException:
            return 0

    def clear_filter(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.update_price_platform))
            )

            platform_filter = self.driver.find_element(By.XPATH, Locators.update_price_platform)
            platform_filter.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_first_platform))
            )

            platform_first_li = self.driver.find_element(By.XPATH, Locators.update_price_first_platform)

            if platform_first_li.is_selected() is False:
                platform_first_li.click()
                WebDriverWait(self.driver, 300).until(
                    EC.invisibility_of_element_located((By.XPATH,
                                                    '//*[@id="cpi-competitors-list_processing"]/span[contains(text(),"Please wait while we are repopulating Seller list...")]'))
                )

                time.sleep(2)

                WebDriverWait(self.driver, 300).until(
                    EC.invisibility_of_element_located((By.XPATH,
                                                    '//*[@id="cpi-competitors-list_processing"]/span[contains(text(),"Please wait while we are loading your report data...")]'))
                )


            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.update_price_product_filter))
            )

            # time.sleep(8)
            # platform_filter.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_product_filter))
            )

            product_name_filter = self.driver.find_element(By.XPATH, Locators.update_price_product_filter)
            product_name_filter.clear()

            time.sleep(2)

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi-list_processing"]'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.update_price_category_filter))
            )

            price_category_filter = self.driver.find_element(By.XPATH, Locators.update_price_category_filter)
            price_category_filter.click()

            price_category_reset = self.driver.find_element(By.XPATH, Locators.update_price_category_filter_select_all)

            if price_category_reset.is_selected() is False:
                price_category_reset.click()

            try:
                WebDriverWait(self.driver, 10).until(
                    EC.element_to_be_clickable((By.XPATH, Locators.update_price_product_category_filter))
                )

                product_category_filter = self.driver.find_element(By.XPATH,Locators.update_price_product_category_filter)
                product_category_filter.click()

                WebDriverWait(self.driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, Locators.update_price_product_category_filter_first_element))
                )

                product_category_first_element = self.driver.find_element(By.XPATH, Locators.update_price_product_category_filter_first_element)
                if product_category_first_element.is_selected() is True:
                    product_category_first_element.click()

            except TimeoutException:
                pass

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_brand_filter))
            )

            brand_filter = self.driver.find_element(By.XPATH, Locators.update_price_brand_filter)
            brand_filter.click()

            try:
                WebDriverWait(self.driver, 30).until(
                    EC.visibility_of_element_located((By.XPATH, Locators.update_price_brand_filter_first_element))
                )
                brand_filter_select_all = self.driver.find_element(By.XPATH, Locators.update_price_brand_filter_first_element)
                if brand_filter_select_all.is_selected() is True:
                    brand_filter_select_all.click()
            except TimeoutException:
                pass

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.filter_button))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.filter_button)
            filter_button.click()

        except TimeoutException:
            print('Timeout Reset_Filter')

    def reset_slider_value(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.current_price_slider))
            )

            slider_first = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[4]/div/div/div[5]')
            slider_last = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[4]/div/div/div[6]')
            # location = slider.location
            # x = location.get('x')
            # y = location.get('y')
            move = ActionChains(self.driver)
            # move.click_and_hold(slider).move_by_offset(int(x/2), int(x/2)).release().perform()  # 10 offset = 40 space
            move.drag_and_drop_by_offset(slider_first, -100, 0).release().perform()
            move.drag_and_drop_by_offset(slider_last, 100, 0).release().perform()

        except TimeoutException:
            print('Timeout Reset_slider_value')


    def get_competitor_report_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.competitor_report_platform))
            )

            time.sleep(10)
            platform = self.driver.find_element(By.XPATH, Locators.competitor_report_platform)
            platform.click()

            items = platform.find_elements_by_tag_name("option")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout competitor_platform')


    def select_platform(self, select_platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_platform))
            )

            platforms = self.driver.find_element(By.XPATH, Locators.update_price_platform)
            platforms.click()

            to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="filter-elements"]/div[1]/div/ul/li/a/label[contains(text(),"'+str(select_platform)+'")]')
            to_select_platform.click()

        except TimeoutException:
            print('Timeout Select_Platform')

    def select_platform_competitor_report(self, select_platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.competitor_report_platform))
            )

            to_select_platform = self.driver.find_element(By.XPATH, '//select[@id="platforms"]/option[text()="'+str(select_platform)+'"]')
            to_select_platform.click()
            # '//*[@id="platforms"]/option[1]'

        except TimeoutException:
            print('Timeout Select_Competitor__Platform')

    def wait_comp_report_platform_load(self):
        try:
            WebDriverWait(self.driver, 50).until(
                EC.visibility_of_element_located((By.XPATH, Locators.first_item_competitor_report))
            )

        except TimeoutException:
            print('Timeout comp_report_product_load')

    def click_first_sku(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.update_price_first_sku))
            )

            first_sku = self.driver.find_element(By.XPATH, Locators.update_price_first_sku)
            first_sku.click()
            self.driver.switch_to.window(self.driver.window_handles[1])

        except TimeoutException:
            print('Timeout report_SKU')

    def wait_cpi_report_load(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/table/tbody/tr/td[2]/span'))
            )

            print('Found')

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.report_loader_icon))
            )

        except TimeoutException:
            print('Timeout cpi_product_load')







