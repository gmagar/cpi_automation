import time
import re
from selenium.webdriver.support.ui import WebDriverWait
from locators.Locators import Locators
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
import random


class nearestcompetitorclass():
    def __init__(self, driver):
        self.driver = driver

    def click_item_name(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_first_item_name))
            )

            first_item = self.driver.find_element(By.XPATH, Locators.nearest_5_first_item_name)
            first_item_name = first_item.text
            first_item.click()

            self.driver.switch_to.window(self.driver.window_handles[2])
            return first_item_name.split(' ')[0]

        except TimeoutException:
            print("Timeout CLICK_ITEM_NAME_NEAREST")
            return "timeout"

    def check_item_page(self, item_name, nearest_five_checker):
        try:
            time.sleep(10)
            if item_name != "timeout":
                if item_name in self.driver.page_source:
                    nearest_five_checker.append(['Nearest_5_Page Item name redirection to product page', 'PASS'])
                else:
                    nearest_five_checker.append(['Nearest_5_Page Item name redirection to product page', 'FAIL'])
                self.driver.close()
                self.driver.switch_to.window(self.driver.window_handles[1])
            else:
                pass

        except TimeoutException:
            print('Timeout CHECK_ITEM_NAME_NEAREST')

    def click_first_sku(self):
        try:
            time.sleep(10)
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div/i'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_first_sku_name))
            )
            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '/html/body/div[2]/div/div/div/i'))
            )

            first_sku = self.driver.find_element(By.XPATH, Locators.nearest_5_first_sku_name)
            first_sku.click()
            self.driver.switch_to.window(self.driver.window_handles[2])

        except TimeoutException:
            print('Timeout Nearest_5_SKU_click')

    def get_first_sku_name(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_first_sku_name))
            )

            first_sku = self.driver.find_element(By.XPATH, Locators.nearest_5_first_sku_name)
            return first_sku.text

        except TimeoutException:
            print('Timeout Nearest_5_SKU Read')


    def check_click_first_sku(self, clicked_sku, platform, update_price_check):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/table/tbody/tr/td[2]/span'))
            )

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.product_detail_page_product_name))
            )

            sku_name_xpath = self.driver.find_element(By.XPATH, Locators.product_detail_page_product_name)
            platform_xpath = self.driver.find_element(By.XPATH, Locators.dashboard_rundate)
            sku_name = sku_name_xpath.text
            if clicked_sku in sku_name and platform in platform_xpath.text:
                update_price_check.append(['Redirection to Product Detail Page on click SKU', 'PASS'])
            else:
                update_price_check.append(['Redirection to Product Detail Page on click SKU', 'FAIL'])
            self.driver.close()

            self.driver.switch_to.window(self.driver.window_handles[1])


        except TimeoutException:
            print('Timeout Nearest_5_First_Sku_Click')

    def get_number_of_competitor(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_table))
            )

            nearest_5_table = self.driver.find_element(By.XPATH, Locators.nearest_5_table)
            rows = nearest_5_table.find_elements(By.TAG_NAME, 'th')
            table_header = []
            for i in rows:
                table_header.append(i.text)
            count = 0
            competitor_index = []
            for item in table_header:
                match = re.search(r'\d\Z', item)
                if match:
                    competitor_index.append(count)
                count = count + 1
            return competitor_index


        except TimeoutException:
            print('Timeout Nearest_5_number_of_competitor')

    def check_product_page_redirection(self, competitor_index, nearest_5_checker, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_table))
            )
            count = 3
            for i in competitor_index:
                try:
                    WebDriverWait(self.driver, 30).until(
                        EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td['+str(i+1)+']/a'))
                    )
                    competitor_click_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td['+str(i+1)+']/a')
                    self.driver.execute_script("arguments[0].scrollIntoView();", competitor_click_xpath)
                    # print(competitor_click_xpath.text, i)
                    competitor_name = competitor_click_xpath.text
                    competitor_click_xpath.click()
                    self.driver.switch_to.window(self.driver.window_handles[2])
                    item_url = self.driver.current_url
                    if 'Google'.lower() in platform.lower().split(' ')[0] or 'Amazon'.lower() in platform.lower().split(' ')[0]:
                        if platform.lower().split(' ')[0] in item_url:
                            nearest_5_checker.append(['Redirection to Platform Page on Competitor_' + str(i - count) + ' item link', 'PASS'])
                        else:
                            nearest_5_checker.append(['Redirection to Platform Page on Competitor_' + str(i - count) + ' item link', 'FAIL'])
                    else:
                        if competitor_name.lower() in item_url:
                            nearest_5_checker.append(['Redirection to Platform Page on Competitor_'+str(i-count)+' item link', 'PASS'])
                        else:
                            nearest_5_checker.append(['Redirection to Platform Page on Competitor_'+str(i-count)+' item link', 'FAIL'])
                    self.driver.close()
                    self.driver.switch_to.window(self.driver.window_handles[1])
                    count = count + 1

                except TimeoutException:
                    # print(i-count)
                    pass

        except TimeoutException:
            print('Timeout Nearest_5_product_redirection')

    def get_index_of_shipping(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_data_table))
            )

            nearest_5_table = self.driver.find_element(By.XPATH, Locators.nearest_5_data_table)
            rows = nearest_5_table.find_elements(By.TAG_NAME, 'tr')
            shipping_col_index = []
            shipping_row_index = []
            break_index = 0
            row = 0
            for i in rows:
                row = row + 1
                if 'shipping' in i.text:
                    col = i.find_elements(By.TAG_NAME, 'td')
                    if len(col) != 0:
                        for a in range(len(col)):
                            if 'shipping' in col[a].text:
                                shipping_row_index.append([row, a+1])
                                break_index = break_index + 1
                                if break_index == 2:
                                    return shipping_row_index
            # count = 0
            # competitor_index = []
            # for item in rows:
            #     col = item.find_elements(By.TAG_NAME, 'td')
            #     if len(col) != 0:
            #         print(col[0].text)


        except TimeoutException:
            print('Timeout Nearest_5_number_of_competitor')


    def get_shipping_data(self, row_index, nearest_five_checker, no_shipping_client, account_name):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_data_table))
            )

            to_check_shipping = []

            for i in row_index:
                shipping_value = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr['+str(i[0])+']/td['+str(i[1])+']')
                shipping_value_text = shipping_value.text
                try:
                    if account_name in no_shipping_client:
                        combined_shipping_value = float(shipping_value_text.split(' (+$')[0].split('$')[1])
                    else:
                        combined_shipping_value = float(shipping_value_text.split(' (+$')[0].split('$')[1]) + float(shipping_value_text.split(' (+$')[1].split('shipping')[0])
                    to_check_shipping.append(float(round(combined_shipping_value, 2)))
                except ValueError:
                    nearest_five_checker.append(['Price Displayed in "$119.99 (+$4.99 shpping)" Pattern', 'FAIL'])
            nearest_five_checker.append(['Price Displayed in "$119.99 (+$4.99 shpping)" Pattern', 'PASS'])

            return to_check_shipping

        except TimeoutException:
            print('Timeout Nearest_5_get_shipping_data')


    def switch_price_display_mode(self):
        try:
            self.driver.execute_script("window.scrollTo(0, 0);")
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.price_display_mode))
            )

            price_display_mode = self.driver.find_element(By.XPATH, Locators.price_display_mode)
            price_display_mode.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.total_price))
            )
            total_price = self.driver.find_element(By.XPATH, Locators.total_price)
            total_price.click()

            apply_filter = self.driver.find_element(By.XPATH, Locators.apply_filter_button)
            apply_filter.click()

        except TimeoutException:
            print('Timeout Switch_Price_Display_Mode')

    def wait_switch_price_display_mode(self):
        try:
            time.sleep(2)
            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, Locators.loader_loading_icon))
            )

            time.sleep(2)

            WebDriverWait(self.driver, 300).until(
                EC.invisibility_of_element_located((By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable_processing"]'))
            )

        except TimeoutException:
            print('Timeout Switch_Price_Display_Mode')

    def get_total_price_data(self, row_index):
        try:
            # WebDriverWait(self.driver, 30).until(
            #     EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_data_table))
            # )

            to_check_shipping = []

            for i in row_index:
                shipping_value = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr['+str(i[0])+']/td['+str(i[1])+']')
                shipping_value_text = shipping_value.text
                combined_shipping_value = float(shipping_value_text.split('$')[1])
                to_check_shipping.append(combined_shipping_value)

            return to_check_shipping

        except TimeoutException:
            print('Timeout Get_Total_Price_Data')

    def check_with_and_without_shipping(self, nearest_5_with_shipping_value, nearest_5_with_total_price, nearest_five_checker):
        if nearest_5_with_shipping_value[0] == nearest_5_with_total_price[0] and nearest_5_with_shipping_value[1] == nearest_5_with_total_price[1]:
            nearest_five_checker.append(['Price Displayed on Base Price Filter and Total Price Filter', 'PASS'])
        else:
            nearest_five_checker.append(['Price Displayed on Base Price Filter and Total Price Filter', 'FAIL'])

    def get_all_competitor_price(self, competitor_name_index, nearest_5_checker):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.nearest_5_table))
            )
            row_count = len(self.driver.find_elements(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr'))
            # print(row_count)
            if row_count < 3:
                col = sorted(random.sample(range(row_count), 1))
            else:
                col = sorted(random.sample(range(row_count), 3))
            for i in col:
                previous_competitor_price = 0
                for j in competitor_name_index:
                    try:
                        competitor_price_xpath = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr['+str(i+1)+']/td[' + str(j + 2) + ']')
                        # print(previous_competitor_price, competitor_price_xpath.text)
                        self.driver.execute_script("arguments[0].scrollIntoView();", competitor_price_xpath)

                        WebDriverWait(self.driver, 30).until(
                            EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr['+str(i+1)+']/td[' + str(j + 2) + ']'))
                        )

                        # print(competitor_price_xpath.text)
                        if competitor_price_xpath.text:
                            if 'shipping' in competitor_price_xpath.text:
                                combined_competitor_price = float(competitor_price_xpath.text.split(' (+$')[0].split('$')[1]) + float(competitor_price_xpath.text.split(' (+$')[1].split('shipping')[0])
                            elif "(Shipping N/A)" in competitor_price_xpath.text:
                                combined_competitor_price = float(competitor_price_xpath.text.split(' (Shipping')[0].split('$')[1])
                            elif "(Free)" in competitor_price_xpath.text:
                                combined_competitor_price = float(competitor_price_xpath.text.split(' (Free)')[0].split('$')[1])
                            else:
                                combined_competitor_price = float(competitor_price_xpath.text.split('$')[1])
                        else:
                            break
                        if previous_competitor_price <= combined_competitor_price:
                            previous_competitor_price = combined_competitor_price
                        else:
                            sku_name = self.driver.find_element(By.XPATH,'//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[' + str(i + 1) + ']/td[2]/a')
                            nearest_5_checker.append(['Ascending Price of ' + str(sku_name.text) + '', 'FAIL'])
                            break

                    except TimeoutException:
                        print('Timeout Competitor_' + str(i + 1) + '_Click',i,j)
            nearest_5_checker.append(['Ascending Price of Competitors', 'PASS'])

        except TimeoutException:
            print('Timeout Compare_Competitor_Price')








