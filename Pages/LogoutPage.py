from selenium.webdriver.support.ui import WebDriverWait
from locators.Locators import Locators
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
from Pages.DashboardPage import dashboardpageclass
from Tests.utilities.reporter import Reporter


class logoutclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Logout Page')

    def click_user_icon(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.user_image))
            )

            user_image_link = self.driver.find_element(By.XPATH, Locators.user_image)
            user_image_link.click()

        except TimeoutException as e:
            return 'Timeout user_image'

    def check_button_color(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.profile_button))
            )

            profile_button = self.driver.find_element(By.XPATH, Locators.profile_button)
            self.check_color(profile_button, 'Profile Button')

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.logout_button))
            )

            logout_button = self.driver.find_element(By.XPATH, Locators.logout_button)
            self.check_color(logout_button, 'Logout Button')

        except TimeoutException as e:
            print('Timeout profile and logout button')

    def click_logout(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.user_image))
            )

            # user_image_link = self.driver.find_element(By.XPATH, Locators.user_image)
            # user_image_link.click()

            logout_link = self.driver.find_element(By.XPATH, Locators.logout_button)
            logout_link.click()

        except TimeoutException as e:
            print('Timeout Logout_Button')\

    def check_color(self, element, element_name):
        # print(element.value_of_css_property('background-color'))
        if 'rgba(175, 0, 0, 1)' in element.value_of_css_property('background-color'):
            self.reporter.append_row(element_name + 'color', 'OK')
            self.reporter.report()
            print(element_name + 'color is OK')
        else:
            self.reporter.append_row(element_name + 'color', 'NOT OK')
            self.reporter.report()
            print(element_name + 'color is not OK')