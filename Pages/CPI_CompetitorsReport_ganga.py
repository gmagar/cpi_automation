import re
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from Tests.utilities.reporter import Reporter
from Pages.CPI_Dashboard import cpidashboardpageclass
from locators.Locators_Ganga import Locators


class cpicompetitorsreport():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Competitors_Report Page')
        self.dashboard = cpidashboardpageclass(self.driver)

    def switch_to_competitor_report_menu(self):
        try:

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_reports"]/a'))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, '//*[@id="account_reports"]/a')
            dropdown_menu.click()
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_ul"]/li[2]/a[contains(text(),"Competitors Report")]'))
            )

            competitors_report = self.driver.find_element(By.XPATH,
                                                          '//*[@id="account_ul"]/li[2]/a[contains(text(),"Competitors Report")]')
            competitors_report.click()

            iframe = self.driver.find_element(By.XPATH, '/html/body/div/div/section/div/iframe')
            self.driver.switch_to.frame(iframe)

            clear_product_sku = self.driver.find_element(By.XPATH, '//*[@id="ProductNameOrSku"]')
            time.sleep(2)
            clear_product_sku.clear()


        except TimeoutException:
            print('Timeout switch_to_competitor_report_menu')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_site_comp_report_platform))
            )

            time.sleep(10)
            platform = self.driver.find_element(By.XPATH, Locators.cpi_site_comp_report_platform)
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_comp_report_platform_ul))
            )

            platform_list = self.driver.find_element(By.XPATH, Locators.cpi_comp_report_platform_ul)
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout cpi_site_comp_report_platform')

    def get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_competitor_run_date))
            )

            run_date = self.driver.find_element(By.XPATH, Locators.cpi_competitor_run_date)
            return run_date.text

        except TimeoutException:
            print('Timeout comp_report_run_date')

    def select_platform(self, select_platform):
        # print("Platform: ", select_platform)
        try:

            time.sleep(10)

            # item table detail
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]'))
            )

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH,
                                                  '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/button/span'))
            )

            platforms = self.driver.find_element(By.XPATH,
                                                 '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/button/span')
            # print('Current platform: ', platforms.text)
            time.sleep(5)
            self.driver.save_screenshot(select_platform + '_competitors_report_detail.png')

            if platforms.text != select_platform:
                # print('Changing Platform....')
                # platforms.click()
                self.driver.execute_script("arguments[0].click();", platforms)
                # print('Changed to ', select_platform)

                to_select_platform = self.driver.find_element(By.XPATH,
                                                              '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul/li/a/label[contains(@title, "' + str(
                                                                  select_platform) + '")]')
                to_select_platform.click()

                cpi_filter = self.driver.find_element(By.XPATH, Locators.filter_button)
                cpi_filter.click()
                # print('Filter click[Platform change] to: ', to_select_platform)
                time.sleep(5)
                self.driver.save_screenshot(select_platform + '_competitors_report_detail.png')
            else:
                pass

        except TimeoutException:
            print('Timeout CPI_Select_Platform')

    def get_sellers(self, competitor_report_result):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.sellers))
            )
            sellers = self.driver.find_element(By.XPATH, Locators.sellers).text
            if sellers != None:
                competitor_report_result.append(['Sellers List', sellers])

        except TimeoutException:
            print('Timeout get_sellers')

    def get_total_product(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.competitor_report_product_count))
            )

            product_count_text = self.driver.find_element(By.XPATH, Locators.competitor_report_product_count).text
            product_count = re.split(r'(of | entries)\s*', product_count_text)
            return product_count[2]

        except TimeoutException:
            print('Timeout competitor_report_get_product_count')

    def select_price_category(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.element_to_be_clickable((By.XPATH, Locators.cpi_comp_report_price_category_filter))
            )

            price_category = self.driver.find_element(By.XPATH, Locators.cpi_comp_report_price_category_filter)
            price_category.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, Locators.cpi_comp_report_price_category_filter_select_all))
            )

            price_category_select_all = self.driver.find_element(By.XPATH,
                                                                 Locators.cpi_comp_report_price_category_filter_select_all)
            if price_category_select_all.is_selected() is False:
                price_category_select_all.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_competitors_page_filter))
            )

            filter_button = self.driver.find_element(By.XPATH, Locators.cpi_competitors_page_filter)
            self.driver.execute_script("arguments[0].click();", filter_button)
            # filter_button.click()

        except TimeoutException:
            print('Timeout select_price_category')

    def get_clickable_sku_index(self):
        i = 1
        while i < 30:
            try:
                WebDriverWait(self.driver, 30).until(
                    EC.visibility_of_element_located(
                        (By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(
                            i) + ']/td[2]/a'))
                )
                return i
            except TimeoutException:
                i = i + 1
                pass
        return i

    def get_first_sku_name(self, clickable_sku_index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[2]/a'))
            )

            first_sku = self.driver.find_element(By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(clickable_sku_index)+']/td[2]/a')
            return first_sku.text

        except TimeoutException:
            print('Timeout SKU Read')

    def click_first_sku(self, index, competitor_report_result, platform, sku_name):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(index)+']/td[2]/a'))
            )

            first_sku = self.driver.find_element(By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(index)+']/td[2]/a')
            first_sku.click()

            self.driver.switch_to.window(self.driver.window_handles[1])
            time.sleep(1)
            if sku_name in self.driver.current_url:
                self.driver.save_screenshot(platform + "_competitor_sku_detail.png")
                competitor_report_result.append(['Redirection to Detail Page on click SKU', 'PASS'])
            else:
                competitor_report_result.append(['Redirection to Detail Page on click SKU', 'Fail'])

            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])

        except TimeoutException:
            print('Timeout CPI_SKU')

    def click_first_item(self, index, competitor_report_result, platform):
        try:

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH,
                                                  '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(
                                                      index) + ']/td[1]/a'))
            )
            first_item = self.driver.find_element(By.XPATH,
                                                  '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(
                                                      index) + ']/td[1]/a')
            first_item.click()

            self.driver.switch_to.window(self.driver.window_handles[1])
            time.sleep(2)
            if platform.lower() in self.driver.current_url:
                self.driver.save_screenshot("competitor_report_item_detail.png")
                competitor_report_result.append(['Redirection to Item Page on click Item', 'PASS'])
            else:
                competitor_report_result.append(['Redirection to Item Page on click Item', 'Fail'])
            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])

        except TimeoutException:
            print('Timeout cpi_site_click_first_item')

    def get_item_cost(self, index, competitor_report_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(index)+']/td[3]'))
            )

            item_cost = self.driver.find_element(By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(index)+']/td[3]')

            if item_cost.text != None:
                competitor_report_result.append(['Item Cost Present', 'PASS'])
            else:
                competitor_report_result.append(['Item Cost Present', 'FAIL'])

        except TimeoutException:
            print('Timeout check_first_item_cost')

    def get_floor_price(self, index, competitor_report_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH,  '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(index)+']/td[4]'))
            )

            floor_price = self.driver.find_element(By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr['+str(index)+']/td[4]')
            if floor_price.text != None:
                competitor_report_result.append(['Floor Price Present', 'PASS'])
            else:
                competitor_report_result.append(['Floor Price Present', 'FAIL'])

        except TimeoutException:
            print('Timeout check_first_item_floor_price')

    def map_violation_download_option(self, index):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH,
                                                  '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(
                                                      index) + ']/td[5]'))
            )
        except TimeoutException:
            print('Timeout map_violation_download_option')

    def get_price(self, index, competitor_report_result):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(index) +']/td[8]/a'))
            )
            item_price = self.driver.find_element(By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(index) +']/td[8]/a')
            if item_price != None:
                competitor_report_result.append(['Price of Item Present', 'PASS'])
            else:
                competitor_report_result.append(['Price of Item Present', 'FAIL'])
        except TimeoutException:
            print('Timeout get_price')

    def suggested_price(self, index, competitor_report_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH,
                                                  '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(
                                                      index) + ']/td[9]'))
            )
            suggested_price = self.driver.find_element(By.XPATH, '//*[@id="competitorsReportDataTable"]/div[1]/div/div/div/table/tbody/tr[' + str(
                                                      index) + ']/td[9]')
            if suggested_price != None:
                competitor_report_result.append(['Suggested Price Present', 'PASS'])
                return suggested_price

            else:
                competitor_report_result.append(['Suggested Price Present', 'FAIL'])
                return 0
        except TimeoutException:
            print('Timeout suggested_price')

