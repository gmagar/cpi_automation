import re
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from Tests.utilities.reporter import Reporter
from Pages.CPI_Dashboard import cpidashboardpageclass
from locators.Locators_Ganga import Locators


class cpiassortmentclass():

    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Assortment Page')
        self.dashboard = cpidashboardpageclass(self.driver)

    def switch_to_assortment_menu(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_dropdown_menu))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, Locators.cpi_dropdown_menu)
            dropdown_menu.click()
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_assortment_menu))
            )

            assortment_menu = self.driver.find_element(By.XPATH, Locators.cpi_assortment_menu)
            assortment_menu.click()

        except TimeoutException:
            print('Timeout switch_to_assortment_menu')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_assortment_platform))
            )

            platform = self.driver.find_element(By.XPATH, Locators.cpi_assortment_platform)
            platform.click()
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_assortment_platform_ul))
            )

            platform_list = self.driver.find_element(By.XPATH, Locators.cpi_assortment_platform_ul)
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout cpi_assortment_platform')

    def switch_platform(self, platform_from_list):
        try:

            if platform_from_list == 'Google':
                platform_from_list = platform_from_list.replace('Google', 'Google Shopping')


            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_assortment_platform_btn))
            )

            platforms = self.driver.find_element(By.XPATH, Locators.cpi_assortment_platform_btn)
            time.sleep(5)
            self.driver.save_screenshot(platform_from_list + '_assortment_detail.png')

            if platforms.text != platform_from_list:
                # print('Changing Platform....')
                # platforms.click()
                self.driver.execute_script("arguments[0].click();", platforms)

                to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul/li/a/label[contains(text(), "' + str(platform_from_list) + '")]')
                to_select_platform.click()

                apply_btn = self.driver.find_element(By.XPATH, Locators.cpi_assortment_apply_btn)
                apply_btn.click()
                # print('Filter click[Platform change] to: ', platform_from_list)
                time.sleep(5)
                self.driver.save_screenshot(platform_from_list + '_assortment_detail.png')
            else:
                pass

        except TimeoutException:
            print('Timeout CPI_Select_Platform')

    def get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_assortment_run_date))
            )

            run_date = self.driver.find_element(By.XPATH, Locators.cpi_assortment_run_date)
            return run_date.text

        except TimeoutException:
            print('Timeout comp_report_run_date')