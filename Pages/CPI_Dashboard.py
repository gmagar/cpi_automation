import time
import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from locators.Locators_Ganga import Locators
from Tests.utilities.reporter import Reporter


class cpidashboardpageclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('CPI Dashboard Page')

    def wait_dashboard_load(self):
        try:
            WebDriverWait(self.driver, 600).until(
                EC.invisibility_of_element_located((By.XPATH, '//div[@class="loading-logo"]'))
            )
        except TimeoutException:
            print('Timeout Dashboard_Load')
