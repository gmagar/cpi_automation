import time
import re
from selenium.webdriver.support.ui import WebDriverWait
from Tests.utilities.reporter import Reporter
from locators.Locators_Ganga import Locators
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, StaleElementReferenceException
import random

class cpinearest5competitorsclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('NEAREST 5 COMPETITORS V2 PAGE')

    def switch_nearest5_competitors_page(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_dropdown_menu))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, Locators.cpi_dropdown_menu)
            dropdown_menu.click()
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="report_ul"]/li[11]/a[contains(text(),"CPI Nearest 5 Competitors V2")]'))
            )
            nearest5_menu = self.driver.find_element(By.XPATH, '//*[@id="report_ul"]/li[11]/a[contains(text(),"CPI Nearest 5 Competitors V2")]')
            nearest5_menu.click()

        except TimeoutException:
            print('Timeout switch_nearest5_competitors_page')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button'))
            )

            platform = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button')
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul'))
            )

            platform_list = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul')
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout get_platform')

    def switch_platform(self, platform_from_list):
        try:
            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button/span'))
            )

            platforms = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/button/span')
            if platforms.text != platform_from_list:
                # print('Changing Platform....')
                # platforms.click()
                self.driver.execute_script("arguments[0].click();", platforms)
                to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul/li/a/label[contains(text(), "' + str(platform_from_list) + '")]')
                to_select_platform.click()

                apply_btn = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_apply_btn)
                apply_btn.click()
                # print('Filter click[Platform change] to: ', platform_from_list)
                time.sleep(5)
                # self.driver.save_screenshot(platform_from_list + '_map_violation_detail.png')
            else:
                # print(platform_from_list)
                pass

        except TimeoutException:
            print('Timeout switch_platform')

    def get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="v2filter-form"]/div[2]/div/div/button/span'))
            )

            run_date = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[2]/div/div/button/span')
            return run_date.text

        except TimeoutException:
            print('Timeout CPI_MAP_VIOLATION_RUN_DATE')

    def first_item_click(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[1]/a'))
            )

            first_item = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[1]/a')
            first_item.click()
            # print(self.driver.current_url)
            self.driver.switch_to.window(self.driver.window_handles[1])
            # print(self.driver.current_url)
            self.driver.close()
            self.driver.switch_to.window(self.driver.window_handles[0])

        except TimeoutException:
            print('Timeout first_item_click')

    def first_sku_name(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[2]/a'))
            )

            first_sku = self.driver.find_element(By.XPATH,
                                                  '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[2]/a').text
            return first_sku

        except TimeoutException:
            print('Timeout first_item_click')

    def first_sku_click(self, sku_name, cpi_nearest5_competitor_result, platform):
        # print(self.driver.current_url)
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[2]/a'))
            )

            first_sku= self.driver.find_element(By.XPATH,
                                                  '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[2]/a')
            first_sku.click()

            self.driver.switch_to.window(self.driver.window_handles[1])
            # print(self.driver.current_url)

            if sku_name in self.driver.current_url:
                cpi_nearest5_competitor_result.append(['First SKU Click', 'PASS'])
                self.driver.close()
                self.driver.switch_to.window(self.driver.window_handles[0])
            else:
                cpi_nearest5_competitor_result.append(['First SKU Click', 'FAIL'])
                self.driver.close()
                self.driver.switch_to.window(self.driver.window_handles[0])

        except TimeoutException:
            print('Timeout first_item_click')

    def first_item_cost(self, platform, cpi_nearest5_competitor_result):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[3]'))
            )

            first_item_cost = self.driver.find_element(By.XPATH,
                                                 '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[3]')
            if first_item_cost != None:
                cpi_nearest5_competitor_result.append(['First Item Cost', 'PASS'])
            else:
                cpi_nearest5_competitor_result.append(['First Item Cost', 'FAIL'])

        except TimeoutException:
            print('Timeout first_item_cost')

    def first_item_price(self, platform, cpi_nearest5_competitor_result):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[4]/span'))
            )

            first_item_price = self.driver.find_element(By.XPATH,
                                                 '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[4]/span').text
            if first_item_price != None:
                cpi_nearest5_competitor_result.append(['First Item Price', 'PASS'])
            else:
                cpi_nearest5_competitor_result.append(['First Item Price', 'FAIL'])

        except TimeoutException:
            print('Timeout first_item_price')

    def get_first_competitor_name_price(self, cpi_nearest5_competitor_result, platform):
        try:

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[5]'))
            )
            competitor_name = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[5]/a').text
            competitor_price = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[6]')
            if competitor_name != None and competitor_price != None:
                cpi_nearest5_competitor_result.append(['Competitor1 and Price', 'PASS'])
            else:
                cpi_nearest5_competitor_result.append(['Competitor1 and Price', 'NOT PRESENT'])

        except TimeoutException:
            cpi_nearest5_competitor_result.append(['Competitor1 and Price', 'Not Found'])
            # print('Timeout get_first_competitor_name')

    def get_second_competitor_name_price(self, cpi_nearest5_competitor_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[7]/a'))
            )

            competitor_name = self.driver.find_element(By.XPATH,
                                                       '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[7]/a').text
            competitor_price = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[8]')

            if competitor_name != None and competitor_price != None:
                cpi_nearest5_competitor_result.append(['Competitor2 and Price', 'PASS'])
            else:
                cpi_nearest5_competitor_result.append(['Competitor2 and Price', 'Not Present'])

        except TimeoutException:
            cpi_nearest5_competitor_result.append(['Competitor2 and Price', 'Not Found'])
            # print('Timeout get_second_competitor_name')

    def get_third_competitor_name_price(self, cpi_nearest5_competitor_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[9]/a'))
            )

            competitor_name = self.driver.find_element(By.XPATH,
                                                       '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[9]/a').text
            competitor_price = self.driver.find_element(By.XPATH,
                                                       '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[10]/a')

            if competitor_name != None and competitor_price != None:
                cpi_nearest5_competitor_result.append(['Competitor3 and Price', 'PASS'])

            else:
                cpi_nearest5_competitor_result.append(['Competitor3 and Price', 'Not Present'])

        except TimeoutException:
            cpi_nearest5_competitor_result.append(['Competitor3 and Price', 'Not Found'])
            # print('Timeout get_third_competitor_name')

    def get_fourth_competitor_name_price(self, cpi_nearest5_competitor_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[11]/a'))
            )

            competitor_name = self.driver.find_element(By.XPATH,
                                                       '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[11]/a').text
            competitor_price = self.driver.find_element(By.XPATH,
                                                        '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[12]/a')

            if competitor_name != None and competitor_price != None:
                cpi_nearest5_competitor_result.append(['Competitor3 and Price', 'PASS'])
            else:
                cpi_nearest5_competitor_result.append(['Competitor3 and Price', 'FAIL'])

        except TimeoutException:
            # print('Timeout get_fourth_competitor_name')
            cpi_nearest5_competitor_result.append(['Competitor4 and Price', 'Not Found'])


    def get_fifth_competitor_name_price(self, cpi_nearest5_competitor_result, platform):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[13]/a'))
            )

            competitor_name = self.driver.find_element(By.XPATH,
                                                       '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[13]/a').text
            competitor_price = self.driver.find_element(By.XPATH,
                                                        '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[12]/a')

            if competitor_name != None and competitor_price != None:
                cpi_nearest5_competitor_result.append(['Competitor5 and Price', 'PASS'])
            else:
                cpi_nearest5_competitor_result.append(['Competitor5 and Price', 'FAIL'])

        except TimeoutException:
            cpi_nearest5_competitor_result.append(['Competitor5 and Price', 'Not Found'])

            # print('Timeout get_fifth_competitor_name')
