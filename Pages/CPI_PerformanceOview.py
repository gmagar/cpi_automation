import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains

from locators.Locators_Ganga import Locators
from selenium.common.exceptions import TimeoutException
from Tests.utilities.reporter import Reporter
from selenium.webdriver.support.select import Select


class cpiperformanceoviewclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('Performance Overview Page')

    def switch_performance_overview_page(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_dropdown_menu))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, Locators.cpi_dropdown_menu)
            dropdown_menu.click()
            performance_dashboard = self.driver.find_element(By.XPATH,
                                                     '//*[@id="report_ul"]/li[12]/a[contains(text(),"CPI Overview Dashboard")]')
            performance_dashboard.click()
            self.driver.save_screenshot('performance_dashboard.png')
        except TimeoutException:
            print('Timeout switch_performance_overview_page')
