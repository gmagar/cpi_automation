import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains

from locators.Locators_Ganga import Locators
from selenium.common.exceptions import TimeoutException
from Tests.utilities.reporter import Reporter

class cpimapviolationclass():
    def __init__(self, driver):
        self.driver = driver
        self.reporter = Reporter('MAP VIOLATION Page')

    def switch_map_violation_page(self):
        try:

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_dropdown_menu))
            )
            dropdown_menu = self.driver.find_element(By.XPATH, Locators.cpi_dropdown_menu)
            dropdown_menu.click()
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, '//*[@id="account_ul"]/li[5]/a[contains(text(),"Map Violations")]'))
            )
            map_violation = self.driver.find_element(By.XPATH, '//*[@id="account_ul"]/li[5]/a[contains(text(),"Map Violations")]')
            map_violation.click()

        except TimeoutException:
            print('Timeout switch_insight_page')

    def get_platform(self):
        try:
            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_map_violation_platform))
            )

            platform = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_platform)
            platform.click()

            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_map_violation_platform_ul))
            )

            platform_list = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_platform_ul)
            items = platform_list.find_elements_by_tag_name("li")
            text = []
            for item in items:
                text.append(item.text)
            platform.click()
            return text

        except TimeoutException:
            print('Timeout cpi_map_violation_platform')

    def switch_platform(self, platform_from_list):
        try:
            time.sleep(10)

            WebDriverWait(self.driver, 300).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_map_violation_default_platform))
            )

            platforms = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_default_platform)
            time.sleep(5)
            # self.driver.save_screenshot(platform_from_list + '_map_violation_detail.png')

            if platforms.text != platform_from_list:
                # print('Changing Platform....')
                # platforms.click()
                self.driver.execute_script("arguments[0].click();", platforms)
                to_select_platform = self.driver.find_element(By.XPATH, '//*[@id="v2filter-form"]/div[1]/div/div/ul/li/a/label[contains(text(), "' + str(platform_from_list) + '")]')
                to_select_platform.click()

                apply_btn = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_apply_btn)
                apply_btn.click()
                # print('Filter click[Platform change] to: ', platform_from_list)
                time.sleep(5)
                # self.driver.save_screenshot(platform_from_list + '_map_violation_detail.png')
            else:
                pass

        except TimeoutException:
            print('Timeout CPI_MAP_VIOLATION_SWITCH_Platform')

    def get_run_date(self):
        try:
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_map_violation_run_date))
            )

            run_date = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_run_date)
            return run_date.text

        except TimeoutException:
            print('Timeout CPI_MAP_VIOLATION_RUN_DATE')

    def seller_list(self, platform, cpi_map_violation_result):

        try:
            # table list
            time.sleep(5)
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.XPATH, Locators.cpi_map_violation_table_data))
            )
            table_list = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_table_data)
            rows = table_list.find_elements(By.TAG_NAME, "tr")
            for row in range(len(rows)):

                seller_name_by_row = self.driver.find_element(By.XPATH, '//*[@id="seller-map-violation-datatable"]/tbody/tr['+str(row+1)+']/td[1]').text
                # print(seller_name_by_row)
                # print(seller_name_by_row, '//*[@id="seller-map-violation-datatable"]/tbody/tr['+str(row+3)+']/td[1]')
                column_data = self.driver.find_element(By.XPATH, '//*[@id="seller-map-violation-datatable"]/tbody/tr['+str(row+1)+']/td[2]/a')

                column_data.click()
                self.driver.switch_to.window(self.driver.window_handles[1])

                current_platform = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_default_platform).text
                current_competitor = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_competitor_name).text

                if platform.lower() == current_platform.lower() and seller_name_by_row.lower() == current_competitor.lower():

                    first_sku = self.driver.find_element(By.XPATH, '//*[@id="map-violations-detail-datatable"]/tbody/tr[1]/td[2]')
                    map_price = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_map_price)
                    price_difference = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_price_difference)
                    if first_sku != None or first_sku != None or map_price != price_difference:
                        self.driver.save_screenshot(current_competitor + 'has_sku_map_pdifference.png')
                        try:
                            first_item = self.driver.find_element(By.XPATH, '//*[@id="map-violations-detail-datatable"]/tbody/tr[1]/td[1]/a')
                            first_item.click()
                            self.driver.switch_to.window(self.driver.window_handles[2])
                            self.driver.close()
                        except TimeoutException:
                            print('Timeout to click first item in ' + seller_name_by_row)
                        time.sleep(1)
                        self.driver.switch_to.window(self.driver.window_handles[1])

                        try:
                            advertised_price = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_advertised_price)
                            advertised_price.click()
                            time.sleep(1)
                            self.driver.switch_to.window(self.driver.window_handles[2])
                            # self.driver.save_screenshot(current_competitor + '_' + platform + '_advertised_price.png')
                            self.driver.close()
                        except TimeoutException:
                            print('Timeout to click advertised price in ' + seller_name_by_row)

                        self.driver.switch_to.window(self.driver.window_handles[1])

                        try:
                            screenshot_link = self.driver.find_element(By.XPATH, Locators.cpi_map_violation_screenshot_link)
                            screenshot_link.click()
                            self.driver.switch_to.window(self.driver.window_handles[2])
                            # self.driver.save_screenshot(current_competitor + '_' + platform + '+screenshot_link.png')
                            self.driver.close()

                        except TimeoutException:
                            print('Timeout to screenshot link in ' + seller_name_by_row)

                        self.driver.switch_to.window(self.driver.window_handles[1])
                        cpi_map_violation_result.append(['PLATFORM with Item name', 'PASS'])

                        self.driver.refresh()
                        time.sleep(2)
                        self.driver.close()
                        self.driver.switch_to.window(self.driver.window_handles[0])
                    else:
                        print('Please check first_sku or map_price or price_difference in ' + platform)
                else:
                    print('PLEASE CHECK PLATFORM AND SELLER NAME IN ', seller_name_by_row)
                    cpi_map_violation_result.append([platform + ': RUN DATE OR SELLER NAME', 'FAIL'])
                    self.driver.close()
        except TimeoutException:
            print('TIMEOUT seller_list in ' + platform)