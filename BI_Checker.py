import numpy as np
import pandas as pd
import sys
from datetime import datetime
import os
from os import path


df = pd.read_csv('log/' + 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d") + '.csv', dtype=object, encoding='ISO-8859-1').fillna('')
src = 'log/' + 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d") + '.csv'
dst = 'log/' + 'BI_GROW_BY_DATA_LOG.csv'
if path.exists(dst):
    os.remove(dst)
os.rename(src, dst)
OK_Count = len(df[df['status'] == 'OK'])
Total_Status = df['status'].count()
print(Total_Status)
if (Total_Status+1) < 267:
    print('Please Rerun the TEST')
    sys.exit()
if OK_Count == Total_Status:
    print('BI CHECK OK')
else:
    print('BI CHECK NOT OK')
    print(df[df['status'] == 'NOT OK'])


