import unittest
import sys
import os
from locators.Locators_Ganga import  Locators
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))

if Locators.run_time == "PREV":
    test_modules = [
        'Tests.test_PrevDateChecker'
    ]
else:
    test_modules = [
        'Tests.test_AutoDateChecker_ganga'
    ]


suite = unittest.TestSuite()

for test in test_modules:
    try:
        mod = __import__(test, globals(), locals(), ['suite'])
        suite_fn = getattr(mod, 'suite')
        suite.addTest(suite_fn())
    except (ImportError, AttributeError):

        suite.addTest(unittest.defaultTestLoader.loadTestsFromName(test))

unittest.TextTestRunner().run(suite)