import pyodbc
import Config.Config as config


# function to return sql connection
def getConnection():
    connection = pyodbc.connect("Driver= {" + config.DATABASE_CONFIG["Driver"] + "} ;"
                                                                                   "Server=" + config.DATABASE_CONFIG[
                                      "Server"] + ";"
                                                  "Database=" + config.DATABASE_CONFIG["Database"] + ";"
                                                                                                     "uid=" +
                                  config.DATABASE_CONFIG["UID"] + ";"
                                                                  "pwd=" + config.DATABASE_CONFIG["Password"] + "")
    return connection


def getStoreProdValue(query, column_of_stored_prod):
    cursor = getConnection().cursor()

    a = []
    test = []
    cursor.execute(query)

    for row in cursor:
        for item in row:
            a.append(item)

    length = 0
    for i in range(int(len(a) / int(column_of_stored_prod))):
        temp = []
        count = 0
        while count < 4:
            count = count + 1
            temp.append(a[length])
            length = length + 1
        test.append(temp)
    return test


def getMapVoilationSKU(query):
    cursor = getConnection().cursor()

    cursor.execute(query)

    for row in cursor:
        return row
