import os


class Locators(object):
    # Login
    logo = '//*[@id="container"]/header/span[2]/img'
    forget_password = '//*[@id="login"]/div[2]/span[3]/a'
    version = '/html/body/div/div/div[1]/div/h6[2]'

    username = '//*[@id="username"]'
    password = '//*[@id="password"]'
    login = '//*[@id="fm1"]/section[4]/input[4]'
    # account_id = os.environ["Account_id"]
    account_id = '0011T00002Jhg9hQAB'
    # account_id = '0011T00002RVZgtQAH'
    # account_id = '0011T00002MWY5NQAX'
    # account_name = os.environ["Account_name"]
    account_name = " "
    # run_time = os.environ["Run_time"]
    run_time = 'AUTO'

    # BI dashboard https://bi.exclusiveconcepts.com/reports/v2/view/usage_overview
    report_views = '//*[@id="Report-V2-Wrapper"]'
    bi_to_cpi_switch_button  = '/html/body/div[1]/header/nav/div[2]/ul/li[4]'
    bi_to_cpi_switch_link = '/html/body/div[1]/header/nav/div[2]/ul/li[4]/ul/li/a'

    #cpi dropdown menu
    cpi_dropdown_menu = '//*[@id="account_reports"]/a'
    cpi_assortment_menu = '//*[@id="report_ul"]/li[2]/a[contains(text(),"Assortment")]'

    # cpi updateprice page
    cpi_update_price_platform = '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/button'
    cpi_update_price_platform_ul = '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul'
    cpi_run_date = '//*[@id="Rundate"]/option'
    filter_button = '//*[@id="Filter"]'
    cpi_site_update_price_category_filter = '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/button'
    cpi_site_update_price_category_filter_select_all = '//*[@id="frmPriceUpdate"]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/ul/li[2]/a/label/input'
    cpi_site_update_price_product_count = '//*[@id="htStatus"]'
    #competitor_report page
    cpi_site_comp_report_platform = '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/button'
    cpi_comp_report_platform_ul = '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[1]/span[1]/div/ul'
    competitor_report_product_count = '//*[@id="htStatus"]'
    cpi_competitor_run_date = '//*[@id="Rundate"]/option'
    sellers = '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[8]/div[2]/span[1]/div/button/span'
    cpi_competitors_page_filter = '//*[@id="fclear"]'
    cpi_comp_report_price_category_filter = '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/button'
    cpi_comp_report_price_category_filter_select_all = '/html/body/div[1]/div[2]/div[1]/div/div/div[1]/div/div[1]/div[5]/span[1]/div/ul/li[2]/a/label'

    #assortment page
    cpi_assortment_platform = '//*[@id="v2filter-form"]/div[1]/div/div/button'
    cpi_assortment_platform_ul = '//*[@id="v2filter-form"]/div[1]/div/div/ul'
    cpi_assortment_platform_btn = '//*[@id="v2filter-form"]/div[1]/div/div/button'
    cpi_assortment_apply_btn = '//*[@id="apply-filter"]'
    cpi_assortment_run_date = '//*[@id="v2filter-form"]/div[2]/div/div/button/span'

    # map violation page
    cpi_map_violation_platform = '//*[@id="v2filter-form"]/div[1]/div/div/button'
    cpi_map_violation_platform_ul = '//*[@id="v2filter-form"]/div[1]/div/div/ul'
    cpi_map_violation_apply_btn = '//*[@id="apply-filter"]'
    cpi_map_violation_run_date = '//*[@id="v2filter-form"]/div[2]/div/div/button/span'
    cpi_map_violation_table_data = '//*[@id="seller-map-violation-datatable"]/tbody'

    cpi_map_violation_default_platform = '//*[@id="v2filter-form"]/div[1]/div/div/button/span'
    cpi_map_violation_competitor_name = '//*[@id="v2filter-form"]/div[3]/div/div/button/span'
    cpi_map_violation_map_price = '//*[@id="map-violations-detail-datatable"]/tbody/tr[1]/td[3]'
    cpi_map_violation_price_difference = '//*[@id="map-violations-detail-datatable"]/tbody/tr[1]/td[5]'
    cpi_map_violation_advertised_price = '//*[@id="map-violations-detail-datatable"]/tbody/tr[1]/td[4]/a'
    cpi_map_violation_screenshot_link = '//*[@id="map-violations-detail-datatable"]/tbody/tr[1]/td[6]/a'

