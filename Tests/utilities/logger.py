import logging


class Logger:
    #server
    log_path = "D:\Release Engineering\CPI\CPI_Selenium_UnitTest_Phase_6\cpi_automation\log\logs.log"
    #local path
    # log_path = "D:\Release Engineering\cpi_automation_6\logs.txt"
    # log_path = "E:\cpi_automation\cpi_phase_5\log\logs.log"
    logging.basicConfig(filename=log_path, format='%(asctime)s %(message)s', level=logging.INFO, datefmt='%m/%d/%Y %I:%M:%S %p')

    @staticmethod
    def add_log(log):
        logging.info(log)

    @staticmethod
    def add_error(error):
        logging.error(error)
