import pandas as pd
from datetime import datetime


class Reporter:
    def __init__(self, page):
        self.page = page
        self.log_path = 'log//'
        self.file_name = 'BI_GROW_BY_DATA_' + datetime.now().strftime("%Y_%m_%d")
        self.full_path = self.log_path + self.file_name + '.csv'

        try:
            df = pd.read_csv(self.full_path, dtype=object, encoding='ISO-8859-1').fillna('')
        except FileNotFoundError:
            df = pd.DataFrame(columns=['page', 'message', 'status'])

        self.df = df

    def append_row(self, message, status):
        temp_df = pd.DataFrame()
        temp_df['page'] = [self.page]
        temp_df['message'] = [message]
        temp_df['status'] = [status]

        self.df = self.df.append(temp_df)

    def report(self):
        self.df.to_csv(self.full_path, index=False)
