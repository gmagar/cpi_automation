import unittest
import time
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from selenium import webdriver
from Config import Config
from Tests.utilities.reporter import Reporter
from Tests.utilities.timer import Timer
from Pages.DashboardPage import dashboardpageclass
from Pages.AccountPage import accountclass
from Pages.LoginPage import loginpageclass
from Pages.LogoutPage import logoutclass


class AccountCheck(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # cls.options = webdriver.ChromeOptions()
        # cls.options.add_argument('window-size=1920,1080')
        # # cls.options.add_argument('disable-extensions')
        # cls.options.add_argument('start-maximized')
        # cls.options.add_argument('headless')
        # cls.driver = webdriver.Chrome(options=cls.options)
        cls.driver = webdriver.Chrome()
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_account_check(self):
        driver = self.driver

        timer = Timer()

        driver.get("https://bi.growbydata.com")
        login = loginpageclass(driver)
        login.set_username(Config.BI_GROWBYDATA_LOGIN_USERNAME)
        login.set_password(Config.BI_GROWBYDATA_LOGIN_PASSWORD)
        login.submit()

        reporter = Reporter('Account Page')
        dashboard = dashboardpageclass(driver)
        dashboard.wait_loader()

        dashboard.wait_report_loader()
        dashboard.wait_retrieve_report_loader()

        dashboard.wait_dashboard_load()

        dashboard.account_side_bar_click()

        account = accountclass(driver)
        reporter = Reporter('Account Page')
        account.check_loading_message(reporter)
        account.wait_page_load(reporter)
        account.check__all_button_color(reporter)
        account.set_account_name(reporter)
        time.sleep(2)
        account.click_first_account(reporter)
        account.wait_account_detail(reporter)
        account.click_price_intelligence_report(reporter)
        account.wait_cpi_report_load(reporter)

        reporter = Reporter('CPI Assortment')
        account.click_cpi_assortment(reporter)
        account.check_main_loader_and_top_logo(reporter)
        start = timer.get_time()
        account.wait_cpi_assortment_load(reporter)
        end = timer.get_time()
        duration = timer.get_elapsed(start, end)
        reporter.append_row('CPI Assortment Load', duration)
        reporter.report()
        account.check_cpi_button_color(reporter)

        reporter = Reporter('CPI Dashboard')
        account.click_cpi_dashboard(reporter)
        account.check_main_loader_and_top_logo(reporter)
        start = timer.get_time()
        account.wait_cpi_dashboard(reporter)
        end = timer.get_time()
        duration = timer.get_elapsed(start, end)
        reporter.append_row('CPI Dashboard Load', duration)
        reporter.report()
        account.check_cpi_button_color(reporter)

        account.cpi_dashboard_text_check(reporter)
        account.temp(reporter)

        reporter = Reporter('CPI Insights')
        account.click_cpi_insights(reporter)
        account.check_main_loader_and_top_logo(reporter)
        start = timer.get_time()
        account.wait_cpi_assortment_load(reporter)
        end = timer.get_time()
        duration = timer.get_elapsed(start, end)
        reporter.append_row('CPI Insights Load', duration)
        reporter.report()
        account.check_cpi_button_color(reporter)

        reporter = Reporter('CPI Map Violations')
        account.click_map_violations(reporter)
        account.check_main_loader_and_top_logo(reporter)
        start = timer.get_time()
        account.wait_cpi_assortment_load(reporter)
        end = timer.get_time()
        duration = timer.get_elapsed(start, end)
        reporter.append_row('CPI Map Violations Load', duration)
        reporter.report()
        account.check_cpi_button_color(reporter)

        reporter = Reporter('CPI Nearest Competitors')
        account.click_nearest_competitors(reporter)
        account.check_main_loader_and_top_logo(reporter)
        start = timer.get_time()
        account.wait_cpi_assortment_load(reporter)
        end = timer.get_time()
        duration = timer.get_elapsed(start, end)
        reporter.append_row('CPI Nearest Competitors Load', duration)
        reporter.report()
        account.check_cpi_button_color(reporter)

        reporter = Reporter('CPI Product Detail')
        account.click_product_detail(reporter)
        start = timer.get_time()
        account.check_main_loader_and_top_logo(reporter)
        account.wait_product_detail_load(reporter)
        account.check_cpi_button_color(reporter)
        end = timer.get_time()
        duration = timer.get_elapsed(start, end)
        reporter.append_row('CPI Product Detail Load', duration)
        reporter.report()
        account.generate_report()

        logout = logoutclass(driver)
        logout.click_user_icon()
        time.sleep(3)
        logout.click_logout()



    @classmethod
    def tearDownClass(cls):
        # time.sleep(4)
        cls.driver.close()
        cls.driver.quit()
        # pass
