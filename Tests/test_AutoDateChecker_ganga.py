import os
import unittest
import time
import datetime

from Config.driver_builder import DriverBuilder
from locators.Locators_Ganga import Locators
from Pages.LoginPage import loginpageclass
from Config import Config
from Pages.BI_Dashboard import bidashboardpageclass
from selenium.webdriver.common.by import By
from Pages.CPI_Dashboard import cpidashboardpageclass
from Pages.CPI_UpdatePrice import cpiupdatepricepageclass
from Pages.CPI_Insights import cpiInsightclass
from Pages.CPI_CompetitorsReport_ganga import cpicompetitorsreport
from Pages.CPI_Assortment_v2 import cpiassortmentclass
from Pages.CPI_MapViolations import cpimapviolationclass
from Pages.CPI_Nearest5CompetitorsV2 import cpinearest5competitorsclass
from Pages.CPI_PerformanceOview import cpiperformanceoviewclass
from Pages.CPI_ProductDetails import cpiproductdetalpageclass
from tabulate import tabulate

class DateChecker(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        driver_builder = DriverBuilder()
        if Locators.account_id != ' ':
            download_path = "D:\\Release Engineering\\CPI\\CPI_Selenium_UnitTest_Phase_6\\cpi_automation\\map_files\\" + str(
                Locators.account_id) + "\\"

            # download_path = "E:\cpi_automation\cpi_phase_5\\map_files\\" + str(
            #     Locators.account_id) + "\\"

            cls.map_folder = Locators.account_id
        else:
            download_path = "D:\\Release Engineering\\CPI\\CPI_Selenium_UnitTest_Phase_6\\cpi_automation\\map_files\\" + str(
                Locators.account_name) + "\\"
            # download_path = "E:\cpi_automation\cpi_phase_5\\map_files\\" + str(
            #     Locators.account_id) + "\\"
            cls.map_folder = Locators.account_name
        cls.driver = driver_builder.get_driver(download_path, headless=True)

    def test_date(self):
        driver = self.driver
        driver.get("https://bi.exclusiveconcepts.com")
        login = loginpageclass(driver)
        login.set_username(Config.BI_GROWBYDATA_LOGIN_USERNAME)
        login.set_password(Config.BI_GROWBYDATA_LOGIN_PASSWORD)
        login.submit()

        # BI dashboard
        bi_dashboard = bidashboardpageclass(driver)
        bi_dashboard.wait_dashboard_load()
        bi_dashboard.switch_bi_to_cpi_button_click()
        bi_dashboard.switch_cpi_window()
        bi_dashboard.check_is_cpi_tab_open()

        # Get Account name from db
        if Locators.account_id != ' ':
            account_name = bi_dashboard.get_account_name_from_db(Locators.account_id)
        else:
            account_name = Locators.account_name
        print('ACCOUNT: ' + str(account_name))
        print(' ')
        self.driver.find_element(By.XPATH, '//*[@id="accounts"]').send_keys(account_name)
        self.driver.refresh()

        cpi_platform = {
            "Google": '1',
            "Amazon": '2',
            "Basspro": '3',
            "Google Related": '4',
            "Amazon Related": '5',
            'United States': '6',
            'Canada': '7',
            'France': '8',
            'Australia': '9',
            'UK': '10',
            'HongKong': '11',
            'Singapore': '12',
            'SouthKorea': '13',
            'Japan': '14',
            'Taiwan': '15',
            'Amazon Mexico': '16',
            'Walmart Mexico': '17',
            'Mexico': '18',
            'Walmart US': '20',
            'Google Canada': '21',
            'Amazon Canada': '22',
            'Google Weekly': '23',
            'Amazon Weekly': '24'
        }

        today_date = datetime.date.today().strftime("%m/%d/%Y")


        # cpi dashboard load
        cpi_dashboard = cpidashboardpageclass(driver)
        cpi_dashboard.wait_dashboard_load()

        # insight
        cpi_insights = cpiInsightclass(driver)
        # 1. cpi update price section
        cpi_update_price = cpiupdatepricepageclass(driver)

        # competitor report
        cpi_competitor_report = cpicompetitorsreport(driver)

        # assortment page
        cpi_assortment = cpiassortmentclass(driver)

        # map violation page
        cpi_map_violation = cpimapviolationclass(driver)

        # Nearest 5 Competitor v2
        cpi_nearest5_competitor = cpinearest5competitorsclass(driver)

        # Performance Overview
        cpi_performance_overview = cpiperformanceoviewclass(driver)

        # ProductDetail Page
        cpi_product_detail = cpiproductdetalpageclass(driver)


        #1. Competitors Page
        self.main_competitors_page(cpi_competitor_report, account_name, today_date)
        self.driver.refresh()

        #2 & 3. Update Price and Insight Page
        self.main_update_price_nInsight_page(cpi_update_price, account_name, today_date, cpi_insights)
        self.driver.refresh()

        #4. Map Violations
        self.main_map_violation_page(cpi_map_violation, account_name, today_date)
        self.driver.refresh()

        #5. Nearest 5 Comepetitors
        self.main_nearest5_competitors_page(cpi_nearest5_competitor, account_name, today_date)
        test_performance_by_sku = self.driver.find_element(By.XPATH, '//*[@id="cpi_nearest_5_competitors-datatable"]/tbody/tr[1]/td[2]/a').text
        self.driver.refresh()

        #6. Performance Overview
        cpi_performance_overview.switch_performance_overview_page()
        self.driver.back()

        #7. Product Detail
        self.main_product_details_page(cpi_product_detail, account_name, today_date, test_performance_by_sku)
        time.sleep(1)

        #8. Assortment Page
        self.main_assortment_page(cpi_assortment, account_name, today_date)
        self.driver.quit()

    def main_competitors_page(self, cpi_competitor_report, account_name, today_date ):
        cpi_competitor_report.switch_to_competitor_report_menu()
        cpi_competitor_report_platform = cpi_competitor_report.get_platform()
        if len(cpi_competitor_report_platform) > 1:
            cpi_competitor_report_platform = [platform.replace('Google Shopping', 'Google') for platform in
                                              cpi_competitor_report.get_platform()]
            cpi_competitor_report_platform.pop(0)

        print('Checking Detail on Competitor Report Page')
        for i in range(len(cpi_competitor_report_platform)):
            cpi_competitor_report.select_platform(cpi_competitor_report_platform[i])
            cpi_competitor_report_run_date = cpi_competitor_report.get_run_date()
            competitor_report_result = []
            load_time = self.calc_load_time()
            competitor_report_result.append(['Total page load time', str(load_time) +' millsec'])

            cpi_competitor_report.get_sellers(competitor_report_result)

            if cpi_competitor_report_run_date == today_date:
                cpi_competitor_report_product_count = cpi_competitor_report.get_total_product()

                if int(cpi_competitor_report_product_count) > 0:
                    cpi_competitor_report.select_price_category()
                    time.sleep(5)

                    cpi_competitor_report_clickable_index = cpi_competitor_report.get_clickable_sku_index()
                    sku_name = cpi_competitor_report.get_first_sku_name(cpi_competitor_report_clickable_index)
                    cpi_competitor_report.click_first_sku(cpi_competitor_report_clickable_index,
                                                          competitor_report_result, cpi_competitor_report_platform[i],
                                                          sku_name)

                    # iframe
                    try:
                        iframe = self.driver.find_element(By.XPATH, '/html/body/div/div/section/div/iframe')
                        self.driver.switch_to.frame(iframe)
                    except:
                        pass

                    cpi_competitor_report.click_first_item(cpi_competitor_report_clickable_index,
                                                           competitor_report_result, cpi_competitor_report_platform[i])


                    # iframe
                    try:
                        iframe = self.driver.find_element(By.XPATH, '/html/body/div/div/section/div/iframe')
                        self.driver.switch_to.frame(iframe)
                    except:
                        pass

                    cpi_competitor_report.get_item_cost(cpi_competitor_report_clickable_index, competitor_report_result,
                                                        cpi_competitor_report_platform[i])

                    cpi_competitor_report.get_floor_price(cpi_competitor_report_clickable_index,
                                                          competitor_report_result, cpi_competitor_report_platform[i])

                    cpi_competitor_report.map_violation_download_option(cpi_competitor_report_clickable_index)

                    cpi_competitor_report.get_price(cpi_competitor_report_clickable_index, competitor_report_result)

                    # Not Needed right now
                    # cpi_competitor_report.suggested_price(cpi_competitor_report_clickable_index,
                    #                                       competitor_report_result, cpi_competitor_report_platform[i])
                    print("COMPETITORS REPORT CHECKER[ " + cpi_competitor_report_platform[i] + ' ]')
                    headers = ['TEST_CASE', 'RESULT', 'VALUES']
                    print(tabulate(competitor_report_result, headers))
                    print('')
                    print('')
                else:
                    print(str(cpi_competitor_report_platform[i]) + "'s has no value")
            else:
                print(str(account_name) + ' has not run Today for ' + cpi_competitor_report_platform[i])
                self.driver.save_screenshot('COMPETITOR_REPORT_NOT_RUN.png')

    def main_update_price_nInsight_page(self, cpi_update_price, account_name, today_date, cpi_insights):
        cpi_update_price.switch_to_update_price_menu()

        # iframe
        try:
            iframe = self.driver.find_element(By.XPATH, '/html/body/div/div/section/div/iframe')
            self.driver.switch_to.frame(iframe)
        except:
            pass

        cpi_site_platform = [platform.replace('Google Shopping', 'Google') for platform in
                             cpi_update_price.get_all_platform()]

        cpi_site_platform.pop(0)

        temp_platform = cpi_site_platform.copy()
        # 1. update price menu checked
        print('')
        print('Checking Details in Update Price Page')
        for i in range(len(temp_platform)):
            cpi_update_price.cpi_switch_platform(temp_platform[i])
            update_price_check = []
            load_time = self.calc_load_time()
            update_price_check.append(['Total page load time', str(load_time) + ' millsec'])
            run_date = cpi_update_price.cpi_get_run_date()

            if run_date == today_date:

                cpi_update_price_data_count = cpi_update_price.cpi_product_count()
                if int(cpi_update_price_data_count) > 0:
                    # print("Platform " + cpi_site_platform[i] + "   " + "Total Item: " + cpi_update_price_data_count)
                    if 'Google'.lower() in cpi_site_platform[i].lower().split(' ')[0] or 'Amazon'.lower() in \
                            cpi_site_platform[i].lower().split(' ')[0]:
                        cpi_update_price.filter_cheapest_noncheapest_raise(
                            ['Cheapest / Stay', 'Not Cheapest / Stay', 'Cheapest / Raise', 'Not Cheapest / Raise',
                             'Cheapest / Lower', 'Not Cheapest / Lower', 'Only Seller'])
                    else:
                        cpi_update_price.filter_cheapest_noncheapest_raise(
                            ['Cheapest / Stay', 'Not Cheapest / Stay', 'Cheapest / Raise', 'Not Cheapest / Raise',
                             'Cheapest / Lower', 'Not Cheapest / Lower'])

                    time.sleep(10)
                    self.driver.save_screenshot("life_abundance" + str(i + 1) + ".png")
                    clickable_sku_index = cpi_update_price.get_clickable_sku_index()
                    time.sleep(0.5)
                    # print("Index: ", clickable_sku_index)



                    # update price sku column
                    sku_name = cpi_update_price.get_first_sku_name(clickable_sku_index)

                    # print("First sku_name: ", sku_name)

                    time.sleep(1)

                    # update price sku_column
                    cpi_update_price.cpi_site_click_first_sku(clickable_sku_index, update_price_check, temp_platform[i])

                    # 2. cpi_insights
                    cpi_insights.switch_insight_page()
                    cpi_insights.get_total_product(temp_platform[i], update_price_check, cpi_update_price_data_count)
                    self.driver.back()

                    # iframe
                    try:
                        iframe = self.driver.find_element(By.XPATH, '/html/body/div/div/section/div/iframe')
                        self.driver.switch_to.frame(iframe)
                    except:
                        pass

                    time.sleep(0.8)
                    # update price itemlist column
                    cpi_update_price.cpi_site_click_first_item(clickable_sku_index, update_price_check,
                                                               temp_platform[i])

                    try:
                        # iframe
                        iframe = self.driver.find_element(By.XPATH, '/html/body/div/div/section/div/iframe')
                        self.driver.switch_to.frame(iframe)
                    except:
                        pass

                    # update price cost column checked
                    cpi_update_price.check_first_item_cost(clickable_sku_index, update_price_check, temp_platform[i])

                    # #update price floor_price column checked
                    cpi_update_price.check_first_item_floor_price(clickable_sku_index, update_price_check,
                                                                  temp_platform[i])

                    # update price map_voilation column checked
                    cpi_update_price.check_map_violation_download_option(clickable_sku_index, update_price_check,
                                                                         temp_platform[i])

                    # update price current_price column checked
                    cpi_update_price.check_first_item_current_price(clickable_sku_index, update_price_check,
                                                                    temp_platform[i])

                    # update price suggested_price checked
                    cpi_update_price.check_first_item_suggested_price(clickable_sku_index, update_price_check,
                                                                      temp_platform[i])

                    # update price seller_info
                    # cpi_update_price.click_seller_info(update_price_check, temp_platform[i])

                    # iframe
                    try:
                        iframe = self.driver.find_element(By.XPATH, '/html/body/div/div/section/div/iframe')
                        self.driver.switch_to.frame(iframe)
                    except:
                        pass

                    print('')
                    print("UPDATE PRICE CHECKER [ " + temp_platform[i] + " ]")
                    headers = ['TEST_CASE', 'RESULT', 'VALUES']
                    print(tabulate(update_price_check, headers))
                    print('')
                    print('')
                else:
                    print(str(temp_platform[i]) + "'s Update Price has no value")
            else:
                print(str(account_name) + ' has not run Today for ' + temp_platform[i])
                self.driver.save_screenshot('UPDATE_PRICE_NOT_RUN.png')

    def main_map_violation_page(self, cpi_map_violation, account_name, today_date):
        cpi_map_violation.switch_map_violation_page()
        cpi_map_violation_platform = cpi_map_violation.get_platform()
        cpi_map_violation_platform.pop(0)
        print('')
        print('Checking Details in Map Violation Page')
        for i in range(len(cpi_map_violation_platform)):
            cpi_map_violation.switch_platform(cpi_map_violation_platform[i])
            cpi_map_violation_result = []
            load_time = self.calc_load_time()
            cpi_map_violation_result.append(['Total page load time', str(load_time) + ' millsec'])
            cpi_map_violation_run_date = cpi_map_violation.get_run_date()

            today_date_mv = datetime.date.today()
            if cpi_map_violation_run_date == today_date_mv:
                cpi_map_violation.seller_list(cpi_map_violation_platform[i], cpi_map_violation_result)
                print('')
                print("MAP VIOLATION PAGE CHECKER[ " + cpi_map_violation_platform[i] + " ]")
                headers = ['TEST_CASE', 'RESULT', 'VALUES']
                print(tabulate(cpi_map_violation_result, headers))
                print('')
                print('')
            else:
                print(str(account_name) + ' has not run Today for ' + cpi_map_violation_platform[i])
                self.driver.save_screenshot('MAP_VIOlATIOn_NOT_RUN.png')

    def main_nearest5_competitors_page(self, cpi_nearest5_competitor, account_name, today_date):
        cpi_nearest5_competitor.switch_nearest5_competitors_page()
        cpi_nearest5_competitor_platform = cpi_nearest5_competitor.get_platform()
        cpi_nearest5_competitor_platform.pop(0)
        print('')
        print('Checking Details in Nearest5 Competitors Page')
        for i in range(len(cpi_nearest5_competitor_platform)):
            cpi_nearest5_competitor.switch_platform(cpi_nearest5_competitor_platform[i])
            cpi_nearest5_competitor_result = []
            load_time = self.calc_load_time()
            cpi_nearest5_competitor_result.append(['Total page load time', str(load_time) + ' millsec'])

            run_date = cpi_nearest5_competitor.get_run_date()
            if run_date == today_date:
                sku_name = cpi_nearest5_competitor.first_sku_name()
                cpi_nearest5_competitor.first_item_click()
                cpi_nearest5_competitor.first_sku_click(sku_name, cpi_nearest5_competitor_result,
                                                        cpi_nearest5_competitor_platform[i])
                cpi_nearest5_competitor.first_item_cost(cpi_nearest5_competitor_platform[i],
                                                        cpi_nearest5_competitor_result)
                cpi_nearest5_competitor.first_item_price(cpi_nearest5_competitor_platform[i],
                                                         cpi_nearest5_competitor_result)

                cpi_nearest5_competitor.get_first_competitor_name_price(cpi_nearest5_competitor_result,
                                                                        cpi_nearest5_competitor_platform[i])
                cpi_nearest5_competitor.get_second_competitor_name_price(cpi_nearest5_competitor_result,
                                                                         cpi_nearest5_competitor_platform[i])
                cpi_nearest5_competitor.get_third_competitor_name_price(cpi_nearest5_competitor_result,
                                                                        cpi_nearest5_competitor_platform[i])
                cpi_nearest5_competitor.get_fourth_competitor_name_price(cpi_nearest5_competitor_result,
                                                                         cpi_nearest5_competitor_platform[i])
                cpi_nearest5_competitor.get_fifth_competitor_name_price(cpi_nearest5_competitor_result,
                                                                        cpi_nearest5_competitor_platform[i])
                print('')
                print("NEAREST 5 COMPETITOR PAGE CHECKER[ " + cpi_nearest5_competitor_platform[i] + " ]")
                headers = ['TEST_CASE', 'RESULT', 'VALUES']
                print(tabulate(cpi_nearest5_competitor_result, headers))
                print('')
                print('')
            else:
                print(str(account_name) + ' has not run Today for ' + cpi_nearest5_competitor_platform[i])

    def main_product_details_page(self, cpi_product_detail, account_name, today_date, test_performance_by_sku):
        cpi_product_detail.switch_product_detail_page()
        cpi_product_detail_platform = cpi_product_detail.get_platform()
        if len(cpi_product_detail_platform) > 1:
            cpi_product_detail_platform.pop(0)
        for i in range(len(cpi_product_detail_platform)):
            cpi_product_detail.switch_platform(cpi_product_detail_platform[i])
            cpi_product_detail_result = []
            load_time = self.calc_load_time()
            cpi_product_detail_result.append(['Total page load time', str(load_time) + ' millsec'])
            cpi_product_detail.search_by_sku(test_performance_by_sku)
            cpi_product_detail.check_detail(cpi_product_detail_result, cpi_product_detail_platform[i])

            print('')
            print("Product Detail PAGE CHECKER[ " + cpi_product_detail_platform[i] + " ]")
            headers = ['TEST_CASE', 'RESULT', 'VALUES']
            print(tabulate(cpi_product_detail_result, headers))
            print('')
            print('')

    def main_assortment_page(self, cpi_assortment, account_name, today_date):
        cpi_assortment.switch_to_assortment_menu()
        cpi_assortment_platform = cpi_assortment.get_platform()
        if len(cpi_assortment_platform) > 1:
            cpi_assortment_platform.pop(0)
        print('Checking Detail on Assortment Page')
        for i in range(len(cpi_assortment_platform)):
            cpi_assortment.switch_platform(cpi_assortment_platform[i])
            cpi_assortment_result = []
            load_time = self.calc_load_time()
            cpi_assortment_result.append(['Total page load time', str(load_time) + ' millsec'])
            cpi_assortment_run_date = cpi_assortment.get_run_date()
            if cpi_assortment_run_date == today_date:
                cpi_assortment_result.append([cpi_assortment_platform[i] + ': RUN DATE', 'PASS'])

                print('')
                print("ASSORTMENT PAGE CHECKER[ " + cpi_assortment_platform[i] + " ]")
                headers = ['TEST_CASE', 'RESULT', 'VALUES']
                print(tabulate(cpi_assortment_result, headers))
                print('')
                print('')

            else:
                print(str(account_name) + ' has not run Today for ' + cpi_assortment_platform[i])

    def calc_load_time(self):
        navigationStart = self.driver.execute_script("return window.performance.timing.navigationStart")
        responseStart = self.driver.execute_script("return window.performance.timing.responseStart")
        domComplete = self.driver.execute_script("return window.performance.timing.domComplete")

        backendPerformance_calc = responseStart - navigationStart
        frontendPerformance_calc = domComplete - responseStart
        total_time = backendPerformance_calc + frontendPerformance_calc
        # dict_data = {'Page': page, 'Backend Load Time': backendPerformance_calc,
        #              'Frontend Load Time': frontendPerformance_calc, 'Thread': thread_number}

        return total_time