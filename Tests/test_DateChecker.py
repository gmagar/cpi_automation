import unittest
import time
import sys
import os
import datetime
import pyodbc
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from selenium import webdriver
from selenium.webdriver.common.by import By
from Config import Config
from Tests.utilities.reporter import Reporter
from Tests.utilities.timer import Timer
from locators.Locators import Locators
from Pages.DashboardPage import dashboardpageclass
from Pages.UpdatePricesPage import updatepricesclass
from Pages.CPI_Site_UpdatePricePage import cpisiteupdatepricesclass
from Pages.CPI_Site_DashboardPage import cpisitedashboardpageclass
from Pages.CPI_Site_SpecialistPage import cpisitespecialistclass
from Pages.CompetitorsReportPage import competitorsreportclass
from Pages.CPI_Specialist_Page import cpispecialistclass
from Pages.AccountPage import accountclass
from Pages.LoginPage import loginpageclass
from Pages.LogoutPage import logoutclass
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchWindowException

class DateCheck(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.options = webdriver.ChromeOptions()
        cls.options.add_argument('window-size=1920,1080')
        cls.options.add_argument('disable-extensions')
        cls.options.add_argument('start-maximized')
        cls.options.add_argument('headless')
        cls.driver = webdriver.Chrome('E:\Jenkins_Backup\chromedriver_v78_win32\chromedriver.exe', options=cls.options)
        # cls.driver = webdriver.Chrome('E:\Jenkins_Backup\chromedriver_v76_win32\chromedriver.exe')
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_date_check(self):
        driver = self.driver

        timer = Timer()

        driver.get("https://bi.exclusiveconcepts.com")
        login = loginpageclass(driver)
        login.set_username(Config.BI_GROWBYDATA_LOGIN_USERNAME)
        login.set_password(Config.BI_GROWBYDATA_LOGIN_PASSWORD)
        login.submit()


        dashboard = dashboardpageclass(driver)
        dashboard.wait_dashboard_load()
        dashboard.cpi_side_bar_click()
        dashboard.update_prices_click()

        updateprice = updatepricesclass(driver)
        updateprice.wait_for_page_load()
        if Locators.account_id != ' ':
            account_id_xpath = driver.find_element(By.XPATH, '//select[@id="accounts"]/option[contains(@value,"' + str(Locators.account_id) + '")]')
            account_name = account_id_xpath.text
        else:
            account_name_xpath = driver.find_element(By.XPATH, '//select[@id="accounts"]/option[contains(text(),"' + str(Locators.account_name) + '")]')
            Locators.account_id = account_name_xpath.get_attribute('value')
            account_name = Locators.account_name

        print('ACCOUNT: ' + str(account_name))
        print(' ')
        print(' ')

        dashboard.reports_side_bar_click()
        dashboard.wait_internal_report()
        dashboard.filter_cpi_report('CPI Delivery Status Report V2')
        time.sleep(2)
        dashboard.click_cpi_delievery_status()
        dashboard.wait_cpi_delivery_status_report()
        dashboard.delivery_status_filter_last_run_date()
        dashboard.wait_cpi_delivery_status_report()
        dashboard.delivery_status_filter_last_run_date()
        dashboard.wait_cpi_delivery_status_report()
        time.sleep(2)
        client_run_info = dashboard.internal_report_client(account_name)
        if len(client_run_info) == 0:
            cpi_platform = {"Google": '1', "Amazon": '2', "Google Related": '4', "Amazon Related": '5', 'United States': '6'}
            dashboard.cpi_side_bar_click()
            dashboard.update_prices_click()
            updateprice.select_account()
            try:
                self.driver.switch_to.window(self.driver.window_handles[1])
            except NoSuchWindowException:
                print(str(account_name) + ' has not run Today')
                return 0
            cpi_dashboard = cpisitedashboardpageclass(driver)
            cpi_site_account_name = cpi_dashboard.cpi_site_account_name()
            cpi_dashboard.cpi_site_cpi_side_bar_click()
            cpi_dashboard.cpi_site_cpi_specialist_click()
            cpi_site_spec_report = cpisitespecialistclass(driver)
            cpi_site_spec_report.cpi_site_wait_for_load()
            if cpi_site_spec_report.cpi_site_check_client_run(cpi_site_account_name) is True:
            # if cpi_site_spec_report.cpi_site_check_client_run(cpi_site_account_name) is False:
                cpi_dashboard.cpi_site_cpi_side_bar_click()
                cpi_dashboard.cpi_site_update_prices_click()
                cpi_site_update_price = cpisiteupdatepricesclass(driver)
                cpi_site_platform = cpi_site_update_price.get_platform()
                cpi_site_is_date_present = cpi_site_update_price.is_date_present()
                if cpi_site_is_date_present:
                    cpi_site_platform.pop(0)
                    for i in range(len(cpi_site_platform)):
                        cpi_site_updateprice_date = cpi_site_update_price.cpi_site_get_run_date()
                        today_date = datetime.date.today().strftime("%#m/%#d/%Y")
                        if cpi_site_updateprice_date == today_date:
                        # if cpi_site_updateprice_date:
                            cpi_site_update_price.cpi_site_wait_switch_platform()
                            cpi_site_update_price.select_platform(cpi_site_platform[i])
                            cpi_site_update_price.cpi_site_wait_switch_platform()
                            cpi_site_update_price_date = cpi_site_update_price.cpi_site_get_run_date()
                            cpi_site_update_price_data_count = cpi_site_update_price.cpi_site_get_product_count()
                            if int(cpi_site_update_price_data_count.replace(',', '')) > 0:
                                print('PLATFORM: ' + str(cpi_site_platform[i]))
                                print(' ')
                                sys.stdout.flush()
                                os.system('sqlcmd -S 52.87.5.132 -U sa -P "`Cstum&@dm!N" -d Datahub_Mirror -Q "EXECUTE CPI_Validation_Report \''+str(account_name)+'\',\''+str(datetime.date.today().strftime("%Y-%m-%d"))+'\','+str(cpi_platform[cpi_site_platform[i]])+';"')
                                # os.system('sqlcmd -S 192.168.11.21 -U sa -P GBD@sql_vm_prod -d Datahub_Mirror -Q "EXECUTE cpi_test '+str(account_name)+','+str(datetime.date.today().strftime("%Y-%m-%d"))+','+str(i+1)+';"')
                                # print(str(account_name) + "["+str(cpi_site_platform[i])+"]"+" is PASS")
                                print(' ')
                            else:
                                print(str(cpi_site_platform[i]) + "'s Update Price has no value")
                        else:
                            print(str(account_name) + ' has not run Today')
                else:
                    print(str(account_name) + ' has No Date Present')
                return 0
            else:
                print(str(account_name)+' has not run Today')
                return 0

        dashboard.cpi_side_bar_click()
        dashboard.update_prices_click()

        updateprice.select_account()
        updateprice.wait_account_page()
        platform = updateprice.get_platform()

        is_date_present = updateprice.is_date_present()
        if is_date_present:
            platform.pop(0)
            for i in range(len(client_run_info)):
                print("PLATFORM : " + client_run_info[i][0])
                if "In Progress" in client_run_info[i][2]:
                    print("This run is in Progress")
                    print('')
                    continue
                elif "Failed" in client_run_info[i][2]:
                    print("This run is FAILED")
                    print('')
                    continue
                updateprice_date = updateprice.get_run_date()
                today_date = datetime.date.today().strftime("%Y-%m-%d")
                if updateprice_date == today_date:
                # if updateprice_date:
                    updateprice.reset_slider_value()
                    updateprice.wait_switch_platform()
                    updateprice.clear_filter()
                    updateprice.wait_switch_platform()
                    updateprice.select_price_category()
                    updateprice.wait_switch_platform()
                    # update_price_filter_product_count = updateprice.get_product_count()
                    updateprice.clear_filter()
                    updateprice.wait_switch_platform()

                    updateprice.select_platform(client_run_info[i][0])
                    updateprice.wait_switch_platform()
                    update_price_date = updateprice.get_run_date()
                    update_price_data_count = updateprice.get_product_count()
                    if int(update_price_data_count.replace(',', '')) > 0:
                        sku_name = updateprice.get_first_sku_name()
                        updateprice.click_first_sku()
                        updateprice.wait_cpi_report_load()

                        reporter = Reporter('Product Detail')
                        report = accountclass(driver)
                        product_detail_date = []
                        product_page_elem = self.driver.find_elements(By.XPATH, '//*[@id="block-row1col1"]/div/div/div/div/table/tbody/tr/td[2]/span')
                        if len(product_page_elem) == 0:
                            if "No data available" in driver.page_source:
                                product_detail_date.append('No data present in Product Detail')

                        platform_client_info = [i[0] for i in client_run_info]
                        report_platform = platform_client_info.copy()
                        for j in range(len(report_platform)):
                            if 'Google' in report_platform[j]:
                                report_platform[j] = 'Google Shopping'
                        report.set_platform(report_platform, i, reporter)
                        product_detail_date.append(updateprice.get_run_date_V2())

                        first_report_index = report.get_report_index()

                        reporter = Reporter('CPI Assorment')
                        report.click_cpi_assortment(reporter ,first_report_index+1)
                        report.wait_cpi_assortment_load(reporter)
                        assortment_date = []
                        if "No data available" in driver.page_source:
                            assortment_date.append('No data present in CPI_Assortment')
                        report.set_platform(report_platform, i, reporter)
                        report.wait_cpi_assortment_load(reporter)
                        assortment_date.append(updateprice.get_run_date_V2())

                        time.sleep(5)
                        reporter = Reporter('CPI Dashboard')
                        report.click_cpi_dashboard(reporter, first_report_index+2)
                        report.wait_cpi_dashboard(reporter)
                        dashboard_date = []
                        if "No data available" in driver.page_source:
                            dashboard_date.append('No data present in CPI_Dashboard')
                        report.set_platform(report_platform, i, reporter)
                        report.wait_cpi_dashboard(reporter)
                        dashboard_date.append(updateprice.get_run_date_V2())

                        reporter = Reporter('MAP Violations')
                        report.click_map_violations(reporter, first_report_index+4)
                        map_violation_date = []
                        if "No data available" in driver.page_source:
                            map_violation_date.append('No data present in MAP Violations')
                        report.set_platform(report_platform, i, reporter)
                        report.wait_map_violations(reporter)
                        map_violation_date.append(updateprice.get_run_date_V2())

                        reporter = Reporter('Nearest 5 Competitors')
                        report.click_cpi_nearest_competitors(reporter, first_report_index+5)
                        report.wait_cpi_report(reporter)
                        nearest_5_date = []
                        if "No data available" in driver.page_source:
                            nearest_5_date.append('No data present in Nearest 5 Competitors')
                        # report.set_platform(report_platform, i, reporter)
                        nearest_5_date.append(updateprice.get_run_date_V2())

                        reporter = Reporter('Competitors Report')

                        dashboard.cpi_side_bar_click()
                        time.sleep(2)
                        dashboard.competitor_report_click()

                        comp_report = competitorsreportclass(driver)
                        comp_report.wait_for_page_load()
                        updateprice.select_account()
                        comp_report.wait_competitors_report_page_load()
                        comp_report.clear_filter()
                        comp_report.wait_competitors_report_page_load()
                        comp_report.reset_seller_filter()
                        comp_report.wait_competitors_report_page_load()
                        comp_report.reset_slider_value()
                        comp_report.wait_competitors_report_page_load()
                        comp_report_data_count = comp_report.get_product_count()
                        comp_report_platform = updateprice.get_platform()
                        run_date = {}
                        time.sleep(3)
                        select = Select(driver.find_element_by_xpath('//*[@id="platforms"]'))
                        selected_option = select.first_selected_option
                        # run_date["competitor_report_date_" + selected_option.text] = updateprice.get_run_date()
                        comp_report_platform.remove(selected_option.text)
                        # print('')
                        # print('---COMPETITOR REPORT---')
                        # print("Platform : "+platform[i])
                        updateprice.select_platform(client_run_info[i][0])
                        comp_report.wait_competitors_report_page_load()
                        if 'No data available in table' in self.driver.page_source:
                            run_date["Competitor_Report_Date_"+platform_client_info[i]] = 'Not report available for '+platform_client_info[i]
                        else:
                            run_date["Competitor_Report_Date_"+platform_client_info[i]] = updateprice.get_run_date()

                        time.sleep(5)

                        dashboard.cpi_side_bar_click()
                        dashboard.cpi_specialist_click()

                        spec_report = cpispecialistclass(driver)
                        spec_report.wait_for_load()
                        spec_report.filter_platform(platform_client_info[i])
                        spec_report.wait_for_load()
                        is_client_present = spec_report.is_client_present()
                        specialist_dashboard_date = []
                        if is_client_present == 'True':
                            spec_report.click_account()
                            specialist_dashboard_date.append(str(datetime.datetime.strptime(spec_report.get_run_date(), '%m/%d/%Y').date()))
                        else:
                            specialist_dashboard_date.append('Not Ran Today')
                        spec_report_product_count = spec_report.get_total_product_count()
                        print('')
                        print("SPECIALIST DASHBOARD CHECKER")
                        if update_price_data_count.replace(',', '') == spec_report_product_count:
                            print('Update Price count and Specialist report count is PASS.')
                        else:
                            print('Update Price count and Specialist report count is FAIL.')
                        price_category_to_check, price_category_index, only_seller_index = spec_report.get_price_category_index()
                        spec_report.get_table_data(price_category_to_check, price_category_index, only_seller_index)
                        # spec_report_comp_report_count = spec_report.get_comp_report_product_data()
                        spec_report.attribute_change_page()
                        spec_report.attribute_change_wait()
                        attribute_data_check = spec_report.data_present_attribute_change()

                        # if update_price_filter_product_count.replace(',', '') == spec_report_comp_report_count:
                        #     print('Update Price Cheapest, Non-cheapest count and Specialist report count is PASS')
                        # else:
                        #     print('Update Price Cheapest, Non-cheapest count and Specialist report count is FAIL')

                        if attribute_data_check == 'True':
                            print('Attribute Page PASS')
                        else:
                            print('Attribute Page FAIL')
                        print('')
                        print("DATE VALIDATION")
                        all_dates = {
                            'Assortment Date': assortment_date,
                            'Dashboard Date': dashboard_date,
                            'Map Violation Date': map_violation_date,
                            'Nearest 5 Date': nearest_5_date,
                            'Product Detail Date': product_detail_date,
                            'Specialist Report Date': specialist_dashboard_date
                        }

                        for date, values in all_dates.items():
                            if update_price_date in str(values):
                                len_values = len(values)
                                if len_values > 1:
                                    print(date + ' is equal : ' + str(values[1]) + ' [' + str(values[0]) + '].')
                                else:
                                    print(date + ' is equal : ' + str(values[0]) + '.')
                            else:
                                len_values = len(values)
                                if len_values > 1:
                                    print(date + ' is not equal : ' + str(values[1]) + ' [' + str(values[0]) + '].')
                                else:
                                    print(date + ' is not equal : ' + str(values[0]) + '.')

                        for date, values in run_date.items():
                            if values == update_price_date:
                                print(date + ' is equal : ' + str(values)+'.')
                            else:
                                print(date + ' is not equal : ' + str(values)+'.')

                        print('')
                        print('')

                        driver.close()
                        driver.switch_to.window(self.driver.window_handles[0])
                        time.sleep(5)
                    else:
                        print(str(Locators.account_id)+"'s Update Price has no value")
                else:
                    print(str(account_name)+'has not run Today')
        else:
            print(str(account_name)+'has No Date Present')

    @classmethod
    def tearDownClass(cls):
        # time.sleep(4)
        # cls.driver.close()
        # cls.driver.quit()
        pass