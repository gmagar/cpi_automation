import unittest
import time
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from selenium import webdriver
from Config import Config
from Tests.utilities.timer import Timer
from Tests.utilities.reporter import Reporter
from Pages.DashboardPage import dashboardpageclass
from Pages.CompetitorsReportPage import competitorsreportclass
from Pages.UpdatePricesPage import updatepricesclass
from Pages.PerformanceReportPage import performancereportclass
from Pages.CPI_Specialist_Page import cpispecialistclass
from Pages.ProductPage import productpageclass
from Pages.LoginPage import loginpageclass
from Pages.LogoutPage import logoutclass


class simplesearch(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.options = webdriver.ChromeOptions()
        cls.options.add_argument('window-size=1920,1080')
        # cls.options.add_argument('disable-extensions')
        cls.options.add_argument('start-maximized')
        cls.options.add_argument('headless')
        cls.driver = webdriver.Chrome(options=cls.options)
        # cls.driver = webdriver.Chrome()
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()

    def test_Simple_Search(self):
        driver = self.driver

        timer = Timer()
        run_time = timer.get_time()

        driver.get("https://bi.growbydata.com")
        login = loginpageclass(driver)
        login.check_logo()
        login.check_forget_password()
        login.set_username(Config.BI_GROWBYDATA_LOGIN_USERNAME)
        login.set_password(Config.BI_GROWBYDATA_LOGIN_PASSWORD)
        login.submit()

        dashboard = dashboardpageclass(driver)
        dashboard.wait_loader()
        dashboard.check_loader_and_top_logo()
        # dashboard.check_loader_loading_icon()

        dashboard.wait_report_loader()
        dashboard.check_loader_and_top_logo()
        dashboard.wait_retrieve_report_loader()
        # dashboard.check_loader_loading_icon()

        dashboard.wait_dashboard_load()
        dashboard.check_page_for_exclusive()
        time.sleep(3)
        dashboard.check_button_color()
        time.sleep(3)

        dashboard.cpi_side_bar_click()
        dashboard.competitor_report_click()

        competitorsreport = competitorsreportclass(driver)
        competitorsreport.wait_for_page_load()
        competitorsreport.check_go_color()
        competitorsreport.check_page_for_exclusive()
        competitorsreport.select_account()
        # competitorsreport.click_go()
        competitorsreport.check_load_message()
        competitorsreport.wait_competitors_report_page_load()
        time.sleep(5)
        competitorsreport.check_button_in_main_competitor_report()
        competitorsreport.check_navigation_color()
        competitorsreport.check_page_for_exclusive()
        competitorsreport.change_slider_value()
        competitorsreport.check_load_message()
        competitorsreport.wait_change_slider_report_page_load()

        time.sleep(3)
        dashboard.cpi_side_bar_click()
        dashboard.update_prices_click()

        updateprice = updatepricesclass(driver)
        updateprice.check_loading_message()
        updateprice.wait_for_page_load()
        updateprice.check_page_for_exclusive()
        updateprice.check_button_color()
        updateprice.change_slider_value()
        updateprice.check_loading_message()
        updateprice.check_navigation_color()

        time.sleep(4)

        updateprice.click_first_sku()

        updateprice.check_loader_and_top_logo()

        updateprice.wait_sku_detail_load()
        updateprice.check_page_for_exclusive()
        updateprice.check_dashboard_button_color()

        driver.close()
        driver.switch_to.window(driver.window_handles[0])

        dashboard.cpi_side_bar_click()
        dashboard.performance_report_click()

        time.sleep(5)

        performreport = performancereportclass(driver)
        performreport.wait_page_load()
        performreport.check_page_for_exclusive()
        performreport.first_item_click()
        performreport.check_button_color()

        dashboard.cpi_side_bar_click()
        dashboard.cpi_specialist_click()

        cpispecialist = cpispecialistclass(driver)
        cpispecialist.check_load_message()
        cpispecialist.wait_for_load()
        cpispecialist.check_page_for_exclusive()
        cpispecialist.click_first_item()
        cpispecialist.check_button_color()

        logout = logoutclass(driver)
        logout.click_user_icon()
        logout.check_button_color()
        time.sleep(3)
        logout.click_logout()



    @classmethod
    def tearDownClass(cls):
        # time.sleep(4)
        cls.driver.close()
        cls.driver.quit()
        # pass
