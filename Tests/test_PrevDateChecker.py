import unittest
import time
import sys
import os
import datetime
import pyodbc
import HTMLTestRunner
from tabulate import tabulate
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from selenium import webdriver
from selenium.webdriver.common.by import By
from Config import Config
from Config.driver_builder import DriverBuilder
from Tests.utilities.reporter import Reporter
from Tests.utilities.timer import Timer
from Tests.utilities.db_connection import db_connection
from locators.Locators import Locators
from Pages.DashboardPage import dashboardpageclass
from Pages.UpdatePricesPage import updatepricesclass
from Pages.CPI_Site_UpdatePricePage import cpisiteupdatepricesclass
from Pages.CPI_Site_DashboardPage import cpisitedashboardpageclass
from Pages.CPI_Site_SpecialistPage import cpisitespecialistclass
from Pages.CompetitorsReportPage import competitorsreportclass
from Pages.CPI_Specialist_Page import cpispecialistclass
from Pages.CPI_Site_CompetitorsReportPage import cpisitecompetitorsreportclass
from Pages.Nearest5CompetitorPage import nearestcompetitorclass
from Pages.AccountPage import accountclass
from Pages.LoginPage import loginpageclass
from Pages.LogoutPage import logoutclass
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchWindowException

class DateCheck(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        driver_builder = DriverBuilder()
        if Locators.account_id != ' ':
            download_path = "D:\\Release Engineering\\CPI\\CPI_Selenium_UnitTest_Phase_5\\CPI_Selenium_UnitTest\\map_files\\"+str(Locators.account_id)+"\\"
            cls.map_folder = Locators.account_id
        else:
            download_path = "D:\\Release Engineering\\CPI\\CPI_Selenium_UnitTest_Phase_5\\CPI_Selenium_UnitTest\\map_files\\" + str(Locators.account_name)+"\\"
            cls.map_folder = Locators.account_name
        cls.driver = driver_builder.get_driver(download_path, headless=True)

    def test_date_check(self):
        driver = self.driver

        timer = Timer()

        driver.get("https://bi.exclusiveconcepts.com")
        login = loginpageclass(driver)
        login.set_username(Config.BI_GROWBYDATA_LOGIN_USERNAME)
        login.set_password(Config.BI_GROWBYDATA_LOGIN_PASSWORD)
        login.submit()

        temp = getattr(Locators, Locators.run_time)

        dashboard = dashboardpageclass(driver)
        dashboard.wait_dashboard_load()

        dashboard.cpi_switch_button_click()
        dashboard.switch_cpi_window()
        dashboard.check_is_cpi_tab_open()

        updateprice = updatepricesclass(driver)

        if Locators.account_id != ' ':
            account_name = dashboard.get_account_name_from_db(Locators.account_id)
            # account_id_xpath = driver.find_element(By.XPATH, '//select[@id="accounts"]/option[contains(@value,"' + str(Locators.account_id) + '")]')
            # account_name = account_id_xpath.text
        else:
            account_name = Locators.account_name

        print('ACCOUNT: ' + str(account_name))
        print(' ')
        print(' ')

        cpi_platform = {"Google": '1',
                        "Amazon": '2',
                        "Basspro": '3',
                        "Google Related": '4',
                        "Amazon Related": '5',
                        'United States': '6',
                        'Canada': '7',
                        'France': '8',
                        'Australia': '9',
                        'UK': '10',
                        'HongKong': '11',
                        'Singapore': '12',
                        'SouthKorea': '13',
                        'Japan': '14',
                        'Taiwan': '15',
                        'Amazon Mexico': '16',
                        'Walmart Mexico': '17',
                        'Mexico': '18',
                        'Walmart US': '20',
                        'Google Canada': '21',
                        'Amazon Canada': '22',
                        'Google Weekly': '23',
                        'Amazon Weekly': '24'}

        cpi_dashboard = cpisitedashboardpageclass(driver)
        cpi_site_update_price = cpisiteupdatepricesclass(driver)
        cpi_dashboard.wait_dashboard_load()
        cpi_dashboard.cpi_site_select_account(account_name)
        cpi_site_update_price.cpi_site_wait_switch_platform()
        cpi_site_platform = [platform.replace('Google Shopping', 'Google') for platform in
                                 cpi_site_update_price.get_platform()]
        cpi_site_platform.pop(0)
        temp_platform = cpi_site_platform.copy()
        if len(temp_platform) < 1:
            for i in range(len(temp_platform)):
                cpi_site_update_price.only_switch_platform(temp_platform[i])
                cpi_site_update_price.cpi_site_wait_switch_platform()
                temp_run_date = datetime.datetime.strptime(cpi_site_update_price.cpi_site_get_run_date(),'%m/%d/%Y').strftime("%m/%d/%Y")
                if temp_run_date != datetime.date.today().strftime("%m/%d/%Y"):
                    cpi_site_platform.remove(temp_platform[i])
        if str(account_name) == "Lifes Abundance CPI":
            cpi_site_platform.remove('Amazon')
        today_date = datetime.date.today().strftime("%m/%d/%Y")
        cpi_site_is_date_present = cpi_site_update_price.is_date_present()
        cpi_site_account_name = cpi_dashboard.cpi_site_account_name()
        cpi_dashboard.cpi_site_cpi_side_bar_click()
        cpi_dashboard.cpi_site_cpi_specialist_click()
        cpi_site_spec_report = cpisitespecialistclass(driver)
        cpi_site_spec_report.cpi_site_wait_for_load()
        cpi_site_spec_report.filter_client(cpi_site_account_name,cpi_site_platform[0] if len(cpi_site_platform)>1 else temp_platform[0])
        cpi_site_spec_report.cpi_site_wait_for_load()
        # if cpi_site_spec_report.cpi_site_check_client_run(cpi_site_account_name, datetime.date.today().strftime("%m/%d/%Y")) is True and cpi_site_is_date_present is True:
        if cpi_site_spec_report.cpi_site_check_client_run(cpi_site_account_name, datetime.date.today().strftime("%m/%d/%Y")) is False:
            for i in range(len(cpi_site_platform)):
                cpi_dashboard.cpi_site_reports_bar_click()
                cpi_dashboard.cpi_site_update_prices_click()
                dashboard.switch_cpi_window()
                cpi_site_update_price.cpi_site_wait_switch_platform()
                cpi_site_update_price.reset_slider_value()
                cpi_site_update_price.cpi_site_wait_switch_platform()
                cpi_site_update_price.clear_filter()
                cpi_site_update_price.cpi_site_wait_switch_platform()
                cpi_site_update_price.select_price_category()
                cpi_site_update_price.cpi_site_wait_switch_platform()
                cpi_site_update_price.clear_filter()
                cpi_site_update_price.cpi_site_wait_switch_platform()

                time.sleep(20)
                print('PLATFORM: ' + str(cpi_site_platform[i]))
                print(' ')

                cpi_site_update_price.select_platform(cpi_site_platform[i])
                cpi_site_update_price.cpi_site_wait_switch_platform()

                cpi_site_update_price_non_parsed_date = cpi_site_update_price.cpi_site_get_run_date()
                cpi_site_update_price_date = cpi_site_update_price.cpi_site_get_run_date()
                # if cpi_site_update_price_non_parsed_date == today_date:
                if cpi_site_update_price_date:
                    cpi_site_update_price_date = datetime.datetime.strptime(cpi_site_update_price.cpi_site_get_run_date(), '%m/%d/%Y').strftime("%Y-%m-%d")
                    cpi_site_update_price.reset_slider_value()
                    cpi_site_update_price.cpi_site_wait_switch_platform()
                    cpi_site_update_price.clear_filter()
                    cpi_site_update_price.cpi_site_wait_switch_platform()
                    cpi_site_update_price.select_price_category()
                    cpi_site_update_price.cpi_site_wait_switch_platform()
                    cpi_site_update_price.clear_filter()
                    cpi_site_update_price.cpi_site_wait_switch_platform()

                    time.sleep(20)

                    cpi_site_update_price_data_count = cpi_site_update_price.cpi_site_get_product_count()
                    onhold_count = cpi_site_update_price.get_on_hold_count()
                    # print(cpi_site_update_price_data_count, onhold_count)
                    cpi_site_update_price_total_count = int(cpi_site_update_price_data_count) + int(onhold_count)
                    # print(cpi_site_update_price_total_count)
                    cpi_site_update_price_table_data_count = cpi_site_update_price.cpi_site_get_table_data_count()
                    # print(cpi_site_update_price_table_data_count)
                    if int(cpi_site_update_price_total_count) > 0 and int(cpi_site_update_price_table_data_count) > 1:
                    # if int(cpi_site_update_price_total_count) > 0:
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        cpi_site_update_price.click_brand()
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        if 'Google'.lower() in cpi_site_platform[i].lower().split(' ')[0] or 'Amazon'.lower() in cpi_site_platform[i].lower().split(' ')[0]:
                            cpi_site_update_price.filter_cheapest_nonc_raise(['Cheapest / Stay', 'Not Cheapest / Stay', 'Cheapest / Raise', 'Not Cheapest / Raise', 'Cheapest / Lower', 'Not Cheapest / Lower', 'Only Seller'])
                        else:
                            cpi_site_update_price.filter_cheapest_nonc_raise(['Cheapest / Stay', 'Not Cheapest / Stay', 'Cheapest / Raise', 'Not Cheapest / Raise', 'Cheapest / Lower', 'Not Cheapest / Lower'])
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        self.driver.save_screenshot("life_abundance.png")
                        clickable_sku_index = cpi_site_update_price.get_clickable_sku_index()
                        sku_name = cpi_site_update_price.get_first_sku_name(clickable_sku_index)
                        cpi_site_update_price.cpi_site_click_first_sku(clickable_sku_index)

                        update_price_check = []
                        cpi_site_update_price.cpi_site_wait_sku_click()
                        lowest_competitor_name = cpi_site_update_price.check_click_first_sku(sku_name, str(cpi_site_platform[i]), update_price_check)
                        driver.close()
                        driver.switch_to.window(self.driver.window_handles[0])
                        cpi_site_update_price.cpi_site_click_first_item(clickable_sku_index)
                        cpi_site_update_price.check_click_first_item(str(cpi_site_platform[i]), update_price_check,
                                                                         lowest_competitor_name)

                        cpi_site_update_price.select_price_category()
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        # map_sku = cpi_site_update_price.get_map_sku(cpi_site_update_price_date, cpi_site_account_name)
                        # cpi_site_update_price.filter_product_sku(map_sku)
                        # cpi_site_update_price.cpi_site_wait_switch_platform()
                        # cpi_site_update_price.check_map_download()
                        # cpi_site_update_price.wait_map_download()
                        cpi_site_update_price.filter_cheapest_nonc_raise(['Cheapest / Stay', 'Not Cheapest / Stay'])
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        cpi_site_update_price.check_stay_action_sign('Stay', update_price_check)
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        cpi_site_update_price.filter_cheapest_nonc_raise(['Cheapest / Raise', 'Not Cheapest / Raise'])
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        cpi_site_update_price.check_action_sign('Raise', update_price_check)
                        cpi_site_update_price.filter_cheapest_nonc_raise(['Cheapest / Lower', 'Not Cheapest / Lower'])
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        cpi_site_update_price.check_action_sign('Lower', update_price_check)
                        cpi_site_update_price.click_seller_info(update_price_check)
                        driver.execute_script("window.scrollTo(0, 0);")

                        time.sleep(2)

                        cpi_site_update_price.clear_filter()
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        cpi_site_update_price.select_price_category()
                        cpi_site_update_price.cpi_site_wait_switch_platform()
                        self.driver.save_screenshot('affordable_lamps.png')
                        cpi_site_update_price.cpi_site_click_first_sku(clickable_sku_index)
                        map_violation_checker = []
                        nearest_five_checker = []
                        all_dates, run_date, map_download_file_name = self.report_and_competitor_report(driver, cpi_site_platform, updateprice, i, dashboard, map_violation_checker, nearest_five_checker, account_name)

                        account_class = accountclass(self.driver)
                        account_class.map_download_verification(map_download_file_name, map_violation_checker, self.map_folder)

                        #SPECIALIST DASHBOARD START
                        time.sleep(5)

                        cpi_dashboard.cpi_site_cpi_side_bar_click()
                        cpi_dashboard.cpi_site_cpi_specialist_click()

                        cpi_site_spec_report.cpi_site_wait_for_load()
                        cpi_site_spec_report.filter_platform(cpi_site_account_name, cpi_site_platform[i])
                        cpi_site_spec_report.cpi_site_wait_for_load()
                        is_client_present = cpi_site_spec_report.is_client_present(cpi_site_account_name)
                        specialist_dashboard_date = []
                        if is_client_present == 'True':
                            cpi_site_spec_report.click_account(cpi_site_account_name)
                            self.driver.switch_to.window(self.driver.window_handles[1])
                            specialist_dashboard_date.append(
                                str(datetime.datetime.strptime(cpi_site_spec_report.get_run_date(), '%m/%d/%Y').date()))
                        else:
                            specialist_dashboard_date.append('Not Ran Today')
                        spec_report_product_count = cpi_site_spec_report.get_total_product_count()
                        print('')
                        result_list = []
                        # print("SPECIALIST DASHBOARD CHECKER")
                        if int(cpi_site_update_price_total_count) == int(spec_report_product_count):
                            result_list.append(['Update Price count and Specialist report count', 'PASS'])
                        else:
                               result_list.append(['Update Price count and Specialist report count', 'FAIL'])
                        check_data = """   EXECUTE CPI_Compare_RundateValues '"""+str(cpi_site_account_name)+"""','"""+str(cpi_site_update_price_date)+"""','"""+str(cpi_platform[cpi_site_platform[i]])+"""' """
                        price_category_to_check, price_category_index, only_seller_index = cpi_site_spec_report.get_price_category_index()
                        cpi_site_spec_report.get_table_data(price_category_to_check, price_category_index,only_seller_index, result_list, spec_report_product_count, check_data)
                        cpi_site_spec_report.attribute_change_page()
                        cpi_site_spec_report.attribute_change_wait()
                        attribute_data_check = cpi_site_spec_report.data_present_attribute_change()

                        if attribute_data_check == 'True':
                            result_list.append(['Attribute Page', 'PASS'])
                        else:
                            result_list.append(['Attribute Page', 'FAIL'])

                        print("UPDATE PRICE CHECKER")
                        print('')
                        headers = ['TEST_CASE', 'RESULT', 'VALUES']
                        print(tabulate(update_price_check, headers))
                        print('')
                        print('')

                        if len(map_violation_checker) > 1:
                            print("MAP VIOLATION CHECKER")
                            print('')
                            headers = ['TEST_CASE', 'RESULT', 'VALUES']
                            print(tabulate(map_violation_checker, headers))
                            print('')
                            print('')

                        if len(nearest_five_checker) > 1:
                            print("NEAREST 5 CHECKER")
                            print('')
                            headers = ['TEST_CASE', 'RESULT', 'VALUES']
                            print(tabulate(nearest_five_checker, headers))
                            print('')
                            print('')

                        print("SPECIALIST DASHBOARD CHECKER")
                        print(' ')
                        headers = ['TEST_CASE', 'RESULT', 'UI_VALUES', 'STORE_PROD_VAL']
                        print(tabulate(result_list, headers=headers))
                        print('')

                        all_dates['Specialist Report Date'] = specialist_dashboard_date

                        self.print_results(all_dates, run_date, cpi_site_update_price_date)
                        self.driver.switch_to.window(self.driver.window_handles[1])
                        self.driver.close()

                        #SPECIALIST DASHBOARD ENG
                        sys.stdout.flush()
                        os.system('sqlcmd -S 52.87.5.132 -U dh_admin -P "ECI@udh!23" -d Datahub_Mirror -Q "EXECUTE CPI_Validation_Report \''+str(cpi_site_account_name)+'\',\''+str(datetime.date.today().strftime("%Y-%m-%d"))+'\','+str(cpi_platform[cpi_site_platform[i]])+';"')
                        # os.system('sqlcmd -S 192.168.11.21 -U dh_admin -P "ECI@udh!23"_vm_prod -d Datahub_Mirror -Q "EXECUTE cpi_test '+str(account_name)+','+str(datetime.date.today().strftime("%Y-%m-%d"))+','+str(i+1)+';"')                            print(' ')
                        print(' ')
                        print(' ')
                        driver.switch_to.window(self.driver.window_handles[0])
                        time.sleep(10)
                    else:
                        print(str(cpi_site_platform[i]) + "'s Update Price has no value")
                else:
                    print(str(cpi_site_platform[i]) + ' has No Date Present')
                    print(' ')
        else:
            print(str(account_name) + ' has not run Today')
        return 0


    def report_and_competitor_report(self, driver, cpi_site_platform, updateprice, i, dashboard, map_violation_checker, nearest_five_checker, account_name):
        reporter = Reporter('Product Detail')
        report = accountclass(driver)
        product_detail_date = []
        report.wait_cpi_assortment_load()
        product_page_elem = self.driver.find_elements(By.XPATH,
                                                      '//*[@id="block-row1col1"]/div/div/div/div/table/tbody/tr/td[2]/span')
        if len(product_page_elem) == 0:
            if "No data available" in driver.page_source and "No data available in table" not in driver.page_source:
                product_detail_date.append('No data present in Product Detail')

        report_platform = cpi_site_platform.copy()
        report_platform = ['Hong Kong' if x == 'HongKong' else x for x in report_platform]
        report_platform = ['South Korea' if x == 'SouthKorea' else x for x in report_platform]
        # report_platform = platform_client_info
        # for j in range(len(report_platform)):
        #     if 'Google' in report_platform[j]:
        #         report_platform[j] = 'Google Shopping'
        report.set_platform(report_platform, i, reporter)
        product_detail_date.append(updateprice.get_run_date_V2())

        report_name_list = report.get_report_index()

        reporter = Reporter('CPI Assorment')
        report.click_cpi_assortment(reporter, report_name_list.index('CPI Assortment V2') + 1)
        # report.wait_cpi_assortment_load(reporter)
        assortment_date = []
        if "No data available" in driver.page_source:
            assortment_date.append('No data present in CPI_Assortment')
        report.set_platform(report_platform, i, reporter)
        # report.wait_cpi_assortment_load(reporter)
        assortment_date.append(updateprice.get_run_date_V2())

        time.sleep(5)
        reporter = Reporter('CPI Dashboard')
        report.click_cpi_dashboard(reporter, report_name_list.index('CPI Dashboard V2') + 1)
        # report.wait_cpi_dashboard(reporter)
        dashboard_date = []
        report.wait_cpi_report(reporter)
        if "No data available" in driver.page_source:
            dashboard_date.append('No data present in CPI_Dashboard')
        report.set_platform(report_platform, i, reporter)
        # report.wait_cpi_dashboard(reporter)
        dashboard_date.append(updateprice.get_run_date_V2())

        reporter = Reporter('MAP Violations')
        report.click_map_violations(reporter, report_name_list.index('CPI MAP Violations V2') + 1)
        map_violation_date = []
        if "No data available" in report.check_no_data_map_violation() or "No data available" in self.driver.page_source:
            map_violation_date.append('No data present in MAP Violations')
            map_violation_date.append(datetime.date.today().strftime("%Y-%m-%d"))
            map_download_file_name = "pdf"
        else:
            report.set_platform(report_platform, i, reporter)
            report.wait_map_violations(reporter)
            map_violation_date.append(updateprice.get_run_date_V2())
            report.wait_map_violations(reporter)
            map_seller_name = report.click_map_violation_detail()
            report.check_map_violation_detail(map_seller_name, map_violation_checker)
            report.map_violation_download_in_map_violation_page()
            report.wait_map_violation_download()
            map_download_file_name = "pdf_" + str(map_seller_name) + "_" + str(datetime.date.today().strftime("%Y-%m-%d"))

        reporter = Reporter('Nearest 5 Competitors')
        nearest_competitor = nearestcompetitorclass(driver)
        report.click_cpi_nearest_competitors(reporter, report_name_list.index('CPI Nearest 5 Competitors V2') + 1)
        report.wait_cpi_report(reporter)
        nearest_5_date = []
        if "No data available" in driver.page_source:
            nearest_5_date.append('No data present in Nearest 5 Competitors')
            nearest_5_date.append(datetime.date.today().strftime("%Y-%m-%d"))
            # driver.close()
        else:
            report.set_platform(report_platform, i, reporter)
            report.wait_cpi_report(reporter)
            nearest_5_date.append(updateprice.get_run_date_V2())
            nearest_first_item_name = nearest_competitor.click_item_name()
            nearest_competitor.check_item_page(nearest_first_item_name, nearest_five_checker)
            sku_name = nearest_competitor.get_first_sku_name()
            nearest_competitor.click_first_sku()
            nearest_competitor.check_click_first_sku(sku_name, str(cpi_site_platform[i]), nearest_five_checker)
            index_of_competitor = nearest_competitor.get_number_of_competitor()
            nearest_competitor.check_product_page_redirection(index_of_competitor, nearest_five_checker, cpi_site_platform[i])
            row_index = nearest_competitor.get_index_of_shipping()
            if row_index:
                no_shipping_client = ['MFS Supply']
                nearest_5_with_shipping_value = nearest_competitor.get_shipping_data(row_index, nearest_five_checker, no_shipping_client, account_name)
                nearest_competitor.wait_switch_price_display_mode()
                nearest_competitor.switch_price_display_mode()
                nearest_competitor.wait_switch_price_display_mode()
                nearest_5_with_total_price = nearest_competitor.get_total_price_data(row_index)
                nearest_competitor.check_with_and_without_shipping(nearest_5_with_shipping_value, nearest_5_with_total_price, nearest_five_checker)
                # print(nearest_5_with_shipping_value, nearest_5_with_total_price)
            nearest_competitor.get_all_competitor_price(index_of_competitor, nearest_five_checker)

        run_date = self.cpi_competitor_report(cpi_site_platform[i])

        all_dates = {
            'Assortment Date': assortment_date,
            'Dashboard Date': dashboard_date,
            'Map Violation Date': map_violation_date,
            'Nearest 5 Date': nearest_5_date,
            'Product Detail Date': product_detail_date,
        }

        return all_dates, run_date, map_download_file_name


    def print_results(self, all_dates, run_date, update_price_date):
        print('')
        result_date_list = []
        headers = ['DATE', 'RESULT', 'VALUE', 'REMARKS']
        print("DATE VALIDATION")
        print(' ')
        for date, values in all_dates.items():
            if update_price_date in str(values):
                len_values = len(values)
                if len_values > 1:
                    # print(date + ' is equal : ' + str(values[1]) + ' [' + str(values[0]) + '].')
                    result_date_list.append([date, 'PASS', str(values[1]), str(values[0])])

                else:
                    # print(date + ' is equal : ' + str(values[0]) + '.')
                    result_date_list.append([date, 'PASS', str(values[0])])
            else:
                len_values = len(values)
                if len_values > 1:
                    # print(date + ' is not equal : ' + str(values[1]) + ' [' + str(values[0]) + '].')
                    result_date_list.append([date, 'FAIL', str(values[1]), str(values[0])])
                else:
                    # print(date + ' is not equal : ' + str(values[0]) + '.')
                    result_date_list.append([date, 'FAIL', str(values[0])])

        for date, values in run_date.items():
            if values == update_price_date:
                # print(date + ' is equal : ' + str(values) + '.')
                result_date_list.append([date, 'PASS', str(values)])
            else:
                # print(date + ' is not equal : ' + str(values) + '.')
                result_date_list.append([date, 'FAIL', str(values)])

        print(tabulate(result_date_list, headers=headers))

        print('')
        print('')

        self.driver.close()
        time.sleep(5)


    def cpi_competitor_report(self, platform):
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])
        cpi_dashboard = cpisitedashboardpageclass(self.driver)
        cpi_dashboard.cpi_site_reports_bar_click()
        cpi_dashboard.cpi_site_competitor_report_click()
        cpi_dashboard.switch_cpi_window()

        time.sleep(2)

        cpi_site_comp_report = cpisitecompetitorsreportclass(self.driver)
        cpi_site_comp_report.wait_for_page_load()
        cpi_site_comp_report.reset_slider_value()
        cpi_site_comp_report.wait_for_page_load()
        cpi_site_comp_report.clear_filter()
        self.driver.save_screenshot('Competitor_Report_Bug.png')
        cpi_site_comp_report.wait_for_page_load()
        cpi_site_comp_report.select_price_category()
        cpi_site_comp_report.wait_for_page_load()
        # cpi_site_comp_report.clear_filter()
        # cpi_site_comp_report.wait_for_page_load()
        cpi_site_comp_report.reset_seller_filter()
        cpi_site_comp_report.wait_competitors_report_page_load()
        comp_report_platform = cpi_site_comp_report.get_platform()
        run_date = {}
        time.sleep(3)
        selected_option = cpi_site_comp_report.selected_text()
        comp_report_platform.remove(selected_option)
        # print('')
        # print('---COMPETITOR REPORT---')
        # print("Platform : "+platform[i])
        cpi_site_comp_report.select_platform(platform)
        cpi_site_comp_report.wait_for_page_load()
        if 'No data available in table' in self.driver.page_source:
            run_date["Competitor_Report_Date_" + platform] = 'No report available for ' + platform
        else:
            run_date["Competitor_Report_Date_" + platform] = datetime.datetime.strptime(cpi_site_comp_report.cpi_site_get_run_date(),'%m/%d/%Y').strftime("%#Y-%m-%d")
        return run_date

    @classmethod
    def tearDownClass(cls):
        # time.sleep(4)
        # cls.driver.close()
        # cls.driver.quit()

        # unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='example.html'))
        pass
